
# coding:utf-8
import random
import threading
import Config
import time
from lxml import etree
# from IntroductionLogic import IntroductionLogic
import Utils as Utils
import requests
import json
from logger_file import logger
from Logic import Logic

class Introduction(threading.Thread):
    def __init__(self, kwargs={}):
        threading.Thread.__init__(self, target=self.work, kwargs=kwargs)
        self.s = requests.session()
        self.a = ''
        self.lock = threading.Lock()


    def work(self, **kwargs):
        n = 0
        pp = []
        print(kwargs)
        print(len(kwargs.items()))
        for k, v in kwargs.items():
            print(k, v)
            items = {}
            url = "https://www.feixiaohao.com/currencies/{}/".format(str(k))
            print(url)
        # #     # url = "https://www.okex.com/api/v1/kline.do?symbol={}_usdt&type=1hour".format(str(i))
            response = self.request(url)
            html = etree.HTML(response)
            items['c_id'] = v
            try:
                imgs = html.xpath('//*[@id="__layout"]/section/div/div/div[1]/div[1]/div[1]/img/@src')
                print(imgs)
                items['img'] = imgs
            except:
                items['img'] = "-"
            # try:
            #     intro = html.xpath('//*[@id="__layout"]/section/div/div/div[1]/div[2]/div/div[3]/div[1]/div[1]/div[1]/text()')[0]
            #     print(intro)
            #     items['intro'] = intro
            # except:
            #     items['intro'] = "-"
            # try:
            #     liutong_num = html.xpath('//*[@id="__layout"]/section/div/div/div[1]/div[2]/div/div[3]/div[1]/div[1]/div[3]/div[3]/span[2]/text()')
            #     print((liutong_num[0].replace(',', '')))
            #     items['liutong'] = (liutong_num[0].replace(',', ''))
            # except:
            #     items['liutong'] = "-"
            # try:
            #     publicTime = html.xpath('//*[@id="__layout"]/section/div/div/div[1]/div[2]/div/div[3]/div[1]/div[1]/div[3]/div[1]/span[2]/text()')
            #     print(publicTime[0])
            #     items['publicTime'] = publicTime[0]
            # except:
            #     items['publicTime'] = "-"
            # try:
            #     supple = html.xpath('//*[@id="__layout"]/section/div/div/div[1]/div[2]/div/div[3]/div[1]/div[1]/div[3]/div[2]/span[2]/text()')
            #     print((supple[0]).replace(",", ''))
            #     items['supple'] = (supple[0]).replace(",", '')
            # except:
            #     items['supple'] = "-"
            # try:
            #     ex_num = html.xpath('//*[@id="__layout"]/section/div/div/div[1]/div[2]/div/div[3]/div[1]/div[1]/div[3]/div[4]/span[2]/text()')
            #     print((str(ex_num[0])).replace('家', ""))
            #     items['exchange_num'] = ex_num
            # except:
            #     items['exchange_num'] = "-"
            # print(items)
            pp.append(items)

        print(pp)
        # logic.importData(pp)

    def request(self, url,kwargs = {}):
        n = 0
        while True:
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36',
                "Accept-Encoding": "Gzip"
            }
            try:
                response = self.s.get(url, headers=headers,timeout=5)
                return response.text
            except requests.exceptions.ConnectionError:
                n = n + 1
                if n == Config.REQUEST_NUM:
                    break
                continue
            except requests.exceptions.Timeout:
                n = n + 1
                if n == Config.REQUEST_NUM:
                    break
                continue

if __name__ == "__main__":
    logger = logger("feixiaohao.log")
    logger.info("****************************{}**************************".format(Utils.getCurFullDay()))
    ppp = []
    curr_list = {}
    A = Utils.getCurFullDay()
    select_logic = Logic("finance_data")
    start = time.clock()
    logic = Introduction()
    currencys = select_logic.get_currenies()
    if currencys != {}:
        for i in currencys:
            id = i.get("id")
            name = i.get("name")
            if str(name).upper() in curr_list.keys():
                continue
            curr_list[str(name).upper()] = id
    item_kes = {'bitcoin': 1, 'ethereum': 2, 'ripple': 4, 'bitcoin-cash': 6, 'bitcoin-cash-sv': 9, 'litecoin': 5, 'eos': 7, 'tezos': 38, 'stellar': 62, 'monero': 14, 'chainlink': 17, 'ht': 3, 'cardano': 12, 'tron': 10, 'dash': 13, 'ethereum-classic': 8, 'neo': 33, 'cosmos': 21, 'nem': 91, 'zcash': 11, 'ont': 18, 'basic-attention-token': 97, 'dogecoin': 59, 'vechaincom': 71, 'qtum': 29, 'decred': 101, 'vsystems': 42, '0x': 93, 'waves': 68, 'omisego': 43, 'raiblocks': 109, 'bytom': 26, 'steem': 46, 'status': 88, 'bitshares': 64, 'hypercash': 37, 'iost': 19, 'seele': 30, 'golem-network-tokens': 92, 'zilliqa': 63, 'bitcoinhd': 48, 'zcoin': 81, 'republic-protocol': 56, 'waykichain': 32, 'aeternity': 40, 'decentraland': 96, 'gxshares': 45, 'aelf': 47, 'elastos': 77, 'mxtoken': 23, 'nuls': 102, 'bhttoken': 52, 'lambda': 22, 'storj': 95, 'nebulas': 58, 'cortex': 67, 'walton': 84, 'newton': 31, 'bixtoken': 86, 'civic': 98, 'ekt8': 20, 'ruff': 87, 'cybermiles': 65, 'unlimitedip': 83, 'achain': 66, 'smartmesh': 54, 'iotchain': 49, 'fusion': 82, 'medishares': 94, 'odyssey': 80, 'crypto-com-chain': 108, 'gatechaintoken': 89, 'thetatoken': 57, 'bittorrent': 44, 'thundercore': 25, 'hpt': 24, 'one': 104, 'kan': 107, 'projectpai': 28, 'atlasprotocol': 78, 'rsr': 39, 'irisnet': 74, 'harmonyone': 104, 'nkn': 110, 'libra-credit': 106, 'unetwork': 34, 'cnns': 103, 'arpa': 70, 'egretia': 51, 'allsportschain': 72, 'topnetwork': 73, 'dock': 111, 'blocktrade': 44, 'data': 75, 'origo': 55, 'linkeye': 76, 'hitchain': 41, 'litex': 60, 'skrumblenetwork': 100, 'for': 90, 'topone': 73, 'farmatrust': 99, 'algorand': 27, 'menlo-one': 104, '1bcone': 104, 'cybereits': 79, 'carry': 79, 'comet': 65, 'lexit': 60}

    # print(curr_list)
    # item = {}
    # for j in range(1, 30):
    #
    #     response = requests.get("https://dncapi.bqiapp.com/api/coin/web-coinrank?page={}&type=-1&pagesize=100&webp=1".format(j)).text
    #     jsondata = json.loads(response)
    #     for i in jsondata.get('data'):
    #
    #         print(i)
    #         if i.get("name") in curr_list.keys():
    #             item[i.get('code')] = curr_list[i.get('name')]
    # print(item)
    # print(len(item.keys()))
    # for i in range(1, 30):
    #     news = Feixiaohao({"url": requests_url.format(str(i))})
    #     news.start()
    # news = Introduction(item_kes)
    # news.start()

    from feixiaohao_spider.introductionLogic import IntroductionLogic
    logic = IntroductionLogic()

    pps = [{'c_id': 1, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/bitcoin_200_200.png?v=20']},
     {'c_id': 2, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/ethereum_200_200.png?v=35']},
     {'c_id': 4, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/ripple_200_200.png?v=05']},
     {'c_id': 6, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/bitcoin-cash_200_200.png?v=21']},
     {'c_id': 9, 'img': ['https://s2.bqiapp.com/image/20190121/bitcoincashsv_mid_1548040891573.png?v=53']},
     {'c_id': 5, 'img': ['https://s2.bqiapp.com/image/20190201/litecoin_mid.png?v=08']},
     {'c_id': 7, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/eos_200_200.png?v=08']},
     {'c_id': 38, 'img': ['https://s2.bqiapp.com/image/20181213/tezos_mid.png']},
     {'c_id': 62, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/stellar_200_200.png?v=17']},
     {'c_id': 14, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/monero_200_200.png']},
     {'c_id': 17, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/chainlink_200_200.png']},
     {'c_id': 3, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/ht_200_200.png?v=97']},
     {'c_id': 12, 'img': ['https://s2.bqiapp.com/image/20190227/cardano_mid.png']},
     {'c_id': 10, 'img': ['https://s2.bqiapp.com/image/20190220/tron_mid.png']},
     {'c_id': 13, 'img': ['https://s2.bqiapp.com/image/20190402/dash_mid.png?v=45']},
     {'c_id': 8, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/ethereum-classic_200_200.png?v=09']},
     {'c_id': 33, 'img': ['https://s2.bqiapp.com/logo/1/neo_72.png?v=98']},
     {'c_id': 21, 'img': ['https://s2.bqiapp.com/image/20190315/1552613537840_mid.png?v=1562147544']},
     {'c_id': 91, 'img': ['https://s2.bqiapp.com/image/20190114/nem_mid.png']},
     {'c_id': 11, 'img': ['https://s2.bqiapp.com/image/20181214/zcash_mid.png?v=59']},
     {'c_id': 18, 'img': ['https://s2.bqiapp.com/logo/1/ont_72.png?v=14']},
     {'c_id': 97, 'img': ['https://s2.bqiapp.com/image/20190404/basicattentiontoken_mid.png?v=08']},
     {'c_id': 59, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/dogecoin_200_200.png']},
     {'c_id': 71, 'img': ['https://s2.bqiapp.com/image/20190423/vechaincom_mid.png']},
     {'c_id': 29, 'img': ['https://s2.bqiapp.com/image/20190330/qtum_mid.png']},
     {'c_id': 101, 'img': ['https://s2.bqiapp.com/image/20190320/decred_mid.png?v=1562558097']},
     {'c_id': 42, 'img': ['https://s2.bqiapp.com/image/20190325/vsystems_mid_1553502887176.png?v=69']},
     {'c_id': 93, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/0x_200_200.png']},
     {'c_id': 68, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/waves_200_200.png?v=50']},
     {'c_id': 43, 'img': ['https://s2.bqiapp.com/image/20190213/omisego_mid_1550021305608.png']},
     {'c_id': 109, 'img': ['https://s2.bqiapp.com/image/20181205/raiblocks_mid.png?v=1562229233']},
     {'c_id': 26, 'img': ['https://s2.bqiapp.com/image/20190330/bytom_mid.png?v=51']},
     {'c_id': 46, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/steem_200_200.png']},
     {'c_id': 88, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/status_200_200.png']},
     {'c_id': 64, 'img': ['https://s2.bqiapp.com/image/20190123/bitshares_mid.png']},
     {'c_id': 37, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/hypercash_200_200.png']},
     {'c_id': 19, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/iost_200_200.png?v=89']},
     {'c_id': 30, 'img': ['https://s2.bqiapp.com/image/20190409/seele_mid.png?v=63']},
     {'c_id': 92, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/golem-network-tokens_200_200.png?v=1560244602']},
     {'c_id': 63, 'img': ['https://s2.bqiapp.com/image/20190220/zilliqa_mid_1550625668192.png']},
     {'c_id': 48, 'img': ['https://s2.bqiapp.com/uploadfiles/nopassed/bitcoinhd_bhd_big.png?v=49']},
     {'c_id': 81, 'img': ['https://s2.bqiapp.com/image/20190116/zcoin_mid.png?v=38']},
     {'c_id': 56, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/republic-protocol_200_200.png']},
     {'c_id': 32, 'img': ['https://s2.bqiapp.com/logo/1/waykichain_72.png?v=42']},
     {'c_id': 40, 'img': ['https://s2.bqiapp.com/image/20190116/aeternity_mid.png?v=60']},
     {'c_id': 96, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/decentraland_200_200.png?v=1560328992']},
     {'c_id': 45, 'img': ['https://s2.bqiapp.com/image/20181119/gxshares_mid.png?v=1565168902']},
     {'c_id': 47, 'img': ['https://s2.bqiapp.com/image/20181115/aelf_mid.png?v=1561174981']},
     {'c_id': 77, 'img': ['https://s2.bqiapp.com/image/20190624/elastos_mid.png?v=1561339400']},
     {'c_id': 23, 'img': ['https://s2.bqiapp.com/image/20190419/mxtoken_mid.png?v=90']},
     {'c_id': 102, 'img': ['https://s2.bqiapp.com/image/20190401/nuls_mid.png?v=75']},
     {'c_id': 52, 'img': ['https://s2.bqiapp.com/image/20190722/bhttoken_mid.png?v=1567150945']},
     {'c_id': 22, 'img': ['https://s2.bqiapp.com/logo/1/lambda_72.png?v=64']},
     {'c_id': 95, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/storj_200_200.png?v=1560493968']},
     {'c_id': 58, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/nebulas_200_200.png']},
     {'c_id': 67, 'img': ['https://s2.bqiapp.com/image/20190215/cortex_mid.png']},
     {'c_id': 84, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/walton_200_200.png']},
     {'c_id': 31, 'img': ['https://s2.bqiapp.com/image/20190409/newton_mid.png?v=52']},
     {'c_id': 86, 'img': ['https://s2.bqiapp.com/image/20181015/bixtoken_mid.png?v=58']},
     {'c_id': 98, 'img': ['https://s2.bqiapp.com/image/20181109/civic_mid_1541747036479.png']},
     {'c_id': 20, 'img': ['https://s2.bqiapp.com/image/20181015/ekt8_mid.png?v=1563245530']},
     {'c_id': 87, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/ruff_200_200.png?v=1563871471']},
     {'c_id': 65, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/cybermiles_200_200.png?v=1561341893']},
     {'c_id': 83, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/unlimitedip_200_200.png?v=1564023856']},
     {'c_id': 66, 'img': ['https://s2.bqiapp.com/image/20190325/achain_mid.png?v=68']},
     {'c_id': 54, 'img': ['https://s2.bqiapp.com/image/20190118/smartmesh_mid.png']},
     {'c_id': 49, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/iotchain_200_200.png']},
     {'c_id': 82, 'img': ['https://s2.bqiapp.com/image/20190318/fusion_mid.png']},
     {'c_id': 94, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/medishares_200_200.png']},
     {'c_id': 80, 'img': ['https://s2.bqiapp.com/coin/20181030_72_png/odyssey_200_200.png']},
     {'c_id': 108, 'img': ['https://s2.bqiapp.com/image/20181218/cryptocomchain_mid.png?v=1562809637']},
     {'c_id': 89, 'img': ['https://s2.bqiapp.com/image/20190401/gatechaintoken_mid_1554109432555.png?v=05']},
     {'c_id': 57, 'img': ['https://s2.bqiapp.com/image/20181015/thetatoken_mid.png']},
     {'c_id': 44, 'img': ['https://s2.bqiapp.com/image/20190213/bittorrent_mid.png']},
     {'c_id': 25, 'img': ['https://s2.bqiapp.com/image/20190511/thundercore_mid.png?v=96']},
     {'c_id': 24, 'img': ['https://s2.bqiapp.com/image/20181224/hpt_mid.png?v=66']},
     {'c_id': 104, 'img': ['https://s2.bqiapp.com/image/20181113/one_mid.png?v=57']},
     {'c_id': 107, 'img': ['https://s2.bqiapp.com/image/20190412/kan_mid.png?v=46']},
     {'c_id': 28, 'img': ['https://s2.bqiapp.com/uploadfiles/nopassed/projectpai_pai_big.png']},
     {'c_id': 78, 'img': ['https://s2.bqiapp.com/image/20181024/atlasprotocol_atp_mid.png']},
     {'c_id': 39, 'img': ['https://s2.bqiapp.com/image/20190513/rsr_mid.png']},
     {'c_id': 74, 'img': ['https://s2.bqiapp.com/image/20190408/irisnet_mid_1554709179399.png?v=86']},
     {'c_id': 104, 'img': ['https://s2.bqiapp.com/image/20190516/harmonyone_mid.png?v=83']},
     {'c_id': 110, 'img': ['https://s2.bqiapp.com/image/20181112/nkn_mid_1542003534398.png']},
     {'c_id': 106, 'img': ['https://s2.bqiapp.com/image/20181218/libracredit_mid.png?v=24']},
     {'c_id': 34, 'img': ['https://s2.bqiapp.com/image/20181207/unetwork_mid.png']},
     {'c_id': 103, 'img': ['https://s2.bqiapp.com/image/20190418/cnns_mid_1555552728996.png']},
     {'c_id': 70, 'img': ['https://s2.bqiapp.com/image/20190625/arpa_mid.png?v=16']},
     {'c_id': 51, 'img': ['https://s2.bqiapp.com/image/20181015/egretia_mid.png?v=1562809464']},
     {'c_id': 72, 'img': ['https://s2.bqiapp.com/image/20181015/allsportschain_mid.png?v=77']},
     {'c_id': 73, 'img': ['https://s2.bqiapp.com/image/20190321/1553149119527_mid.png']},
     {'c_id': 111, 'img': ['https://s2.bqiapp.com/image/20190124/dock_mid.png']},
     {'c_id': 44, 'img': ['https://s2.bqiapp.com/image/20180927/blocktrade_mid.png']},
     {'c_id': 75, 'img': ['https://s2.bqiapp.com/image/20181114/data_mid.png']},
     {'c_id': 55, 'img': ['https://s2.bqiapp.com/image/20190604/1559609528409_mid.png?v=1562576495']},
     {'c_id': 76, 'img': ['https://s2.bqiapp.com/image/20181224/linkeye_mid.png']}, {'c_id': 41, 'img': [
        'https://s2.bqiapp.com/uploadfiles/coin/20180802/06eae2ae3fa94393a596c3f04f949275_2_32_32.jpg']},
     {'c_id': 60, 'img': ['https://s2.bqiapp.com/image/20180930/litex_mid.png']},
     {'c_id': 100, 'img': ['https://s2.bqiapp.com/image/20181108/skrumblenetwork_mid.png']},
     {'c_id': 90, 'img': ['https://s2.bqiapp.com/image/20190424/1556070126283_mid.png?v=1564624583']},
     {'c_id': 73, 'img': ['https://s2.bqiapp.com/image/20181127/topone_mid_1543303139132.png']},
     {'c_id': 99, 'img': ['https://s2.bqiapp.com/image/20181015/farmatrust_mid.png']},
     {'c_id': 27, 'img': ['https://s2.bqiapp.com/image/20190619/algorand_mid.png?v=1561080571']},
     {'c_id': 104, 'img': ['https://s2.bqiapp.com/image/20190426/menloone_mid.png']},
     {'c_id': 104, 'img': ['https://s2.bqiapp.com/logo/1/1bcone_72.png?v=51']},
     {'c_id': 79, 'img': ['https://s2.bqiapp.com/image/20181207/cybereits_mid.png']},
     {'c_id': 79, 'img': ['https://s2.bqiapp.com/image/20190520/1558314178806_mid.png?v=41']},
     {'c_id': 65, 'img': ['https://s2.bqiapp.com/image/20181114/comet_mid.png']},
     {'c_id': 60, 'img': ['https://s2.bqiapp.com/image/20181105/lexit_mid.png?v=1567219421']}]
    logic.importData(pps)
    # for i in kk:print(i)