#! /usr/bin env python
# coding:utf-8
# 业务逻辑

import Config
import pymysql
import json
import sys
import threading
import Utils as Utils

from Logic import Logic

class IntroductionLogic(Logic):
    def __init__(self):
        Logic.__init__(self, 'finance_data')
        # self.db = pymysql.connect(
        #     host="172.21.0.17",
        #     port=3306,
        #     user="kuailian",
        #     passwd="aJxu1OGpYxaBsNKUkOkw",
        #     db="kuailian",
        #     charset="utf8mb4"
        # )
        # self.cursor = self.db.cursor(cursor=pymysql.cursors.DictCursor)
        self.b = []
        self.c = 0
        self.lock = threading.Lock()

    def importData(self, rows):
        for i in rows:
            #更新简介
            # c_id  = i.get('c_id')
            # info  = i.get('intro')
            # liutong  = i.get('liutong')
            # publicTime  = i.get('publicTime')
            # exchange_num  = i.get('exchange_num')[0].replace("家", "")
            # supple  = i.get('supple')
            # valuesTuple = (
            #                 c_id,
            #                 pymysql.escape_string(info),
            #                 liutong,
            #                 publicTime,
            #                 exchange_num,
            #                 supple,
            #                 u"feixiaohao"
            #             )
            # Utils.D(valuesTuple)
            # values = "%s,'%s','%s','%s','%s','%s','%s'" % valuesTuple
            # sql = "insert into %s (`c_id`, `info`, `liutong`, `publicTime`, `exchange_num`, `supple`, `source`) values (%s)" % (
            #     "introduction", values)
            # print("Insert: %s" % sql)
            # self.cursor.execute(sql)
            #更新logo
            c_id = i.get('c_id')
            img = i.get('img')[0]
            sql = "UPDATE currencies SET logo = '%s' WHERE id = %s" % (
                img, c_id)
            print("Insert: %s" % sql)
            self.cursor.execute(sql)
        self.db.commit()
        # n = 72
        # for i in rows:
        #     print(i)
        #     sql = "UPDATE `currencies` SET `is_del` = %s,`status` = %s WHERE `name` = '%s'"%(1,0,str(i).upper())
        #     # sql = "INSERT INTO `currencies` (`id`,`name`, `logo`, `is_del`, `sort`,  `shorthand`, `introduce`, `note`, `status`) VALUES (%s, '%s', 'Null', '0', '2029',  NULL, NULL, NULL, '1');" % (n,str(i).upper())
        #     print(sql)
        #     self.cursor.execute(sql)
        #     n+=1
            # self.db.commit()
        # cc = [u'btex', u'aacoin', u'bigone', u'gatecoin', u'iquant', u'idcm-io', u'dextop', u'acx-io', u'liqui',
        #       u'gopax', u'braziliex', u'ocx', u'fex-hk', u'novaexchange', u'coinyee', u'coinfalcon', u'be-top',
        #       u'neraexpro', u'cryptonex', u'coinpark', u'fatbtc', u'fubt', u'hib8', u'fcoin', u'cryptobridge', u'rudex',
        #       u'coinexmarket', u'bilaxy', u'b2bx', u'forkdelta', u'bgj-io', u'otcbtc', u'zaif', u'fisco', u'rootrex',
        #       u'syex-io', u'hht-one', u'mercatox', u'cryptox', u'cybex', u'c-cex', u'bitbay', u'cobinhood', u'bitmart',
        #       u'coss', u'uex', u'coinbene', u'coinhub', u'xbrick-io', u'therocktrading', u'yoe-im', u'koinex',
        #       u'openledger', u'coinroom', u'utxo', u'cryptohub', u'ore-bz', u'simex', u'bleutrade', u'tdax', u'c2cx',
        #       u'freiexchange', u'bisq', u'kuna', u'cfinex', u'btcdo', u'lykke-exchange', u'cm-xyz', u'coinmake',
        #       u'rightbtc', u'token-store', u'uncoinex', u'ebtcbank', u'ddex', u'trade-by-trade', u'qbtc', u'leoxchange',
        #       u'coinbe', u'coolcoin', u'tokenomy', u'btc-alpha', u'coin2coin', u'ooobtc', u'bx-thailand',
        #       u'stocks-exchange', u'tux-exchange', u'coinsuper', u'omicrex', u'bjex', u'rfinex', u'bitforex',
        #       u'heat-wallet', u'coinoah', u'coinegg', u'digifinex', u'coinrail', u'indodax', u'thinkbit', u'bitflip',
        #       u'bancor-network', u'dex-top', u'kyber-network', u'bition-pro', u'tradeogre', u'tidebit', u'bittylicious',
        #       u'bitebtc', u'latoken', u'abcc', u'idax', u'localtrade', u'gemini', u'bitlish', u'poloniex', u'oasisdex',
        #       u'hpx', u'crex24', u'bitpaction', u'btctrade-im', u'keepbtc', u'etherflyer', u'kucoin', u'qryptos',
        #       u'abucoins', u'bitso', u'chaoex', u'coindeal', u'c-patex', u'dobitrade', u'cex-com', u'exrates',
        #       u'bige-top', u'hotbit', u'waves-dex', u'etherdelta', u'58coin', u'bittrex', u'trade-satoshi', u'kraken',
        #       u'coineal', u'hibtc', u'sistemkoin', u'exx', u'bitinka']
        # logic = BlockLogic()
        # currencys = logic.getCurrencyList()
        # for i in Config.k:
        #     if i.get("symbol") not in [j.get('name') for j in currencys]:
        #

                # for i in cc:
        #     # sql = "INSERT INTO `exchanges` (`id`, `name`, `logo`, `is_del`, `sort`, `created_at`) VALUES ('%s', '%s', NULL, '0', '1000', NULL);" % (str(i), str(cc.pop()))
        #     sql = "INSERT INTO `exchanges` (`name`, `logo`, `is_del`, `sort`) VALUES ('%s', 'Null', '0', '1000');" % (str(i))
        #     Utils.D("Insert: %s" % sql)
        #     self.cursor.execute(sql)
        #     self.db.commit()
        # "INSERT INTO `kuailian`.`exchanges` (`id`, `name`, `logo`, `is_del`, `sort`, `created_at`) VALUES ('52', '比特亚洲', 'Null', '0', '1000', );"
        # sql = ""
        # if 0 == len(rows):
        #     return
        # self.lock.acquire()
        # # f = open('/root/spider/newsspider/week_table/week_table.txt', 'r')
        # # tables = f.read()
        # rate = self.getRate()[0].get('rate')
        # Cnyprice = self.getCnyprice(34,"market_currency_deals_20180716")[0]
        # btcCnyprice = Cnyprice.get('cnyprice')
        # btcprice = Cnyprice.get('price')
        # Cnyprice = self.getCnyprice(1, "market_currency_deals_20180716")[0]
        # ethCnyprice = Cnyprice.get('cnyprice')
        # ethprice = Cnyprice.get('price')
        # for i in rows:
        #     try:
        #         exchange_name = i.get("market", "")
        #         # print(exchange_name)
        #         marketInfo = self.getMarketByName(exchange_name)
        #         if 0 == len(marketInfo):
        #             self.b.append(exchange_name)
        #             continue
        #         if str(i.get("symbol_pair")).endswith('_USDT') or str(i.get("symbol_pair")).endswith('_USD'):
        #
        #             c_id = id
        #             e_id = marketInfo.get('id',0)
        #             price = i.get('last')
        #             cnyprice = float(price) * float(rate)
        #             hightPrice = i.get('high')
        #             cnyHightPrice = float(hightPrice) * float(rate)
        #             lowPrice = i.get('low')
        #             cnyLowPrice = float(lowPrice) * float(rate)
        #             riseRate = i.get('change_daily')
        #             c_volume = i.get('base_volume',0)
        #             #
        #             valuesTuple = (
        #                 c_id,
        #                 e_id,
        #                 price,
        #                 cnyprice,
        #                 hightPrice,
        #                 cnyHightPrice,
        #                 lowPrice,
        #                 cnyLowPrice,
        #                 riseRate,
        #                 c_volume,
        #                 u"block"
        #             )
        #             Utils.D(valuesTuple)
        #             # #     # try:
        #             #     #insert
        #             values = "%s,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'" % valuesTuple
        #             sql = "insert into %s (`c_id`, `e_id`, `price`, `cnyprice`, `hightPrice`, `cnyHightPrice`, `lowPrice`, `cnyLowPrice`,`riseRate`, `cny_volume`, `source`) values (%s)" % (
        #                 "market_currency_deals_20180716", values)
        #             print("Insert: %s" % sql)
        #             self.cursor.execute(sql)
        #         # except:
        #         #     raise FeixiaohaoException(Config.KD_ERROR, [
        #         #         "sql: %s" % sql,
        #         #         "value: %s" % json.dumps(valuesTuple)
        #         #     ])
        #
        #     # 提交事物
        #     # self.db.commit()
        #     # print(self.b)
        #
        #         elif str(i.get("symbol_pair")).endswith('_BTC'):
        #             c_id = id
        #             e_id = marketInfo.get('id', 0)
        #             price = float(i.get('last')) * float(btcprice)
        #             cnyprice = float(price) * float(rate)
        #             hightPrice = float(i.get('high')) * float(btcprice)
        #             cnyHightPrice = float(hightPrice) * float(rate)
        #             lowPrice = i.get('low')
        #             cnyLowPrice = float(lowPrice) * float(rate)
        #             riseRate = i.get('change_daily')
        #             c_volume = i.get('base_volume', 0)
        #             valuesTuple = (
        #                 c_id,
        #                 e_id,
        #                 price,
        #                 cnyprice,
        #                 hightPrice,
        #                 cnyHightPrice,
        #                 lowPrice,
        #                 cnyLowPrice,
        #                 riseRate,
        #                 c_volume,
        #                 u"block"
        #             )
        #             Utils.D(valuesTuple)
        #             # # #     # try:
        #             # #     #insert
        #             values = "%s,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'" % valuesTuple
        #             sql = "insert into %s (`c_id`, `e_id`, `price`, `cnyprice`, `hightPrice`, `cnyHightPrice`, `lowPrice`, `cnyLowPrice`,`riseRate`, `cny_volume`, `source`) values (%s)" % (
        #                 "market_currency_deals_20180716", values)
        #             print("Insert: %s" % sql)
        #             self.cursor.execute(sql)
        #
        #         elif str(i.get("symbol_pair")).endswith('_ETH'):
        #             c_id = id
        #             e_id = marketInfo.get('id', 0)
        #             price = float(i.get('last')) * float(ethprice)
        #             cnyprice = float(price) * float(rate)
        #             hightPrice = float(i.get('high')) * float(ethprice)
        #             cnyHightPrice = float(hightPrice) * float(rate)
        #             lowPrice = float(i.get('low')) * float(ethprice)
        #             cnyLowPrice = float(lowPrice) * float(rate)
        #             riseRate = i.get('change_daily')
        #             c_volume = i.get('base_volume', 0)
        #             valuesTuple = (
        #                 c_id,
        #                 e_id,
        #                 price,
        #                 cnyprice,
        #                 hightPrice,
        #                 cnyHightPrice,
        #                 lowPrice,
        #                 cnyLowPrice,
        #                 riseRate,
        #                 c_volume,
        #                 u"block"
        #             )
        #             Utils.D(valuesTuple)
        #             # # #     # try:
        #             # #     #insert
        #             values = "%s,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s'" % valuesTuple
        #             sql = "insert into %s (`c_id`, `e_id`, `price`, `cnyprice`, `hightPrice`, `cnyHightPrice`, `lowPrice`, `cnyLowPrice`,`riseRate`, `cny_volume`, `source`) values (%s)" % (
        #                 "market_currency_deals_20180716", values)
        #             print("Insert: %s" % sql)
        #             self.cursor.execute(sql)
        #     except:
        #         self.c+=1
        #
        #         continue
        # self.db.commit()
        # self.lock.release()
        # print("异常币种信息{}".format(self.c))