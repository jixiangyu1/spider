
# coding:utf-8

import threading
import Config
import time
from lxml import etree
import Utils as Utils
import requests
import json
import hashlib
from Logic import Logic
from feixiaohao_spider.fei_logic import FeixiaohaoLogic
from logger_file import logger
from news.request_class import ReqCls

orderno = "ZF2020411942R36FBe"
secret = "ca9153d6fce24c98affba07bcf819e3e"
ip_forward = "forward.xdaili.cn"

def auth_proxy():
    timestamp = str(int(time.time()))
    string = ""
    string = "orderno=" + orderno + "," + "secret=" + secret + "," + "timestamp=" + timestamp

    string = string.encode()

    md5_string = hashlib.md5(string).hexdigest()
    sign = md5_string.upper()
    # print(sign)
    auth = "sign=" + sign + "&" + "orderno=" + orderno + "&" + "timestamp=" + timestamp

    return auth


port = "80"
ip_port = ip_forward + ":" + port
privoxy = {"http": "http://" + ip_port, "https": "https://" + ip_port}

class Feixiaohao(threading.Thread):
    def __init__(self, kwargs={}):
        threading.Thread.__init__(self, target=self.work, kwargs=kwargs)
        self.s = requests.session()
        self.req = ReqCls()
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36',
            'Referer': 'https://www.feixiaohao.com/',
        }
    def work(self, **kwargs):
        global ppp
        global b
        global ip
        global currencies_items
        row = []
        pp = 0
        while True:
            # try:
            url = kwargs.get("url")
            response = self.req.req_html(url,self.headers,"GET")
            try:
                # response = self.request(url).text

                jsondata = (json.loads(response)).get('data')
                for i in (jsondata):
                    items = {}
                    name = i.get('name')
                    if curr_list.get(name) != None:
                        marketValue = i.get('market_value') / 100000000
                        items['makeValue'] = (round(marketValue,2))
                        price = i.get('current_price')
                        items['price'] = (price)
                        marketNum = i.get('supply') /10000
                        items['makeNum'] = (round(marketNum,2))
                        volumn = i.get('vol')/100000000
                        items['volumn'] = (round(volumn,2))
                        priceLimit = i.get('change_percent')
                        items['pricelimit'] = (priceLimit)
                        if curr_list.get(str(name)) != None:
                            items['c_id'] = curr_list.get(str(name))
                        else:
                            continue
                        print(items)
                        row.append(items)
                break
            except Exception as e:
                print("不能解析")
                pp+=1
                # print(response)
                time.sleep(2)
                if pp == 5:
                    print("出现五次解析不成功")
                    break





        ppp.append(row)
        # print(threading.active_count())
        if threading.active_count() == 2:
            print("===={}".format(len(ppp)))
            logic.importData(ppp,A)
        exit(0)


    def request(self, url):
        n = 0

        # print("请求的ip{}".format(ip))
        while True:
            ip = privox()
            proxies = {
                "http": "http://{}".format(ip),
                       # "https": "https://{}".format(ip),
                       }
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36',
                'Referer': 'https://www.feixiaohao.com/',
            }
            #
            try:
                response = requests.get(url, headers=headers,proxies=proxies,verify=False,timeout = 15)

                if response.text == None:
                    time.sleep(3)
                    continue
                else:
                    return response.text
            except requests.exceptions.ConnectTimeout:
                n = n + 1
                if n == Config.REQUEST_NUM:
                    break
                time.sleep(4)
                n += 1
                if n == 5:
                    break
                print("超时切换ip为{}".format(ip))
                continue
            except Exception as e:
                print(e)
                time.sleep(3)
                n+=1
                if n == 5:
                    break
                # ip = privox()
                print("异常切换ip为{}".format(ip))
                continue


def privox():
    n=0
    while True:
        # url = "http://kps.kdlapi.com/api/getkps/?orderid=953067577969172&num=3&pt=1&format=json&sep=1"
        url = "http://tpv.daxiangdaili.com/ip/?tid=559266265573959&num=1&delay=5"
        # url = "http://api.ip.data5u.com/dynamic/get.html?order=cd7db55d6b04cbe59317488e1738cc11&json=1&random=true&sep=3"
        # url = "http://api.ip.data5u.com/dynamic/get.html?order=cd7db55d6b04cbe59317488e1738cc11&json=1&random=true&sep=3"
        # "http://api.ip.data5u.com/dynamic/get.html?order=98277143c71adf09aa3f523fcf455ee2&json=1&random=true&sep=3"
        try:
            html = requests.get(url).text
            print("请求回来的ip{}".format(html))
            return html
        except Exception as e:
            print("kkkkkkkkkkkkkk" + str(e))
            continue
        except requests.exceptions.ConnectionError:
            print("ssssssssssss")
            n = n + 1
            if n == Config.REQUEST_NUM:
                break
            continue
        except requests.exceptions.Timeout:
            print("ssssssssssss")
            n = n + 1
            if n == Config.REQUEST_NUM:
                break
            continue

    # url = "http://tvp.daxiangdaili.com/ip/?tid=559266265573959&num=1"
    # html = requests.get(url).text
    # lists = res.get('proxy_list')
    # privoxys = (random.choice(lists))

if __name__ == "__main__":
    logger = logger("feixiaohao.log")
    logger.info("****************************{}**************************".format(Utils.getCurFullDay()))
    ppp = []
    requests_url = "https://dncapi.bqiapp.com/api/coin/web-coinrank?page={}&type=0&pagesize=100&webp=1"
    curr_list = {}
    # ip = privox()
    # print(ip)
    A = Utils.getCurFullDay()
    select_logic = Logic("finance_data")
    start = time.clock()
    logic = FeixiaohaoLogic()
    currencys = select_logic.get_currenies()
    if currencys != {}:
        for i in currencys:
            id = i.get("id")
            name = i.get("name")
            if str(name).upper() in curr_list.keys():
                continue
            curr_list[str(name).upper()] = id
    for i in range(1,30):
        news = Feixiaohao({"url":requests_url.format(str(i))})
        news.start()









