#! /usr/bin env python
# coding:utf-8
# 业务逻辑

# import Config
import Config
import pymysql
import json
from Logic import Logic
import Utils as Utils
from news.Exceptions_class import ExceptionCls
import traceback
class LogicCls_zb(Logic):
    def __init__(self):
        Logic.__init__(self, "finance")

    def importData(self, rows):
        sql = ""

        # writer = {u'共享财经-头条': "http:daka.png"}
        if 0 == len(rows):
            return
        siteInfo = self.getSiteByName(rows[0].get("site_info"))
        categoryInfo = self.getCategoryByName(rows[0].get("category_info"))

        if 0 == len(siteInfo):
            raise ExceptionCls(Config.KD_ERROR, ["no set siteinfo!"])

        for i in rows:
            newsid = i.get('newsid', '')
            siteid = siteInfo.get("id", '')
            categoryid = categoryInfo.get("id", '')
            articleContent = pymysql.escape_string(str(i.get('content', '')))

            # res = self.getNewsByNewsid(newsid)
            # if res:
            #     # 如果文章已存在，不做操作
            #     continue
                # 插入作者以及作者的图片
            resId = self.getNickId(i.get('author'))
            kol_id = resId.get('id', '')
            try:
                if kol_id:
                    author_imgs = self.getAuthor(i.get('author',))
                    author_img = author_imgs.get('img', '')
                    # insert
                    valuesTuple = (
                        i.get('title', ''),
                        i.get('cover', ''),
                        kol_id,
                        i.get('author', ),
                        i.get("summary"),
                        i.get("publish_time"),
                        i.get("publish_time")

                    )
                    values = "'%s','%s','%s','%s','%s','%s','%s'" % valuesTuple
                    sql = "insert into %s (" \
                          "`title`," \
                          "`cover`," \
                          "`kol_id`," \
                          "`kol_name`," \
                          "`summary`," \
                          "`create_time`," \
                          "`update_time`" \
                          ") values (%s)" % (
                              Config.TableMap.get('FLASH'), values)
                    Utils.D("Insert: %s" % sql)
                    self.cursor.execute(sql)
                    last_id = self.cursor.lastrowid
                    for detial_one in i.get("detail_html"):
                        articleContent = pymysql.escape_string(str(detial_one.get('content', '')))
                        # 插入文章内容
                        sql = "insert into %s (`flash_id`, `content`,`author`,`cover`,`create_time`,`update_time`) values (%s, '%s', '%s', '%s', '%s', '%s')" % (
                            Config.TableMap.get("FLASH_DETAIL"), last_id, articleContent, detial_one.get("author"), detial_one.get("author_imgs"), detial_one.get("dates"), detial_one.get("dates"))
                        Utils.D("Insert: %s" % sql)
                        self.cursor.execute(sql)

                else:
                    kol_id = self.insertAuthorImg(i.get("author",), i.get("author_img",))
                    # insert
                    valuesTuple = (
                        i.get('title', ''),
                        i.get('cover', ''),
                        kol_id,
                        i.get('author', ),
                        i.get("summary"),
                        i.get("publish_time"),
                        i.get("publish_time")

                    )
                    values = "'%s','%s','%s','%s','%s','%s','%s'" % valuesTuple
                    sql = "insert into %s (" \
                          "`title`," \
                          "`cover`," \
                          "`kol_id`," \
                          "`kol_name`," \
                          "`summary`," \
                          "`create_time`," \
                          "`update_time`" \
                          ") values (%s)" % (
                              Config.TableMap.get('FLASH'), values)
                    Utils.D("Insert: %s" % sql)
                    self.cursor.execute(sql)

                    last_id = self.cursor.lastrowid
                    print(last_id)
                    for detial_one in i.get("detail_html"):
                        articleContent = pymysql.escape_string(str(detial_one.get('content', '')))
                        # 插入文章内容
                        sql = "insert into %s (`flash_id`, `content`,`author`,`cover`,`create_time`,`update_time`) values (%s, '%s', '%s', '%s', '%s', '%s')" % (
                            Config.TableMap.get("FLASH_DETAIL"), last_id, articleContent, detial_one.get("author"), detial_one.get("author_imgs"),detial_one.get("dates"),detial_one.get("dates"))
                        Utils.D("Insert: %s" % sql)
                        self.cursor.execute(sql)
            except:
                print(traceback.format_exc())
        self.db.commit()

