#encoding:utf8
import json
import random
# from Config import author_img
from lxml import etree
from news.request_class import ReqCls,req
from news.Logic_zbclass import LogicCls_zb
from Utils import md5
import threading
import queue
import time
from logger_file import logger
from Utils import date, getCurFullDay
import requests
class HXCJ():
    def __init__(self):
        self.headers = {
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36"
        }
        self.newsinfo_site = "金色财经"
        self.newsinfo_category = "新闻-直播"
        self.rows = []
        self.reqs = req()
        self.logic = LogicCls_zb()
        self.q = queue.Queue()
        self.list_end = True
        self.num = 0
        self.number = 0
        self.logger = logger("jscj_zb.log")
        self.detail_url = "https://api.jinse.com/cmi-api/v1/live_broadcast_feed/getList?id={}&limit=20&last_id=0&direction=1&page=&_source=m"

    def parse_detail(self, i):
        item = {}
        try:
            title = i['title']
            item['title'] = title
            item["newsid"] = md5(title.encode('utf8'))
        except:
            return "fail"
        try:
            item['author'] = i['author']
            # item['author_img'] = i['extra']['author_img']
        except:
            item['author'] = "金色财经-直播"
            # item['author_img'] = random.choice(author_img)
        try:
            publish_time = date(i['created_at'])
            item['publish_time'] = publish_time
        except:
            item['publish_time'] = getCurFullDay()
        item['site_info'] = self.newsinfo_site
        item['category_info'] = self.newsinfo_category
        item['reflink'] = i["url"]
        response = requests.get(self.detail_url.format(i['id']), headers=self.headers, ).text
        print(response)
        if response == "fail":
            return "fail"
        jsondata = json.loads(response)
        contents = "<div>"
        if jsondata.get("data").get("count") == 0:
            return "fail"
        detail_html = []
        for j in jsondata.get("data").get("list"):
            item_detail = {}
            content = j.get("content")
            try:
                dates = date(j.get("created_at"))
                # print(dates)
            except:
                dates = getCurFullDay()
            try:
                author = (j.get("publisher_info").get("nickname"))
                author_imgs = (j.get("publisher_info").get("images"))[0] + "_image105.png"

            except:
                author = "金色财经"
                author_imgs = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADgAAAA4CAYAAACohjseAAABOUlEQVR4Ae3XgUYFQRTG8S8UogRAgAggPUdQ6Rl6gp4gBEkQ50xKJFKpN0gCIO5FBEKgAMzZW5fKNAuIG2Z2Md39Dn8cOOsH1qDt8YpdE3x6QUjJFB+VYgt/jCkuku4Jvr1ir3WgCYZJuN89Y8QEh8mceyZ4bR+o+MoFmuJlJPASU5n33ggksDtAAgkkkEACCSTQK3wD4FPxQHPYNEUv9piU4MEUG4UDm0/XgAQSODjCclxOTXGd2FVsvXigCW7rJStFVX+8bKCinw2MhTPMEkgggQQSmBmBBBJIIIG9RsBjzJQNFGzn4kxx9y8evO+C+eEhFpNyWAgBEx140RNIIIEnmMsGBofpgcOKd1gtMsGaCc6b/Adv6qX88oF9AkuNQAIJJJBAAgkk8H6sgZViyRT7sYNxK9p2fgAvyTZY27oGtgAAAABJRU5ErkJggg=="
            item_detail["author"] = author
            item_detail["author_imgs"] = author_imgs
            item_detail["dates"] = dates
            content_html = etree.HTML(content)
            content = etree.tostring(content_html, method='html')
            item_detail["content"] = content
            detail_html.append(item_detail)
        item['detail_html'] = detail_html
        item["summary"] = ""
        item['cover'] = i['posters_image']
        item['source'] = "金色财经"
        return item

    def start(self):
        # t1 = threading.Thread(target=self.get_detail_url)
        # t1.start()\
        import requests
        # response = self.reqs.req_html("https://m.jinse.com/news/blockchain/596876.html", self.headers, "GET")
        # html = etree.HTML(response.text)
        # content = html.xpath('//section[@class="js-article-content"]')
        # print(content)
        self.parse()
        self.list_end = False

    def parse(self):

        response = self.reqs.req_html("https://api.jinse.com/cmi-api/v1/live_broadcasts?broadcast_id=0&limit=20&_source=m", self.headers, "GET")
        if response == "fail":
            return
        jsondata = json.loads(response)

        for i in jsondata.get("data").get("list"):
            items = (self.parse_detail(i))
            print(items)
            if items == "fail":
                print("jinru fdailyemian")
                print(items)
                continue
            self.rows.append(items)
            self.number += 1
        print("本次金色财经直播抓{}".format(str(self.number)))
        self.logger.info("本次金色财经直播抓{}".format(str(self.number)))

        self.logic.importData(self.rows)

if __name__ == '__main__':
    p = HXCJ()
    p.start()

