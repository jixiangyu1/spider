#encoding:utf8
import json
import random
import re

from Config import author_img
from lxml import etree
from news.request_class import ReqCls,req
from news.Logic_class import LogicCls
from Utils import md5, getCurDay
import threading
import queue
import time
from logger_file import logger
from Utils import date
class HXCJ():
    def __init__(self):
        self.headers = {
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36"
        }
        # self.req_cls = ReqCls("https://www.huoxing24.com/", self.headers)
        self.newsinfo_site = "金色财经"
        self.newsinfo_category = "快讯-金色财经"
        self.rows = []
        self.reqs = req()
        self.logic = LogicCls()
        self.q = queue.Queue()
        self.list_end = True
        self.num = 0
        self.number = 0
        self.logger = logger("jscj_kx.log")
        self.detail_url = "https://api.jinse.com/v4/live/list?reading=false&sort=&flag=down&id=0&limit=20&_source=m"

    def req_detail(self, response):
        # print(response)
        detail_html = json.loads(response)
        for detail in detail_html.get("list")[0].get("lives"):
            item = {}
            try:
                reqs_co = re.compile(r'【(.*?)】', re.S)
                title = reqs_co.findall(detail.get("content"))
                print(title)
                item['title'] = title[0]
                item["newsid"] = md5(title[0].encode('utf8'))
            except:
                print("获取标题出错")
                continue
            try:
                content = "<div><p>"  + detail.get("content") + "</p></div>"
                content_html = etree.HTML(content)
                content = etree.tostring(content_html, method='html')
                item["content"] = content
            except:
                print("获取内容失败")
                continue
            try:

                item['author'] = "金色财经-快讯"
                item['author_img'] = random.choice(author_img)
            except:
                item['author'] = "金色财经-快讯"
                item['author_img'] = random.choice(author_img)
            try:
                item['publish_time'] = date(detail.get("created_at"))
            except:
                item['publish_time'] = ""
            item['site_info'] = self.newsinfo_site
            item['category_info'] = self.newsinfo_category
            item['reflink'] = self.detail_url
            item['cover'] = ""
            item['source'] = "金色财经-快讯"
            try:
                lihao  =detail.get("up_counts")
                item["lihao"] = lihao
            except:
                item["lihao"] = "-"
            try:
                likong = detail.get("down_counts")
                item["lihao"] = likong
            except:
                item["likong"] = "-"
                self.number += 1

            # print(item)
            self.rows.append(item)
        self.logger.info("本次金色财经快讯抓{}".format(str(self.number)))
        self.logic.importData(self.rows)

    def parse(self):
        response = self.reqs.req_html("https://api.jinse.com/v4/live/list?reading=false&sort=&flag=down&id=0&limit=20&_source=m", self.headers, "GET")
        if response == "fail":
            return

        return self.req_detail(response)

if __name__ == '__main__':
    p = HXCJ()
    p.parse()
    # p = "【Twitter已与对冲基金Elliott等公司达成新交易 Jack Dorsey将继续担任推特CEO】Twitter已与投资公司Elliott Management和Silver Lake达成一笔交易。该笔交易是在对冲基金Elliott创始人计划解除杰克•多尔西(Jack Dorsey)的Twitter首席执行官职务之后。而该笔交易达成后，不会改变Jack Dorsey担任Twitter首席执行官的角色。（CNBC）"
    # reqs = re.compile(r'【(.*?)】', re.S)
    # print(reqs.findall(p))