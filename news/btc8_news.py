#encoding:utf8
import json
import random
from Config import author_img
from lxml import etree
from news.request_class import ReqCls,req
from news.Logic_class import LogicCls
from Utils import md5
import threading
import queue
import time
from logger_file import logger
from Utils import date
class BTC():
    def __init__(self):
        self.headers = {
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36",
            "referer": "https://www.8btc.com/flash",
            "origin": "https://www.8btc.com",
            "source-site": "8btc",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "cross-site",
            "authority": "app.blockmeta.com",
            "path": "/w1/news/list?num=20&cat_id=4481&page=1",
            "scheme": "https",
            "accept": "application/json, text/plain, */*",
            "from": "web",
        }
        self.newsinfo_site = "巴比特"
        self.newsinfo_category = "快讯-巴比特"
        self.rows = []
        self.reqs = req()
        self.logic = LogicCls()
        self.q = queue.Queue()
        self.list_end = True
        self.num = 0
        self.number = 0
        self.logger = logger("btc_news.log")

    def parse_detail(self, i):
        item = {}
        try:
            title = i['title']
            item['title'] = title
            item["newsid"] = md5(title.encode('utf8'))
        except:
            return "fail"
        try:
            item['author'] = str(i['source']['name']).replace("微博@", "")
            item['author_img'] = random.choice(author_img)
        except:
            item['author'] = "区块链-巴比特"
            item['author_img'] = random.choice(author_img)
        try:
            publish_time = i["post_date_format"]
            item['publish_time'] = publish_time
        except:
            item['publish_time'] = ""
        item['site_info'] = self.newsinfo_site
        item['category_info'] = self.newsinfo_category

        item['reflink'] = "https://www.8btc.com/flash"

        item["content"] = i.get("content")


        item['cover'] = i['image']
        item['source'] = "巴比特"
        return item

    def start(self):
        self.parse()
        self.list_end = False

    def parse(self):
        import requests
        # response = self.reqs.req_html("https://app.blockmeta.com/w1/news/list?num=20&cat_id=4481&page=1", self.headers, "GET")
        response = requests.get("https://app.blockmeta.com/w1/news/list?num=20&cat_id=4481&page=1", headers=self.headers).text
        print(response)
        if response == "fail":
            return
        jsondata = json.loads(response)
        for i in jsondata.get("list"):
            print(i)
            items = self.parse_detail(i)
            print(items)
            self.rows.append(items)
            self.number += 1
        self.logger.info("本次巴比特资讯抓{}".format(str(self.number)))
        self.logic.importData(self.rows)

if __name__ == '__main__':
    p = BTC()
    p.start()
