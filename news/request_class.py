#encoding:utf8
import json

import requests
import time
import queue
from lxml import etree
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

class getProxy():
    def __init__(self):
        # self.redis_con = RedisConnect()
        self.api = "http://39.99.153.57:5000/proxy?count=30"
        self.queue = queue.Queue()
        # self.logger = logger("getPorxy.log")
        self.flag = True

    def Get_NewProxy(self, times=0):

        """从代理池中获取代理
        """
        if times >= 6:
            return

        try:
            proxy = requests.get(self.api, timeout=25).text
            proxy_list = eval(proxy)
            proxies_list = list()
            if type(proxy_list) is dict:
                time.sleep(10)
                times += 1
                self.Get_NewProxy()
            if proxy_list[0]["ip"] is None or proxy_list[0]["ip"] == "None":
                time.sleep(30)
                times += 1
                self.Get_NewProxy()
            else:
                for proxy_msg in proxy_list:
                    proxies_list.append(proxy_msg)
                    self.queue.put(proxy_msg)
                self.flag = True
            if self.queue.empty():
                times += 1
                self.Get_NewProxy()
        except Exception as e:
            # self.logger.info("请求接口失败 {}".format(e))
            time.sleep(5)
            self.Get_NewProxy()

    def retProxy(self):
        if self.queue.empty():
            if self.flag == True:
                self.flag = False
                self.Get_NewProxy()
            return "fail"

        else:
            return self.queue.get()

getProxy = getProxy()
class req():
    def __init__(self):
        # self.logger = logger("{}.log".format(log_name))
        self.s = requests.session()

    def req_html(self, url, headers, method, parame=None, parses=None):
        print("====")
        without_title = 0

        except_flag = 0
        while True:
            try:
                proxies = getProxy.retProxy()
                print(proxies)

                if proxies != "fail":
                    if str(url).split(':')[0] == "http":
                        privoxy = {"http": "http://{}".format(proxies.get("ip")), "https": "https://{}".format(proxies.get("ip"))}
                    else:
                        privoxy = {"http": "http://{}".format(proxies.get("ip")), "https": "https://{}".format(proxies.get("ip"))}
                #     #proxies=privoxy,proxies=privoxy, verify=False, proxies=privoxy, verify=False,
                #     ip_forward = "forward.xdaili.cn"
                #     port = "80"
                #     ip_port = ip_forward + ":" + port
                #     proxy = {"http": "http://" + ip_port, "https": "https://" + ip_port}
                    if method == 'GET':

                        #百度百家详情页搜索
                        if parame == 1:
                            pass

                        else:
                            #headers['Authorization'] = auth_proxy()
                            #print(proxy)
                            print("zhengzai qingiqu url {}".format(url))
                            response = self.s.get(url=url, headers=headers, proxies=privoxy, verify=False, timeout=15, allow_redirects=True)
                            # print(response.text)
                            # print(response.status_code)
                            if response.status_code == 200:
                                if "huoxing24" in url:
                                    # print(response.text)
                                    html = etree.HTML(response.text)
                                    # print( len(('//div[@class="index-news-list"]/a/@href')))
                                    if len(html.xpath( '//div[@class="news-details-content simditor"]/h1/text() | //div[@class="index-news-list"]/a/@href | //div[@class="video-box"] | //div[@class="cont-item"] | //div[@class="cont"] | //div[@class="news-details-content"]')) > 0 or len(html.xpath('//div[@class="flash-list-item-wrapper"]')) > 0:
                                        return (response.text)
                                    else:
                                        without_title += 1
                                        if without_title == 10:
                                            return "fail"
                                        print("没有匹配到盒子 ")
                                        continue
                                elif "www.mclouds.io/" in url:
                                    return "fail"

                                elif "feed.mix" in url:
                                    jsonata = json.loads(response.text)
                                    if jsonata.get("result").get("status").get("code") == 0:
                                        return response.text

                                elif "bishijie" in url:
                                    html = etree.HTML(response.text)
                                    if len(html.xpath('//ul[@class="newscontainer"]/li')) > 0:
                                        return response.text

                                elif "m.jinse.com" in url:
                                    print("详情页匹配")
                                    html = etree.HTML(response.text)
                                    if len(html.xpath('//section[@class="js-article-content"]')) > 0:
                                        return response.text

                                elif "api.jinse.com" in url:
                                    # print(type(response.text))
                                    jsondata = json.loads(response.text)
                                    if "count" in jsondata.keys():
                                        return response.text
                                    elif jsondata['status_code'] == 0:
                                        return response.text

                                elif "live_broadcast" in url:
                                    pass

                                elif "8btc.com/video" in url or "8btc.com/article/" in url:
                                    html = etree.HTML(response.text)
                                    if len(html.xpath('//*[@id="app"]/div[2]/div[1]/div/div[2]/div[1]/iframe/@src | //div[@class="video-item"] | //div[@class="bbt-html"]')) > 0:
                                        return response.text

                                elif "app.blockmeta.com" in url:
                                    # print(type(response.text))
                                    jsondata = json.loads(response.text)

                                    if jsondata['data'] != []:
                                        return response.text


                                elif "finance.sina" in url:
                                    html = etree.HTML(response.text)
                                    if len(html.xpath("//h1[@class='main-title']/text()")) > 0:
                                        return response

                                elif "finance.eastmoney" in url:
                                    html = etree.HTML(response.text)
                                    print(html.xpath("//ul[@class='list list_side'] | //div[@id='ContentBody']"))
                                    if len(html.xpath("//ul[@class='list list_side']/li | //div[@id='ContentBody']")) > 0:
                                        return response
                                    else:
                                        without_title += 1
                                        if without_title == 10:
                                            return "fail"
                                        print("没有匹配到盒子 ")
                                        continue
                                elif 'life.eastmoney' in url or 'stock.eastmoney' in url:
                                    return 'fail'

                            else:
                                except_flag += 1
                                if except_flag == 10:
                                    print("请求的url{} 返回的状态码 {} 请求的次数 {} 返回的response ".format(url, response.status_code, except_flag))
                                    #("请求的url{} 返回的状态码 {} 请求的次数 {} 返回的response ".format(url, response.status_code, except_flag))
                                    return "fail"

                                continue

                    else:
                        time.sleep(1)

            except Exception as e:
                # self.logger.info("请求列表页 url {} 异常 {} ".format(url, e))
                except_flag += 1
                if except_flag == 20:
                    print("出现异常错误五次 url {} 异常 {}".format(url, e))
                    return "fail"
                continue

            time.sleep(5)


class ReqCls():
    def __init__(self):

        self.except_num = 0
        self.s = requests.session()

    def req_html(self, url, headers, method, parame=None, parses=None):
        print("====")
        without_title = 0

        except_flag = 0
        while True:
            try:
                proxies = getProxy.retProxy()
                print(proxies)

                if proxies != "fail":
                    if str(url).split(':')[0] == "http":
                        privoxy = {"http": "http://{}".format(proxies.get("ip")), "https": "https://{}".format(proxies.get("ip"))}
                    else:
                        privoxy = {"http": "http://{}".format(proxies.get("ip")), "https": "https://{}".format(proxies.get("ip"))}
                #     #proxies=privoxy,proxies=privoxy, verify=False, proxies=privoxy, verify=False,
                #     ip_forward = "forward.xdaili.cn"
                #     port = "80"
                #     ip_port = ip_forward + ":" + port
                #     proxy = {"http": "http://" + ip_port, "https": "https://" + ip_port}
                    if method == 'GET':

                        #百度百家详情页搜索
                        if parame == 1:
                            pass

                        else:
                            #headers['Authorization'] = auth_proxy()
                            #print(proxy)
                            print("zhengzai qingiqu url {}".format(url))
                            response = self.s.get(url=url, headers=headers, proxies=privoxy, verify=False, timeout=15, allow_redirects=True)
                            # print(response.text)
                            # print(response.status_code)
                            if response.status_code == 200:
                                jsondata = json.loads(response.text)
                                if "data" in jsondata:
                                    return response.text
                            else:
                                except_flag += 1
                                if except_flag == 10:
                                    print("请求的url{} 返回的状态码 {} 请求的次数 {} 返回的response ".format(url, response.status_code, except_flag))
                                    #("请求的url{} 返回的状态码 {} 请求的次数 {} 返回的response ".format(url, response.status_code, except_flag))
                                    return "fail"

                                continue

                    else:
                        time.sleep(1)

            except Exception as e:
                print("请求列表页 url {} 异常 {} ".format(url, e))
                except_flag += 1
                if except_flag == 20:
                    print("出现异常错误五次 url {} 异常 {}".format(url, e))
                    return "fail"
                continue

            time.sleep(5)


