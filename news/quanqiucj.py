#encoding:utf8
import random

import re

from Config import author_img
from lxml import etree
from news.request_class import ReqCls,req
from news.Logic_class import LogicCls
from Utils import md5, getCurFullDay
import threading
import requests
import queue
import time
from logger_file import logger
class HXCJ():
    def __init__(self):
        self.headers = {
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36"
        }
        # self.req_cls = ReqCls("https://www.huoxing24.com/", self.headers)
        self.newsinfo_site = "全球财经"
        self.newsinfo_category = "财经-全球财经"
        self.rows = []
        self.reqs = req()
        self.logic = LogicCls()
        self.q = queue.Queue()
        self.list_end = True
        self.num = 0
        self.number = 0
        self.logger = logger("qqcj.log")
        self.thread_num = 0
    def threds(self):
        if self.q.empty() != True:
            result = self.q.get()
            i = result[0]
            self.req_detail(i)

        else:
            self.num += 1
            print("队列的长度为 {} {}".format(self.q.qsize(), self.num))
            if self.num == self.thread_num:
                self.logger.info("本次火星财经抓{}".format(str(self.number)))
                print("本次火星财经抓{}".format(str(self.number)))
                self.logic.importData(self.rows)
            exit()

    def req_detail(self, i):
        item = {}
        detail_html = ""
        # response_detail = self.reqs.req_html(i, self.headers, "GET")
        response_detail = requests.get(i, headers=self.headers)
        if response_detail == "fail":
            self.threds()
        code = (response_detail.encoding)
        html = response_detail.text
        html = html.encode(code)
        response_detail = html.decode("utf8")


        try:
            detail_html = etree.HTML(response_detail)
        except:
            self.threds()
        try:
            title = detail_html.xpath("/html/body/div[1]/div/div[3]/div[1]/div[2]/div[1]/h1/text()")
            item['title'] = title[0]
            item["newsid"] = md5(title[0].encode('utf8'))
        except:
            self.threds()
        try:
            content = detail_html.xpath("//div[@id='ContentBody']")[0]
            content = etree.tostring((content), method='html')
            content = str(content).replace("\\n","").replace("\\r","")
            item["content"] = content
        except:
            self.threds()
        try:
            author = detail_html.xpath("//div[@class='source data-source']/@data-source | /html/body/div[1]/div/div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/text()")

            item['author'] = str(author[0]).strip().replace("作者：", "")
            item['author_img'] = random.choice(author_img)
            print(author)
        except:
            item['author'] = "财经-全球财经"
            item['author_img'] = random.choice(author_img)

        try:
            item['publish_time'] = getCurFullDay()
        except:
            item['publish_time'] = getCurFullDay()
        item['site_info'] = self.newsinfo_site
        item['category_info'] = self.newsinfo_category
        item['reflink'] = i
        item['cover'] = ""
        item['source'] = "全球财经"
        print(item)
        self.rows.append(item)
        self.number+=1
        self.threds()


    def get_detail_url(self):
        time.sleep(15)
        print("程序启动")
        while True:
            print("目前的q {} {}".format(self.q.empty(), self.list_end))
            n = 0
            while not self.q.empty():
                result = self.q.get()
                t = threading.Thread(target=self.req_detail, args=(result[0],))
                t.start()

                n += 1
                if n == self.thread_num:
                    return

            if self.q.empty() == True and self.list_end == False:
                print("进入结束页面")
                return "1"

            elif self.q.empty() == True and self.list_end == True:
                print("正在等待")
                time.sleep(2)

    def start(self):
        t1 = threading.Thread(target=self.get_detail_url)
        t1.start()

        self.parse()
        self.list_end = False

    def parse(self):
        import requests
        response = self.reqs.req_html("http://finance.eastmoney.com/", self.headers, "GET")
        # response = requests.get("http://finance.eastmoney.com/", headers=self.headers)
        if response == "fail":
            return
        # print(response)
        html = etree.HTML(response.text)
        all_div = html.xpath("//ul[@class='list list_side']/li")
        for k in all_div:
            all_href = k.xpath("./div[2]/a/@href")[0]
            print(all_href)
            self.q.put((all_href,))
            print("存入对接")
            self.thread_num += 1

if __name__ == '__main__':
    p = HXCJ()
    p.start()
