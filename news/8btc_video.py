#encoding:utf8
import random
from Config import author_img
from lxml import etree
from news.request_class import ReqCls,req
from news.Logic_class import LogicCls
from Utils import md5
import threading
import queue
import time
from logger_file import logger
class HXCJ():
    def __init__(self):
        self.headers = {
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36",
        }
        self.newsinfo_site = "巴比特"
        self.newsinfo_category = "新闻-视频"
        self.rows = []
        self.reqs = req()
        self.logic = LogicCls()
        self.q = queue.Queue()
        self.list_end = True
        self.num = 0
        self.number = 0
        self.logger = logger("bbt_video.log")

    def threds(self):
        if self.q.empty() != True:
            result = self.q.get()
            self.req_detail(result,)

        else:
            self.num += 1
            print("队列的长度为 {} {}".format(self.q.qsize(), self.num))
            if self.num == 30:
                self.logger.info("本次巴比特视频抓{}".format(str(self.number)))
                self.logic.importData(self.rows)
            exit()

    def req_detail(self, item):
        print("传到详情页 ")
        response_detail = self.reqs.req_html(item['reflink'], self.headers, "GET")
        if response_detail == "fail":
            self.threds()
        html = etree.HTML(response_detail)
        try:
            content = html.xpath('//*[@id="app"]/div[2]/div[1]/div/div[2]/div[1]/iframe/@src')
            item['content'] = content[0]
        except:
            self.threds()
        print(item)
        self.rows.append(item)
        self.number+=1
        self.threds()

    def get_detail_url(self):
        time.sleep(5)
        print("程序启动")
        while True:
            print("目前的q {} {}".format(self.q.empty(), self.list_end))
            n = 0
            while not self.q.empty():
                result = self.q.get()
                print("取出来的")
                print(result)
                t = threading.Thread(target=self.req_detail, args=(result,))
                t.start()

                n += 1
                if n == 30:
                    return

            if self.q.empty() == True and self.list_end == False:
                print("进入结束页面")
                return "1"

            elif self.q.empty() == True and self.list_end == True:
                print("正在等待")
                time.sleep(2)

    def start(self):
        t1 = threading.Thread(target=self.get_detail_url)
        t1.start()

        self.parse()
        self.list_end = False

    def parse_detail(self, i):
        print(i)
        item = {}
        try:
            item['title'] = i['title']
            item["newsid"] = md5(i['title'].encode('utf8'))
        except:
            return "fail"

        try:
            item['author'] = i['author_info']['display_name']
            item['author_img'] = i['author_info']['avatar']
        except:
            item['author'] = "巴比特-视频"
            item['author_img'] = random.choice(author_img)
        try:
            item['publish_time'] = i['post_date_format']
        except:
            item['publish_time'] = ""
        item['site_info'] = self.newsinfo_site
        item['category_info'] = self.newsinfo_category
        item['reflink'] = "https://www.8btc.com/video/" + str(i['id'])
        item['cover'] = i['image']
        item['source'] = "巴比特"
        return item

    def parse(self):
        import requests
        # response = self.reqs.req_html("https://www.8btc.com/video", self.headers, "GET")
        # if response == "fail":
        #     return
        response = requests.get("https://www.8btc.com/video", headers=self.headers).text

        import re
        import json
        recom = re.compile(r'window.__INITIAL_STATE__=(.*?);\(',re.S)
        pp = recom.search(response).group(1)
        jsons = (pp.replace(';(function(){var s;(s=document.currentScript||document.scripts[document.scripts.length-1]).parentNode.removeChild(s);}());', ''))
        print(jsons)
        jsondata = json.loads(jsons)
        for i in jsondata['video']:

            if i in ["digest", "btcwen", "chainge", "live", "review", "media", "miao"]:
                for j in jsondata['video'][i]['list']:
                    items = self.parse_detail(j)
                    if items == "fail":
                        continue
                    print("传进去的 ")
                    self.q.put(items)

if __name__ == '__main__':
    p = HXCJ()
    p.start()
