#! /usr/bin env python
# coding:utf-8
# 业务逻辑

# import Config
import Config
import pymysql
import json
import traceback
from Logic import Logic
import Utils as Utils
from news.Exceptions_class import ExceptionCls
from Utils import md5, getCurFullDay
class LogicCls(Logic):
    def __init__(self):
        Logic.__init__(self, "finance")

    def importData(self, rows):
        sql = ""

        # writer = {u'共享财经-头条': "http:daka.png"}
        if 0 == len(rows):
            return
        siteInfo = self.getSiteByName(rows[0].get("site_info"))
        categoryInfo = self.getCategoryByName(rows[0].get("category_info"))

        if 0 == len(siteInfo):
            raise ExceptionCls(Config.KD_ERROR, ["no set siteinfo!"])

        for i in rows:
            title = i.get("title")
            newsid = md5(str(title).replace("：","").replace("，","").replace(",", "").replace("。","").strip().encode('utf8'))
            Utils.D("newsid {}".format(newsid))
            if newsid == "":
                Utils.D("newsid {} None title {}".format(newsid, i.get("title")))
                newsid = md5(str(i.get("title")).replace("：","").replace("，","").replace(",", "").replace("。","").strip().encode('utf8'))
            siteid = siteInfo.get("id", '')
            categoryid = categoryInfo.get("id", '')
            articleContent = (str(i.get('content', '')).replace("\\n","").replace("\\r",""))
            articleContent = pymysql.escape_string(articleContent)


            res = self.getNewsByNewsid(newsid)
            if res:
                Utils.D("w文章存在")
                # 如果文章已存在，不做操作
                continue
                # 插入作者以及作者的图片
            if "微博@" in i.get("author") or "作者：" in i.get("author") or "微博¥" in i.get("author"):
                str(i.get("author")).replace("微博@", "").replace("作者：", "")
            resId = self.getNickId(i.get('author'))
            kol_id = resId.get('id', '')
            print(categoryid)
            top_category_id = self.getParentid(categoryid)
            print("dsgk", top_category_id)
            news_category_type = Config.parent_site.get(int(top_category_id))
            try:
                if kol_id:
                    author_imgs = self.getAuthor(i.get('author',))
                    author_img = author_imgs.get('img', '')
                    # insert
                    valuesTuple = (
                        newsid,
                        i.get('title', ''),
                        kol_id,
                        i.get('author',),
                        author_img,
                        i.get('reflink', ''),
                        i.get("source", ""),
                        siteid,
                        categoryid,
                        top_category_id,
                        0,
                        0,
                        i.get('cover', '')
                    )
                    values = "'%s','%s','%s','%s','%s','%s','%s',%s,%s,%s,%s,%s,'%s'" % valuesTuple
                    sql = "insert into %s (" \
                          "`newsid`," \
                          "`title`," \
                          "`kol_id`," \
                          "`author`," \
                          "`author_img`," \
                          "`reflink`," \
                          "`source`," \
                          "`siteid`," \
                          "`category_id`," \
                          "`top_category_id`," \
                          "`is_release`," \
                          "`is_verify`," \
                          "`cover`" \
                          ") values (%s)" % (
                              Config.TableMap.get('NEWS_TABLE'), values)
                    Utils.D("Insert: %s" % sql)
                    self.cursor.execute(sql)

                    news_lastrowid = self.cursor.lastrowid

                    # 插入文章内容
                    sql = "insert into %s (`news_id`, `content`) values (%s, '%s')" % (
                        Config.TableMap.get("NEWS_DATAS_TABLE"), self.cursor.lastrowid, articleContent)
                    Utils.D("Insert: %s" % sql)
                    self.cursor.execute(sql)

                    # 插入关联分类id
                    sql = "insert into %s (`data_id`, `category_id`, `type`, `create_time`) values (%s, %s, %s, '%s')" % (
                        "news_category", news_lastrowid, categoryid, news_category_type, getCurFullDay())
                    Utils.D("Insert: %s" % sql)
                    self.cursor.execute(sql)

                else:
                    kol_id = self.insertAuthorImg(i.get("author",), i.get("author_img",))
                    # insert
                    valuesTuple = (
                        newsid,
                        i.get('title', ''),
                        kol_id,
                        i.get('author', ),
                        i.get("author_img"),
                        i.get('reflink', ''),
                        i.get("source", ""),
                        siteid,
                        categoryid,
                        top_category_id,
                        0,
                        0,
                        i.get('cover', '')
                    )
                    values = "'%s','%s','%s','%s','%s','%s','%s',%s,%s,%s,%s,%s,'%s'" % valuesTuple
                    sql = "insert into %s (" \
                          "`newsid`," \
                          "`title`," \
                          "`kol_id`," \
                          "`author`," \
                          "`author_img`," \
                          "`reflink`," \
                          "`source`," \
                          "`siteid`," \
                          "`category_id`," \
                          "`top_category_id`," \
                          "`is_release`," \
                          "`is_verify`," \
                          "`cover`" \
                          ") values (%s)" % (
                              Config.TableMap.get('NEWS_TABLE'), values)
                    Utils.D("Insert: %s" % sql)
                    self.cursor.execute(sql)

                    news_lastrowid = self.cursor.lastrowid

                    # 插入文章内容
                    sql = "insert into %s (`news_id`, `content`) values (%s, '%s')" % (
                        Config.TableMap.get("NEWS_DATAS_TABLE"), self.cursor.lastrowid, articleContent)
                    Utils.D("Insert: %s" % sql)
                    self.cursor.execute(sql)

                    # 插入关联分类id
                    sql = "insert into %s (`data_id`, `category_id`, `type`, `create_time`) values (%s, %s, %s, '%s')" % (
                        "news_category", news_lastrowid, categoryid, news_category_type, getCurFullDay())
                    Utils.D("Insert: %s" % sql)
                    self.cursor.execute(sql)

            except:
                print(traceback.format_exc())

        self.db.commit()
