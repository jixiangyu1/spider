#encoding:utf8
import random
from Config import author_img
from lxml import etree
from news.request_class import ReqCls,req
from news.Logic_class import LogicCls
from Utils import md5
import threading
import queue
import time
from logger_file import logger
import requests
class HXCJ():
    def __init__(self):
        self.headers = {
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36"
        }
        # self.req_cls = ReqCls("https://www.huoxing24.com/", self.headers)
        self.newsinfo_site = "火星财经"
        self.newsinfo_category = "新闻-矿业"
        self.rows = []
        self.reqs = req()
        self.logic = LogicCls()
        self.q = queue.Queue()
        self.list_end = True
        self.num = 0
        self.number = 0
        self.logger = logger("hxcj_ky.log")
        self.thread_num = 0
    def threds(self):
        if self.q.empty() != True:
            result = self.q.get()
            i, j = result[0], result[1]
            self.req_detail(i, j)

        else:
            self.num += 1
            print("队列的长度为 {} {}".format(self.q.qsize(), self.num))
            if self.num == self.thread_num:
                self.logger.info("本次火星财经抓{}".format(str(self.number)))
                print("本次火星财经抓{}".format(str(self.number)))
                self.logic.importData(self.rows)
            exit()

    def req_detail(self, i, j):
        item = {}
        detail_html = ""
        # response_detail = self.reqs.req_html(i, self.headers, "GET")
        response_detail = requests.get(i, headers=self.headers).text
        if response_detail == "fail":
            self.threds()
        try:
            detail_html = etree.HTML(response_detail)
        except:
            self.threds()
        try:
            title = detail_html.xpath("//div[@class='news-details-content']/h1/text()")
            item['title'] = title[0]
            item["newsid"] = md5(title[0].encode('utf8'))
        except:
            print("fsedag")
            self.threds()
        try:
            content = detail_html.xpath("//div[@id='newsDetailsContent']/div")[0]
            content = etree.tostring(content, method='html')
            item["content"] = content
        except:
            print('asdgsdgdf')
            self.threds()
        try:
            author = detail_html.xpath("//div[@class='news-details-content simditor']/div[1]/a/text()")
            item['author'] = author[0]
            item['author_img'] = random.choice(author_img)
        except:
            item['author'] = "火星财经-矿业"
            item['author_img'] = random.choice(author_img)
        try:
            publish_time = detail_html.xpath("//div[@class='news-details-content simditor']/div[1]//time/text()")
            item['publish_time'] = publish_time[0]
        except:
            item['publish_time'] = ""
        item['site_info'] = self.newsinfo_site
        item['category_info'] = self.newsinfo_category
        item['reflink'] = i
        item['cover'] = j
        item['source'] = "火星财经"
        self.rows.append(item)
        self.number+=1
        self.threds()


    def get_detail_url(self):
        time.sleep(30)
        print("程序启动")
        while True:
            print("目前的q {} {}".format(self.q.empty(), self.list_end))
            n = 0
            while not self.q.empty():
                result = self.q.get()
                t = threading.Thread(target=self.req_detail, args=(result[0], result[1]))
                t.start()

                n += 1
                if n == self.thread_num:
                    return

            if self.q.empty() == True and self.list_end == False:
                print("进入结束页面")
                return "1"

            elif self.q.empty() == True and self.list_end == True:
                print("正在等待")
                time.sleep(2)

    def start(self):
        t1 = threading.Thread(target=self.get_detail_url)
        t1.start()

        self.parse()
        self.list_end = False

    def parse(self):
        response = requests.get("https://www.huoxing24.com/", headers=self.headers).text
        # response = self.reqs.req_html("https://www.huoxing24.com/", self.headers, "GET")
        if response == "fail":
            return
        # print(response)
        html = etree.HTML(response)
        all_div = html.xpath("//div[@id='newsBlock24']/div[1]")
        for k in all_div:
            all_href = k.xpath("./div/a/@href")
            all_cover = k.xpath("./div/a/div[1]/img/@data-src")
            print(len(all_href), len(all_cover))
            self.headers["referer"] = "https://www.huoxing24.com/"
            for i, j in zip(all_href, all_cover):
                print("存入对接")
                self.q.put((i, j))
                self.thread_num += 1

if __name__ == '__main__':
    p = HXCJ()
    p.start()
