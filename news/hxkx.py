#encoding:utf8
import random
from Config import author_img
from lxml import etree
from news.request_class import ReqCls,req
from news.Logic_class import LogicCls
from Utils import md5, getCurDay
import threading
import queue
import time
from logger_file import logger
class HXCJ():
    def __init__(self):
        self.headers = {
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36"
        }
        # self.req_cls = ReqCls("https://www.huoxing24.com/", self.headers)
        self.newsinfo_site = "火星财经"
        self.newsinfo_category = "快讯-火星财经"
        self.rows = []
        self.reqs = req()
        self.logic = LogicCls()
        self.q = queue.Queue()
        self.list_end = True
        self.num = 0
        self.number = 0
        self.logger = logger("hxcj_kx.log")

    def req_detail(self, response):
        # print(response)
        detail_html = etree.HTML(response)
        detail_html = detail_html.xpath('//div[@class="flash-list-content"]/div')
        for detail in detail_html:
            item = {}
            try:
                title = detail.xpath("./a/text()")

                item['title'] = title[0]
                item["newsid"] = md5(title[0].encode('utf8'))
            except:
                print("获取标题出错")
                continue
            try:
                content = detail.xpath("./h3/div[1]")
                content = etree.tostring(content[0], method='html')
                item["content"] = content
            except:
                print("获取内容失败")
                continue
            try:

                item['author'] = "火星财经-快讯"
                item['author_img'] = random.choice(author_img)
            except:
                item['author'] = "火星财经-快讯"
                item['author_img'] = random.choice(author_img)
            try:
                publish_time = detail.xpath("./div[1]/div[2]/text()")
                item['publish_time'] = getCurDay() + " " + publish_time[0]
            except:
                item['publish_time'] = ""
            item['site_info'] = self.newsinfo_site
            item['category_info'] = self.newsinfo_category
            item['reflink'] = "https://news.huoxing24.com/flash"
            item['cover'] = ""
            item['source'] = "火星财经快讯"
            try:
                lihao  =detail.xpath("./div[2]/p[1]/span[2]/text()")
                item["lihao"] = lihao[0]
            except:
                item["lihao"] = "-"
            try:
                likong = detail.xpath("./div[2]/p[2]/span[2]/text()")
                item["likong"] = likong[0]
            except:
                item["likong"] = "-"
                self.number += 1

            # print(item)
            self.rows.append(item)
        self.logger.info("本次火星财经抓{}".format(str(self.number)))
        self.logic.importData(self.rows)

    def parse(self):
        response = self.reqs.req_html("https://news.huoxing24.com/flash", self.headers, "GET")
        if response == "fail":
            return

        return self.req_detail(response)

if __name__ == '__main__':
    p = HXCJ()
    p.parse()
