#encoding:utf8
import random
from Config import author_img
from lxml import etree
from news.request_class import ReqCls,req
from news.Logic_class import LogicCls
from Utils import md5, getCurDay, getCurFullDay
import threading
import queue
import time
from logger_file import logger
class HXCJ():
    def __init__(self):
        self.headers = {
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36"
        }
        # self.req_cls = ReqCls("https://www.huoxing24.com/", self.headers)
        self.newsinfo_site = "币世界"
        self.newsinfo_category = "快讯-币世界"
        self.rows = []
        self.reqs = req()
        self.logic = LogicCls()
        self.q = queue.Queue()
        self.list_end = True
        self.num = 0
        self.number = 0
        self.logger = logger("bishijie_kx.log")

    def req_detail(self, response):
        # print(response)
        detail_html = etree.HTML(response)
        detail_html = detail_html.xpath('//ul[@class="newscontainer"]/li')
        for detail in detail_html:
            item = {}
            try:
                title = detail.xpath("./div/a/h3/text()")

                item['title'] = str(title[0]).strip()
                item["newsid"] = md5(title[0].encode('utf8'))
            except:
                print("获取标题出错")
                continue
            try:
                content = detail.xpath("./div/div[1]")
                content = etree.tostring(content[0], method='html')
                item["content"] = content
            except:
                print("获取内容失败")
                continue
            try:

                item['author'] = "币世界-快讯"
                item['author_img'] = random.choice(author_img)
            except:
                item['author'] = "币世界-快讯"
                item['author_img'] = random.choice(author_img)
            try:
                # publish_time = detail.xpath("./div/a/h3/span/text()")
                item['publish_time'] = getCurFullDay()
            except:
                item['publish_time'] = getCurFullDay()
            item['site_info'] = self.newsinfo_site
            item['category_info'] = self.newsinfo_category
            item['reflink'] = "https://www.bishijie.com/kuaixun"
            item['cover'] = ""
            item['source'] = "币世界快讯"
            try:
                lihao  =detail.xpath("./div/div[2]/div/div[1]/div[3]/text()")
                item["lihao"] = str(lihao[0]).strip()
            except:
                item["lihao"] = "-"
            try:
                likong = detail.xpath("./div/div[2]/div/div[2]/div[3]/text()")
                item["likong"] = str(likong[0]).strip()
            except:
                item["likong"] = "-"
                self.number += 1

            print(item)
            self.rows.append(item)
        self.logger.info("本次币世界抓{}".format(str(self.number)))
        self.logic.importData(self.rows)

    def parse(self):
        response = self.reqs.req_html("https://www.bishijie.com/kuaixun", self.headers, "GET")
        if response == "fail":
            return

        return self.req_detail(response)

if __name__ == '__main__':
    p = HXCJ()
    p.parse()
