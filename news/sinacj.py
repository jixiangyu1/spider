#encoding:utf8
import json
import random
from Config import author_img
from lxml import etree
from news.request_class import ReqCls,req
from news.Logic_class import LogicCls
from Utils import md5
import threading
import queue
import time
import io
from logger_file import logger
import sys
import requests
sys.stdout = io.TextIOWrapper(sys.stdout.buffer,encoding='utf-8')
class HXCJ():
    def __init__(self):
        self.headers = {
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36"
        }
        # self.req_cls = ReqCls("https://www.huoxing24.com/", self.headers)
        self.newsinfo_site = "新浪财经"
        self.newsinfo_category = "新闻-财经"
        self.rows = []
        self.reqs = req()
        self.logic = LogicCls()
        self.q = queue.Queue()
        self.list_end = True
        self.num = 0
        self.number = 0
        self.logger = logger("sinacj.log")


    def threds(self):
        if self.q.empty() != True:
            result = self.q.get()
            i = result
            self.req_detail(i)

        else:
            self.num += 1
            print("队列的长度为 {} {}".format(self.q.qsize(), self.num))
            if self.num == 30:
                self.logger.info("本次新浪财经抓取 {}".format(self.number))
                print("存入mysql")
                self.logic.importData(self.rows)
            exit()

    def req_detail(self, i):
        item = {}
        detail_html = ""
        # response_detail = self.reqs.req_html(i, self.headers, "GET")
        response_detail = requests.get(i, headers=self.headers)
        if response_detail == "fail":
            self.threds()
        try:
            code = (response_detail.encoding)
            html = response_detail.text
            html =html.encode(code)
            response_detail = html.decode("utf8")
            detail_html = etree.HTML(response_detail)
        except:
            self.threds()
        try:
            title = detail_html.xpath("//h1[@class='main-title']/text()")
            item['title'] = title[0]
            item["newsid"] = md5(title[0].encode('utf8'))
        except:
            # print('titlr ex')
            self.threds()
        try:
            content = detail_html.xpath("//div[@class='article']")[0]
            content = etree.tostring(content, method='html')
            item["content"] = content
        except:
            # print('conten ex')
            self.threds()
        try:
            author = detail_html.xpath("//div[@id='top_bar_wrap']/div[1]/div//div[2]/span[2]/text()")
            item['author'] = author[0]
            item['author_img'] = random.choice(author_img)
        except:
            item['author'] = "新浪财经"
            item['author_img'] = random.choice(author_img)
        try:
            publish_time = detail_html.xpath("//div[@id='top_bar_wrap']/div[1]/div//div[2]/span[1]/text()")
            item['publish_time'] = str(publish_time[0]).replace("年", "-").replace("月", "-").replace("日", "-")
        except:
            item['publish_time'] = ""
        item['site_info'] = self.newsinfo_site
        item['category_info'] = self.newsinfo_category
        item['reflink'] = i
        item['cover'] = ""
        item['source'] = "新浪财经"
        # print(item)
        self.rows.append(item)
        self.number += 1

        self.threds()


    def get_detail_url(self):
        time.sleep(5)
        print("程序启动")
        while True:
            print("目前的q {} {}".format(self.q.empty(), self.list_end))
            n = 0
            while not self.q.empty():
                result = self.q.get()
                # print(result)
                t = threading.Thread(target=self.req_detail, args=(result, ))
                t.start()

                n += 1
                if n == 30:
                    return

            if self.q.empty() == True and self.list_end == False:
                print("进入结束页面")
                return "1"

            elif self.q.empty() == True and self.list_end == True:
                print("正在等待")
                time.sleep(2)

    def start(self):
        t1 = threading.Thread(target=self.get_detail_url)
        t1.start()

        self.parse()
        self.list_end = False

    def parse(self):

        response = self.reqs.req_html("http://feed.mix.sina.com.cn/api/roll/get?pageid=384&lid=2519&k=&num=50&page=1", self.headers, "GET")
        if response == "fail":
            return
        jsondata = json.loads(response)
        for i in jsondata.get("result").get("data"):
            print("存入对接")
            self.q.put(i.get("url"))

        # for i, j in zip(all_href, all_cover):
        #     print("存入对接")
        #     self.q.put((i, j))


if __name__ == '__main__':
    p = HXCJ()
    p.start()


