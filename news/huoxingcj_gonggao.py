#encoding:utf8
import json
import random

import re

from Config import author_img
from lxml import etree
from news.request_class import ReqCls,req
from news.Logic_class import LogicCls
from Utils import md5, getCurFullDay
import threading
import queue
import time
from logger_file import logger
class HXCJ():
    def __init__(self):
        self.headers = {
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36"
        }
        # self.req_cls = ReqCls("https://www.huoxing24.com/", self.headers)
        self.newsinfo_site = "火星财经"
        self.newsinfo_category = "新闻-公告"
        self.rows = []
        self.reqs = req()
        self.logic = LogicCls()
        self.q = queue.Queue()
        self.list_end = True
        self.num = 0
        self.number = 0
        self.logger = logger("hxcj_gg.log")
        self.thread_num = 0

    def threds(self):
        if self.q.empty() != True:
            result = self.q.get()
            i, j, z = result[0], result[1],result[2]
            self.req_detail(i, j, z)

        else:
            self.num += 1
            print("队列的长度为 {} {}".format(self.q.qsize(), self.num))
            if self.num == self.thread_num:
                self.logger.info("本次火星财经抓{}".format(str(self.number)))
                self.logic.importData(self.rows)
            exit()

    def req_detail(self, i, j, z):
        item = {}
        detail_html = ""
        response_detail = self.reqs.req_html(i, self.headers, "GET")

        if response_detail == "fail":
            self.threds()
        try:
            detail_html = etree.HTML(response_detail)
        except:
            self.threds()
        try:
            title = detail_html.xpath("//h5[@class='title-h5']/text()")
            item['title'] = title[0]
            item["newsid"] = md5(title[0].encode('utf8'))
        except:
            self.threds()
        try:
            content = detail_html.xpath("//div[@class='cont']")[0]
            content = etree.tostring(content, method='html')
            item["content"] = str(content).replace("\\n", "").replace("\\t", "")
        except:
            self.threds()
        try:
            item['author'] = z
            item['author_img'] = j
        except:
            item['author'] = "火星财经-公告"
            item['author_img'] = random.choice(author_img)
        try:
            item['publish_time'] = getCurFullDay()
        except:
            item['publish_time'] = getCurFullDay()
        item['site_info'] = self.newsinfo_site
        item['category_info'] = self.newsinfo_category
        item['reflink'] = i
        item['cover'] = ""
        item['source'] = "火星财经"
        print(item)
        self.rows.append(item)
        self.number+=1
        self.threds()


    def get_detail_url(self):
        time.sleep(30)
        print("程序启动")
        while True:
            print("目前的q {} {}".format(self.q.empty(), self.list_end))
            n = 0
            while not self.q.empty():
                result = self.q.get()
                t = threading.Thread(target=self.req_detail, args=(result[0], result[1], result[2]))
                t.start()

                n += 1
                if n == self.thread_num:
                    return

            if self.q.empty() == True and self.list_end == False:
                print("进入结束页面")
                return "1"

            elif self.q.empty() == True and self.list_end == True:
                print("正在等待")
                time.sleep(2)

    def start(self):
        t1 = threading.Thread(target=self.get_detail_url)
        t1.start()

        self.parse()
        self.list_end = False

    def parse(self):
        response = self.reqs.req_html("https://news.huoxing24.com/notice", self.headers, "GET")
        if response == "fail":
            return
        re_con = re.compile(r'window.__INITIAL_STATE__ =(.*)')
        jsondata = (re_con.search(response).group(1))
        jsondata = json.loads(jsondata)
        for i in (jsondata['notice']['statisticsObj']['obj']['newCoinOnlineExchangeNoticeList']) + (jsondata['notice']['statisticsObj']['obj']['importantExchangeNoticeList']):
            print(i)
            all_href = "https://news.huoxing24.com/noticeDetail/" + i.get("noticeId")
            all_cover = i.get("exchangeIconUrl")
            all_author = i.get("exchangeName")
            self.q.put((all_href, all_cover, all_author))
            self.thread_num += 1



if __name__ == '__main__':
    p = HXCJ()
    p.start()
'https://news.huoxing24.com/noticeDetail/a1d6ce642f14b33b85abd4ff14933ceb'
'https://news.huoxing24.com/noticeDetail/a1d6ce642f14b33b85abd4ff14933ceb'