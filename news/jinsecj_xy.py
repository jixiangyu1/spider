#encoding:utf8
import json
import random
from Config import author_img
from lxml import etree
from news.request_class import ReqCls,req
from news.Logic_class import LogicCls
from Utils import md5
import threading
import queue
import time
from logger_file import logger
from Utils import date
import requests
class HXCJ():
    def __init__(self):
        self.headers = {
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36"
        }
        self.newsinfo_site = "金色财经"
        self.newsinfo_category = "新闻-学院"
        self.rows = []
        self.reqs = req()
        self.logic = LogicCls()
        self.q = queue.Queue()
        self.list_end = True
        self.num = 0
        self.number = 0
        self.logger = logger("jscj_xy.log")

    def parse_detail(self, i):
        item = {}
        try:
            title = i['extra']['title']
            item['title'] = title
            item["newsid"] = md5(title.encode('utf8'))
        except:
            return "fail"
        try:
            item['author'] = i['extra']['author']
            item['author_img'] = i['extra']['author_img']
        except:
            item['author'] = "金色财经-学院"
            item['author_img'] = random.choice(author_img)
        try:
            publish_time = date(i['extra']['published_at'])
            item['publish_time'] = publish_time
        except:
            item['publish_time'] = ""
        item['site_info'] = self.newsinfo_site
        item['category_info'] = self.newsinfo_category
        item['reflink'] = i['extra']['topic_url']
        # response = self.reqs.req_html(i['extra']['topic_url'], self.headers, "GET")
        response = requests.get(i['extra']['topic_url'], headers=self.headers).text
        html = etree.HTML(response)
        content = html.xpath('//section[@class="js-article-content"]')[0]
        content = etree.tostring(content, method='html')
        item["content"] = content
        if len(i['extra']['thumbnails_pics']) > 0:
            item['cover'] = i['extra']['thumbnails_pics'][0]
        item['source'] = "金色财经"
        return item

    def start(self):
        self.parse()
        self.list_end = False

    def parse(self):

        response = self.reqs.req_html("https://api.jinse.com/v6/m/information/list?catelogue_key=xueyuan&flag=down&limit=10&information_id=0&version=9.9.9&_source=m", self.headers, "GET")
        if response == "fail":
            return
        jsondata = json.loads(response)
        for i in jsondata.get("list"):
            items = (self.parse_detail(i))
            print(items)
            self.rows.append(items)
            self.number += 1
        self.logger.info("本次金色财经产业抓{}".format(str(self.number)))
        self.logic.importData(self.rows)

if __name__ == '__main__':
    p = HXCJ()
    p.start()
