#! /usr/bin env python
# coding:utf-8
# 业务逻辑

import Config
import pymysql
import json
import Utils as Utils
from Logic import Logic
from huobi_kline.Huobi_Config import config_dict
from logger_file import logger

class Huobi_FS_Logic(Logic):
    def __init__(self):
        Logic.__init__(self, "finance_data")


    def importData(self, row, flag, logger_name):
        sql = ""
        if 0 == len(row):
            return
        logs = logger(logger_name)
        for i in row:
            for id, datas in i.items():
                # for k in datas:
                try:
                    k = datas[0]
                    k_id = k.get('id')
                    c_id = id
                    count = k.get('count')
                    amount = k.get('amount')
                    open = k.get('open')
                    close = k.get('close')
                    low = k.get('low')
                    high = k.get('high')
                    vol = k.get('vol')
                    # select_result = self.getData('huobionemin', flag, id)


                    sets = {
                        "`count` = '%s'" % count,
                        "`amount` = %s" % amount,
                        "`k_id` = %s" % k_id,
                        "`open` = %s" % open,
                        "`close` = %s" % close,
                        "`low` = %s" % low,
                        "`high` = %s" % high,
                        "`vol` = %s" % vol,
                    }
                    sql = 'UPDATE %s SET %s WHERE `c_id` = %s AND `source` = %s' % (
                        'huobionemin', ','.join(sets), c_id, flag)
                    print("UPDATE: %s" % sql)
                    self.cursor.execute(sql)
                    # else:
                    #     valuesTuple = (
                    #         c_id,
                    #         count,
                    #         amount,
                    #         k_id,
                    #         open,
                    #         close,
                    #         low,
                    #         high,
                    #         vol,
                    #         flag
                    #     )
                    #     Utils.D(valuesTuple)
                    #     # # try:
                    #     # insert
                    #     values = "%s,%s,'%s',%s,%s,%s,%s,%s,%s,%s" % valuesTuple
                    #     sql = "insert into %s (`c_id`, `count`, `amount`, `k_id`,`open`, `close`,`low`, `high`,`vol`, `source`) values (%s)" % (
                    #         'huobionemin' , values)
                    #     print("Insert: %s" % sql)
                    #     self.cursor.execute(sql)
                except:
                    logs.info("出现异常 {}".format(id))
        self.db.commit()
