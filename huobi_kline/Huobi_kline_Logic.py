#! /usr/bin env python
# coding:utf-8
# 业务逻辑

import Config
import pymysql
import json
import Utils as Utils
from Logic import Logic
from huobi_kline.Huobi_Config import config_dict
from logger_file import logger
class HuobiLogic(Logic):
    def __init__(self):
        Logic.__init__(self, "finance_data")

    def wir(self, dat):
        with open("a.txt", "w")as f:
            f.write(dat)
    def importData(self, row, flag, logger_name):
        sql = ""
        if 0 == len(row):
            return
        logs = logger(logger_name)
        for i in row:
            for id, datas in i.items():
                # for k in datas:
                    try:
                        k = datas[1]
                        k_id = k.get('id')
                        c_id = id
                        count = k.get('count')
                        amount = k.get('amount')
                        open = round(float(k.get('open')), 4)
                        close = round(float(k.get('close')), 4)
                        low = round(float(k.get('low')), 4)
                        high = round(float(k.get('high')), 4)
                        vol = k.get('vol')
                        if self.getKid(config_dict.get(flag).get("table"), k_id, id):
                            sets = {
                                "`count` = '%s'" % count,
                                "`amount` = %s" % amount,
                                "`k_id` = %s" % k_id,
                                "`open` = %s" % open,
                                "`close` = %s" % close,
                                "`low` = %s" % low,
                                "`high` = %s" % high,
                                "`vol` = %s" % vol,
                            }
                            sql = 'UPDATE %s SET %s WHERE `k_id` = "%s" AND `c_id` = %s' % (
                                config_dict.get(flag).get("table"), ','.join(sets), k_id, id)
                            Utils.D("UPDATE: %s" % sql)
                            self.cursor.execute(sql)
                        else:
                            valuesTuple = (
                                c_id,
                                count,
                                amount,
                                k_id,
                                open,
                                close,
                                low,
                                high,
                                vol,
                                u"huobi"
                            )
                            Utils.D(valuesTuple)
                            # # try:
                            # insert
                            values = "%s,%s,'%s',%s,%s,%s,%s,%s,%s,'%s'" % valuesTuple
                            sql = "insert into %s (`c_id`, `count`, `amount`, `k_id`,`open`, `close`,`low`, `high`,`vol`, `source`) values (%s)" % (
                                config_dict.get(flag).get("table"), values)
                            Utils.D("Insert: %s" % sql)
                            self.cursor.execute(sql)
                    except:
                        logs.info("出现异常 {}".format(id))

            # #提交事物
        self.db.commit()


        # self.cursor.execute(sql)