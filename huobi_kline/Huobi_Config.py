#encoding:utf8

config_dict = {
    5: {"table":"Huobifivemin", "period": "5min"},
    15: {"table":"Huobififteenmin", "period": "15min"},
    30: {"table":"Huobithirtymin", "period": "30min"},
    60: {"table":"Huobihours", "period": "60min"},
    70: {"table":"Huobiday", "period": "1day"},
    80: {"table":"Huobiweek", "period": "1week"},
    90: {"table":"Huobimonth", "period": "1mon"},
}

thread_num = 20