#! /usr/bin env python
# coding:utf-8

import threading
import datetime
import queue
import Config
import time
import Utils as Utils
import requests
import json
from lxml import etree
from Logic import Logic
from huobi_kline.Huobi_Config import config_dict, thread_num
from huobi_kline.Huobi_kline_DWM_Logic import Huobi_DWMLogic
import argparse
from logger_file import logger
from huobi_kline.Huobi_kline_fenshi_Logic import  Huobi_FS_Logic
class HuobiKline():
    def __init__(self,):
        self.s = requests.session()
        parser = argparse.ArgumentParser(description='manual to this script')
        parser.add_argument('--type', type=int, default=None)
        args = parser.parse_args()
        self.value = args.type
        # self.value = 5
        self.url = "https://api-aws.huobi.pro/market/history/kline?period={}&size=200&symbol={}"
        self.fail_num = 0
        self.num = 0
        self.lists = []
        self.logger = logger("huobikline_ss__{}.log".format(self.value))

    def end_fun(self):
        if q.empty() != True:
            item = q.get()
            self.work(item)

        else:
            self.num += 1
            print("队列的长度为 {} {}".format(q.qsize(), self.num))
            if self.num == thread_num:
                print("准备存储")
                if self.value == 70 or self.value == 80 or self.value == 90:
                    logic.importData(self.lists, self.value, "huobikline_ss_{}.log".format(self.value))
                else:
                    fs_logic.importData(self.lists, self.value, "huobikline_ss_{}.log".format(self.value))
            exit()


    def work(self, curr_item):
        name = curr_item.get("name")
        categroy = curr_item.get("categroy")
        url = self.url.format(config_dict.get(self.value).get("period"), str(name).lower() + str(categroy).lower())
        print(url)
        response = self.request(url)

        if response == "fail":
            self.fail_num += 1
            q.put(curr_item)
            print("重新放入")
            return self.end_fun()


        self.lists.append({curr_item.get("id"):response.get("data")})

        self.end_fun()


    def request(self, url):
        n = 0
        while True:
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36',
            }
            try:
                text = self.s.get(url, headers=headers).text
                data = json.loads(text)
                return data
            except requests.exceptions.ConnectionError:
                print("出现链接错误")
                n = n + 1
                if n == Config.REQUEST_NUM:
                    return "fail"
                continue
            except requests.exceptions.Timeout:
                print("出现链接等待超时")
                n = n + 1
                if n == Config.REQUEST_NUM:
                    return "fail"
                continue
            except json.decoder.JSONDecodeError:
                print("出现链接json错误")
                n = n + 1
                if n == Config.REQUEST_NUM:
                    return "fail"
                time.sleep(1)
                continue

if __name__ == "__main__":
    print('*****************NEW********{}*****'.format(Utils.getCurFullDay()))
    select_logic = Logic("finance_data")
    start = time.clock()
    logic = Huobi_DWMLogic()
    fs_logic = Huobi_FS_Logic()
    currencys = select_logic.get_currenies()
    print(currencys)
    thread_num_all = 0
    q = queue.Queue()
    pp = HuobiKline()
    for i in currencys:
        # print(i)
        q.put(i)
        thread_num_all += 1
    s = 0
    while not q.empty():
        i = q.get()
        print("取到的i {}".format(i))
        threading.Thread(target=pp.work, args=(i,)).start()
        s += 1
        if s == thread_num:
            break

    print('*****************End********{}*****'.format(Utils.getCurFullDay()))


