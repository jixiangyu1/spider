# -*- coding: utf-8 -*-

# Define here the models for your spider middleware
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/spider-middleware.html

from scrapy import signals


class AdilcrawlerSpiderMiddleware(object):
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the spider middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_spider_input(self, response, spider):
        # Called for each response that goes through the spider
        # middleware and into the spider.

        # Should return None or raise an exception.
        return None

    def process_spider_output(self, response, result, spider):
        # Called with the results returned from the Spider, after
        # it has processed the response.

        # Must return an iterable of Request, dict or Item objects.
        for i in result:
            yield i

    def process_spider_exception(self, response, exception, spider):
        # Called when a spider or process_spider_input() method
        # (from other spider middleware) raises an exception.

        # Should return either None or an iterable of Request, dict
        # or Item objects.
        pass

    def process_start_requests(self, start_requests, spider):
        # Called with the start requests of the spider, and works
        # similarly to the process_spider_output() method, except
        # that it doesn’t have a response associated.

        # Must return only requests (not items).
        for r in start_requests:
            yield r

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


class AdilcrawlerDownloaderMiddleware(object):
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the downloader middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_request(self, request, spider):
        # Called for each request that goes through the downloader
        # middleware.

        # Must either:
        # - return None: continue processing this request
        # - or return a Response object
        # - or return a Request object
        # - or raise IgnoreRequest: process_exception() methods of
        #   installed downloader middleware will be called
        return None

    def process_response(self, request, response, spider):
        # Called with the response returned from the downloader.

        # Must either;
        # - return a Response object
        # - return a Request object
        # - or raise IgnoreRequest
        return response

    def process_exception(self, request, exception, spider):
        # Called when a download handler or a process_request()
        # (from other downloader middleware) raises an exception.

        # Must either:
        # - return None: continue processing this exception
        # - return a Response object: stops process_exception() chain
        # - return a Request object: stops process_exception() chain
        pass

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)

import queue
import requests
import time
class ProxyMiddleware1(object):
    """适应新建爬虫用代理池
    """

    def __init__(self):
        self.http_request_num = 0
        self.proxies_list = queue.Queue()
        self.api = 'http://39.99.153.57:5000/proxy?count=30'


    def Get_NewProxy(self, times=0):
        """从代理池中获取代理
        """
        if times >= 6:
            return
        proxy = requests.get(self.api, timeout=5).text
        proxy_list = eval(proxy)
        proxies_list = list()
        if type(proxy_list) is dict:
            time.sleep(10)
            times += 1
            self.Get_NewProxy()
        if proxy_list[0]["ip"] is None or proxy_list[0]["ip"] == "None":
            time.sleep(10)
            times += 1
            self.Get_NewProxy()
        else:
            for proxy_msg in proxy_list:
                timestampProxy = int(proxy_msg["timestamp"])
                timestampNow = int(time.time())
                # if timestampNow - timestampProxy >= 5 * 60:
                #     continue
                # else:
                proxies_list.append(proxy_msg)
                self.proxies_list.put(proxy_msg)
        if self.proxies_list.empty():
            print("***************************")
            times += 1
            self.Get_NewProxy()

    def set_proxy(self, request, spider):
        """为http/https请求设置代理
        """
        if self.proxies_list.empty():
            self.Get_NewProxy()
        if self.proxies_list.empty():
            spider.logger.warning("提取代理为空，当前无可用代理，此http/https请求将走本地ip")
            request.meta["proxy"] = None
            request.meta["proxy_msg"] = None
        else:
            proxy_msg = self.proxies_list.get()
            proxy = proxy_msg["ip"]
            print(proxy)
            spider.logger.info("start http/https request, url: %s" % request.url)
            request.meta["proxy"] = 'http://' + proxy
            request.meta["proxy_msg"] = proxy_msg
            print("tianjiadaili")

    def process_request(self, request, spider):
        self.http_request_num += 1
        spider.logger.info("start %s num request" % self.http_request_num)
        self.set_proxy(request, spider)
        print("***************************")

    def process_response(self, request, response, spider):
        print("-------------------------------------{}".format(response.status))
        has_retry = 0

        if response.status != 200 or response.text == None:
            if has_retry:
                request.meta["self_retry_times"] = request.meta["self_retry_times"] + 1
            else:
                request.meta["self_retry_times"] = 1
            self.set_proxy(request, spider)
            url = request.url
            spider.logger.info("status not 200 start retry,  retry_url: %s, retry_time: %s" % (
                url, request.meta["self_retry_times"]))
            request._set_url(url)
            new_request = request.copy()
            proxy_msg = request.meta["proxy_msg"]
            if proxy_msg:
                pass
                # self.Proxy_Statistics(proxy_msg, useful=False)
            return new_request
        else:
            print("zouzhe")
            proxy_msg = request.meta["proxy_msg"]
            return response

    def process_exception(self, request, exception, spider):
        print("safasddsafgads")
        print("exception {}".format(exception))
        spider.logger.info("exception request proxy: (%s)" % request.url)
        self.set_proxy(request, spider=spider)
        has_retry = 0
        if "self_retry_times" in request.meta.keys():
            has_retry = 1
        if has_retry:
            request.meta["self_retry_times"] = request.meta["self_retry_times"] + 1
        else:
            request.meta["self_retry_times"] = 1
        url = request.url
        spider.logger.info("status not 200 start retry,  retry_url: %s, retry_time: %s" % (
            url, request.meta["self_retry_times"]))
        request._set_url(url)
        proxy_msg = request.meta["proxy_msg"]

        new_request = request.copy()
        return new_request