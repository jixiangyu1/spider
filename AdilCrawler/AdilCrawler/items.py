# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class AdilcrawlerItem(scrapy.Item):
    name = scrapy.Field() # 用户唯一标识
    link = scrapy.Field()  # 任务id
    schedule = scrapy.Field()  # 平台id
    tags = scrapy.Field()  # 视频标题
    tags_str = scrapy.Field()  # 视频封面图
    video_type = scrapy.Field()  # 视频发布日期
    release_date = scrapy.Field()  # 视频时长
    country_list = scrapy.Field()  # 视频作者信息
    plot = scrapy.Field()  # 关键词
    video_detail_content = scrapy.Field()  # 视频链接
    cover = scrapy.Field()  # 视频描述
    video_link_zip = scrapy.Field()  # 创建时间
    score = scrapy.Field()  # 排序
    schedule_status = scrapy.Field()  # 视频id，
