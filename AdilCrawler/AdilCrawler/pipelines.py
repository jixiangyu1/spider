# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

import pymysql
import datetime
import logging
# 获取当前日期
def getCurFullDay():
    nowtime = datetime.datetime.now()  # 返回的是本地时区的当前时间
    return nowtime.strftime("%Y-%m-%d %H:%M:%S")

class AdilcrawlerPipeline(object):
    def __init__(self):
        self.db = pymysql.connect(
            host="localhost",
            port=3306,
            user="root",
            passwd="123456",
            db="data_video",
            charset="utf8mb4"
        )
        self.cursor = self.db.cursor(cursor=pymysql.cursors.DictCursor)

    def select_name(self, table_name, name_value, flag=0):
        # print("chaunjinlai de ", name_value)
        # print(flag)
        if flag == 1:
            sql = "SELECT `id` FROM `%s` WHERE `video_id` = %s" % (table_name, name_value)
        else:
            sql = "SELECT `id` FROM `%s` WHERE `name` = '%s'" % (table_name, name_value)
        print(sql)
        self.cursor.execute(sql)
        res = self.cursor.fetchone()
        if res:
            return res
        else:
            return {}

    def select_tag_regin_name(self, table_name, name_value, category_id):
        # print("chaunjinlai de ", name_value)
        # print(flag)

        sql = "SELECT `id` FROM `%s` WHERE `name` = '%s' AND `category_id` = %s" % (table_name, name_value, category_id)
        print(sql)
        self.cursor.execute(sql)
        res = self.cursor.fetchone()
        if res:
            return res
        else:
            return {}

    def insert_tags(self, tag, category_id):
        sql = "INSERT INTO `data_tags`( `name`, `category_id`,  `create_time`) VALUES ( '%s', %s, '%s');" % (
        tag, category_id, getCurFullDay())
        print(sql)
        self.cursor.execute(sql)
        tag_id = self.cursor.lastrowid
        return tag_id

        # 更新schedule状态

    def updates_schedule(self, schedule, schedule_status, video_id, name):
        sql = "UPDATE data_video SET `schedule` = '{}', `schedule_status` = {} WHERE `id` = {} AND `name` = '{}'".format(
            schedule, schedule_status, video_id, name)
        print(sql)
        self.cursor.execute(sql)


        # 插入 data_category 分类名称

    def insert_data_category(self, name):
        sql = "INSERT INTO `data_category`(`name`, `description`, `sort`, `level`, `pid`, `create_time`, `status`) VALUES ('%s', NULL, NULL, NULL, NULL, '%s', 1);" % (
            name, getCurFullDay())
        self.cursor.execute(sql)
        data_category_id = self.cursor.lastrowid
        return data_category_id

        # 插入data_region 国家地区表

    def insert_data_region(self, name, category_id):
        sql = "INSERT INTO `data_region`(`name`, `category_id`, `create_time`) VALUES ('%s', %s, '%s');" % (
        name, category_id, getCurFullDay())
        self.cursor.execute(sql)
        data_region_id = self.cursor.lastrowid
        return data_region_id

        # 插入data_video_region 国家地区 影片id

    def insert_data_video_region(self, video_id, region_id):
        sql = "INSERT INTO `data_video_region`(`video_id`,`region_id`, `create_time`) VALUES (%s, '%s', '%s');" % (
        video_id, region_id, getCurFullDay())
        print(sql)
        self.cursor.execute(sql)

        # 插入data_video_tags videoid 与 tag_id

    def insert_video_tags(self, video_id, tags_id):
        sql = "INSERT INTO `data_video_tags`(`video_id`,`tags_id`, `create_time`) VALUES (%s, %s, '%s');" % (
        video_id, tags_id, getCurFullDay())
        print(sql)
        self.cursor.execute(sql)


        # 插入 data_video_category videoid 与 tag_id

    def data_video_category(self, video_id, category_id):
        sql = "INSERT INTO `data_video_category`(`video_id`, `category_id`, `create_time`) VALUES (%s, %s, '%s');" % (
        video_id, category_id, getCurFullDay())
        print(sql)
        self.cursor.execute(sql)

        # 插入adress表

    # 插入adress表
    def data_video_address(self, video_id, name, url, indexs, updates_flag):
        if updates_flag == 1:
            sql = "SELECT `id` FROM `data_video_address` WHERE `video_id` = %s AND `name` = '%s'" % (video_id, name)
            print(sql)
            self.cursor.execute(sql)
            res = self.cursor.fetchone()
            print("addreass", res)
            if res:
                sql = "UPDATE data_video_address SET `name` = '{}', `url` = '{}', `sort` = {} WHERE video_id = {} AND `name` = '{}'".format(
                    name, url, indexs, video_id, name)
                print(sql)
                self.cursor.execute(sql)
            else:
                sql = "INSERT INTO `data_video_address`(`video_id`, `name`, `url`, `sort`, `create_time`,`play_count`, `source`, `status`) VALUES (%s, '%s', '%s', %s, '%s', 0, '%s',1);" % (
                video_id, name, url, indexs, getCurFullDay(), '0')
                print(sql)
                self.cursor.execute(sql)
        else:
            sql = "INSERT INTO `data_video_address`(`video_id`, `name`, `url`, `sort`, `create_time`,`play_count`, `source`, `status`) VALUES (%s, '%s', '%s', %s, '%s', 0, '%s',1);" % (
            video_id, name, url, indexs, getCurFullDay(), '0')
            print(sql)
            self.cursor.execute(sql)
            # self.db.commit()

    def select_id(self, table_name, name):
        sql = "SELECT `id`, `schedule_status` FROM `%s` WHERE `name` = '%s'" % (table_name, name)
        self.cursor.execute(sql)
        res = self.cursor.fetchone()
        if res:
            if res.get('schedule_status') == 1:
                return "success"
            return res.get('id')

        else:
            return {}

    def process_item(self, item, spider):
        print("存入额item", item)

    # def process_item(self, item, spider):
    #     print("存储的itme {}".format(item))
    #
    #     data_category_name = item.get("video_type")
    #     data_video_name = str(item.get("name")).strip()
    #     schedule = item.get("schedule")
    #     cover = item.get('cover')
    #     tags_str = item.get('tags_str')
    #     score = item.get('score')#without
    #     release_date = item.get('release_date')
    #     plot = item.get('plot')
    #     country_list = item.get('country_list')
    #     schedule_status = item.get('schedule_status')#without
    #     print("schedule_status", schedule_status)
    #
    #     # 查询data_category表name是否存在 存在返回id 否则插入进入返回id
    #     data_category_name_exsit = self.select_name("data_category", data_category_name)
    #     if data_category_name_exsit:
    #         data_category_id = data_category_name_exsit["id"]
    #         print(data_category_id)
    #     else:
    #         data_category_id = self.insert_data_category(data_category_name)
    #
    #     # 查询影片名称是否重复
    #     data_video_name_exsit = self.select_id("data_video", data_video_name)
    #     print("data_video_name_exsit", data_video_name_exsit)
    #     if data_video_name_exsit == "success":
    #         print("影片存在")
    #         return ""
    #         # break
    #     else:
    #         print(data_category_name)
    #         # html化影片内容
    #         video_detail_content = pymysql.escape_string(str(item.get('video_detail_content', '')))
    #
    #         # 插入影片 data_video
    #         if data_video_name_exsit == {}:
    #             if tags_str == []:
    #                 tags_str = ""
    #             else:
    #                 tags_str = ",".join(tags_str)
    #             sql = "INSERT INTO `data_video`( `name`, `cover`, `schedule_status`, `schedule`,`tags`,  `status`, `score`,  `release_date`, `create_time`,`description`,`plot`) VALUES ('%s', '%s', %s, '%s', '%s', %s, %s, %s,'%s', '%s', '%s');" % (
    #                 data_video_name, cover, schedule_status, schedule, tags_str, 1, float(score), int(release_date),
    #                 getCurFullDay(), video_detail_content, plot)
    #
    #             print(sql)
    #             self.cursor.execute(sql)
    #             video_id = self.cursor.lastrowid
    #
    #             # 插入address
    #             video_link_zip = item.get('video_link_zip')
    #
    #             video_address_name_exsits = self.select_name("data_video_address", video_id, flag=1)
    #             print("video_address_name_exsits {}".format(video_address_name_exsits))
    #             if video_address_name_exsits:
    #                 updates_flag = 1
    #             else:
    #                 updates_flag = 0
    #             for k in video_link_zip:
    #                 for i in video_link_zip[k]:
    #                 # print(json.dumps(video_link_zip))
    #                     self.data_video_address(video_id, i.get('name'), i.get("url"), i.get('sort_jishu'), updates_flag)
    #
    #             self.data_video_category(video_id, data_category_id)
    #
    #             # # 查询data_region表name是否存在 存在返回id 否则插入进入返回id
    #             if country_list == []:
    #                 country_list = ["无"]
    #             for country_name in country_list:
    #                 data_region_name_exsit = self.select_tag_regin_name("data_region", str(country_name).strip(),
    #                                                                     data_category_id)
    #                 if data_region_name_exsit:
    #                     data_region_id = data_region_name_exsit["id"]
    #
    #                 else:
    #                     data_region_id = self.insert_data_region(str(country_name).strip(), data_category_id)
    #
    #                 print(data_region_id)
    #
    #                 self.insert_data_video_region(video_id, data_region_id)
    #
    #             tags = item.get('tags')
    #             if tags == []:
    #                 tags = [["无", "无"]]
    #
    #             for tags_lists in tags:
    #                 print(tags_lists)
    #                 data_tags_name_exsit = self.select_tag_regin_name("data_tags", tags_lists[1], data_category_id)
    #                 if data_tags_name_exsit:
    #                     data_tags_id = data_tags_name_exsit["id"]
    #
    #                 else:
    #                     data_tags_id = self.insert_tags(tags_lists[1], data_category_id)
    #
    #                 self.insert_video_tags(video_id, data_tags_id)
    #
    #
    #         else:
    #             print("只需要更新 {}".format(data_video_name))
    #             video_id = data_video_name_exsit
    #
    #             self.updates_schedule(schedule, schedule_status, video_id, data_video_name)
    #             # 插入address
    #             video_link_zip = item.get('video_link_zip')
    #
    #             video_address_name_exsits = self.select_name("data_video_address", video_id, flag=1)
    #             print("video_address_name_exsits {}".format(video_address_name_exsits))
    #             if video_address_name_exsits:
    #                 updates_flag = 1
    #             else:
    #                 updates_flag = 0
    #             for k in video_link_zip:
    #                 for i in video_link_zip[k]:
    #                     self.data_video_address(video_id, i.get('name'), i.get("url"), i.get('sort_jishu'), updates_flag)
    #
    #         print("存入影片")
    #
    #         self.db.commit()
    #     return item
import json
class Asa(object):
    def __init__(self):
        self.db = pymysql.connect(
            host="123.57.33.212",
            port=3306,
            user="root",
            passwd="cy2018xyz",
            db="video_data",
            charset="utf8mb4"
        )
        self.cursor = self.db.cursor(cursor=pymysql.cursors.DictCursor)

    def select_name(self, table_name, name_value, flag=0):
        # print("chaunjinlai de ", name_value)
        # print(flag)
        if flag == 1:
            sql = "SELECT `id` FROM `%s` WHERE `video_id` = %s" % (table_name, name_value)
        else:
            sql = "SELECT `id` FROM `%s` WHERE `name` = '%s'" % (table_name, name_value)
        print(sql)
        self.cursor.execute(sql)
        res = self.cursor.fetchone()
        if res:
            return res
        else:
            return {}

    def select_tag_regin_name(self, table_name, name_value, category_id):
        # print("chaunjinlai de ", name_value)
        # print(flag)

        sql = "SELECT `id` FROM `%s` WHERE `name` = '%s' AND `category_id` = %s" % (table_name, name_value, category_id)
        print(sql)
        self.cursor.execute(sql)
        res = self.cursor.fetchone()
        if res:
            return res
        else:
            return {}

    def insert_tags(self, tag, category_id):
        sql = "INSERT INTO `data_tags`( `name`, `category_id`,  `create_time`) VALUES ( '%s', %s, '%s');" % (
        tag, category_id, getCurFullDay())
        print(sql)
        self.cursor.execute(sql)
        tag_id = self.cursor.lastrowid
        return tag_id

        # 更新schedule状态

    def updates_schedule(self, schedule, schedule_status, video_id, name):
        sql = "UPDATE data_video SET `schedule` = '{}', `schedule_status` = {} WHERE `id` = {} AND `name` = '{}'".format(
            schedule, schedule_status, video_id, name)
        print(sql)
        self.cursor.execute(sql)


        # 插入 data_category 分类名称

    def insert_data_category(self, name):
        sql = "INSERT INTO `data_category`(`name`, `description`, `sort`, `level`, `pid`, `create_time`, `status`) VALUES ('%s', NULL, NULL, NULL, NULL, '%s', 1);" % (
            name, getCurFullDay())
        self.cursor.execute(sql)
        data_category_id = self.cursor.lastrowid
        return data_category_id

        # 插入data_region 国家地区表

    def insert_data_region(self, name, category_id):
        sql = "INSERT INTO `data_region`(`name`, `category_id`, `create_time`) VALUES ('%s', %s, '%s');" % (
        name, category_id, getCurFullDay())
        self.cursor.execute(sql)
        data_region_id = self.cursor.lastrowid
        return data_region_id

        # 插入data_video_region 国家地区 影片id

    def insert_data_video_region(self, video_id, region_id):
        sql = "INSERT INTO `data_video_region`(`video_id`,`region_id`, `create_time`) VALUES (%s, '%s', '%s');" % (
        video_id, region_id, getCurFullDay())
        print(sql)
        self.cursor.execute(sql)

        # 插入data_video_tags videoid 与 tag_id

    def insert_video_tags(self, video_id, tags_id):
        sql = "INSERT INTO `data_video_tags`(`video_id`,`tags_id`, `create_time`) VALUES (%s, %s, '%s');" % (
        video_id, tags_id, getCurFullDay())
        print(sql)
        self.cursor.execute(sql)


        # 插入 data_video_category videoid 与 tag_id

    def data_video_category(self, video_id, category_id):
        sql = "INSERT INTO `data_video_category`(`video_id`, `category_id`, `create_time`) VALUES (%s, %s, '%s');" % (
        video_id, category_id, getCurFullDay())
        print(sql)
        self.cursor.execute(sql)

        # 插入adress表

    # 插入adress表
    def data_video_address(self, video_id, name, url, indexs, updates_flag, source):
        if updates_flag == 1:
            sql = "SELECT `id` FROM `data_video_address` WHERE `video_id` = %s AND `name` = '%s' AND `source` = %s" % (video_id, name, source)
            print(sql)
            self.cursor.execute(sql)
            res = self.cursor.fetchone()
            print("addreass", res)
            if res:
                sql = "UPDATE data_video_address SET `name` = '{}', `url` = '{}', `sort` = {} WHERE video_id = {} AND `name` = '{}' AND `source` = {}".format(
                    name, url, indexs, video_id, name, source)
                print(sql)
                self.cursor.execute(sql)
            else:
                sql = "INSERT INTO `data_video_address`(`video_id`, `name`, `url`, `sort`, `create_time`,`play_count`, `source`, `status`) VALUES (%s, '%s', '%s', %s, '%s', 0, '%s',1);" % (
                video_id, name, url, indexs, getCurFullDay(), source)
                print(sql)
                self.cursor.execute(sql)
        else:
            sql = "INSERT INTO `data_video_address`(`video_id`, `name`, `url`, `sort`, `create_time`,`play_count`, `source`, `status`) VALUES (%s, '%s', '%s', %s, '%s', 0, '%s',1);" % (
            video_id, name, url, indexs, getCurFullDay(), source)
            print(sql)
            self.cursor.execute(sql)
            # self.db.commit()

    def select_id(self, table_name, name):
        sql = "SELECT `id`, `schedule_status`, `web_source` FROM `%s` WHERE `name` = '%s'" % (table_name, name)
        print(sql)
        self.cursor.execute(sql)
        res = self.cursor.fetchone()
        print(res)
        if res:
            if res.get('schedule_status') == 1 and res.get('web_source') == "suiyikan":
                return "success"
            return res.get('id')

        else:
            return {}

    # def process_item(self, item):
    #     print("存入额item", item)

    def process_item(self, items):
        print("存储的itme {}".format(items))
        #
        logging.info((items))
        for k in items:
            print(items[k])
            # print(items[k])
        #     for item in items[k]:
        #         print(item)
            data_category_name = items[k].get("video_type")
            data_video_name = str(items[k].get("name")).strip()
            schedule = items[k].get("schedule")
            cover = items[k].get('cover')
            tags_str = items[k].get('tags_str')
            score = items[k].get('score')#without
            release_date = items[k].get('release_date')
            country_list = items[k].get('country_list')
            schedule_status = items[k].get('schedule_status')#without
            print("schedule_status", schedule_status)

            # 查询data_category表name是否存在 存在返回id 否则插入进入返回id
            data_category_name_exsit = self.select_name("data_category", data_category_name)
            if data_category_name_exsit:
                data_category_id = data_category_name_exsit["id"]
                print(data_category_id)
            else:
                data_category_id = self.insert_data_category(data_category_name)

            # 查询影片名称是否重复
            data_video_name_exsit = self.select_id("data_video", data_video_name)
            print("data_video_name_exsit", data_video_name_exsit)
            if data_video_name_exsit == "success":
                print("影片存在")
                continue
                # break
            else:
                tags = items[k].get('tags')
                print(tags)
                print("sdgdsfg")
                print(data_category_name)
                # html化影片内容
                video_detail_content = pymysql.escape_string(str(items[k].get('video_detail_content', '')))
                plot = pymysql.escape_string(items[k].get('plot'))

                # 插入影片 data_video
                if data_video_name_exsit == {}:
                    if tags_str == []:
                        tags_str = ""
                    else:
                        tags_str = ",".join(tags_str)
                    sql = "INSERT INTO `data_video`( `name`, `cover`, `schedule_status`, `schedule`,`tags`,  `status`, `score`,  `release_date`, `create_time`,`description`,`plot`,`web_source`) VALUES ('%s', '%s', %s, '%s', '%s', %s, %s, %s,'%s', '%s', '%s', '%s');" % (
                        data_video_name, cover, schedule_status, schedule, tags_str, 1, float(score), int(release_date),
                        getCurFullDay(), video_detail_content, plot, "suiyikan")

                    print(sql)
                    self.cursor.execute(sql)
                    video_id = self.cursor.lastrowid

                    # 插入address
                    video_link_zip = items[k].get('video_link_zip')

                    video_address_name_exsits = self.select_name("data_video_address", video_id, flag=1)
                    print("video_address_name_exsits {}".format(video_address_name_exsits))
                    if video_address_name_exsits:
                        updates_flag = 1
                    else:
                        updates_flag = 0
                    for kh in video_link_zip:
                        for  i in video_link_zip[kh]:
                        # print(json.dumps(video_link_zip))
                            self.data_video_address(video_id, i.get('name'), i.get("url"), i.get("sort_jishu"), updates_flag, kh)

                    self.data_video_category(video_id, data_category_id)

                    # # 查询data_region表name是否存在 存在返回id 否则插入进入返回id
                    if country_list == []:
                        country_list = ["无"]
                    for country_name in country_list:
                        data_region_name_exsit = self.select_tag_regin_name("data_region", str(country_name).strip(),
                                                                            data_category_id)
                        if data_region_name_exsit:
                            data_region_id = data_region_name_exsit["id"]

                        else:
                            data_region_id = self.insert_data_region(str(country_name).strip(), data_category_id)

                        print(data_region_id)

                        self.insert_data_video_region(video_id, data_region_id)

                    print(k)
                    tags = items[k].get('tags')
                    print("===================" ,tags)
                    if tags == []:
                        tags = [["无", "无"]]

                    for tags_lists in tags:
                        print(tags_lists)
                        data_tags_name_exsit = self.select_tag_regin_name("data_tags", tags_lists[1], data_category_id)
                        if data_tags_name_exsit:
                            data_tags_id = data_tags_name_exsit["id"]

                        else:
                            data_tags_id = self.insert_tags(tags_lists[1], data_category_id)

                        self.insert_video_tags(video_id, data_tags_id)


                else:
                    print("只需要更新 {}".format(data_video_name))
                    video_id = data_video_name_exsit

                    self.updates_schedule(schedule, schedule_status, video_id, data_video_name)
                    # 插入address
                    video_link_zip = items[k].get('video_link_zip')

                    video_address_name_exsits = self.select_name("data_video_address", video_id, flag=1)
                    print("video_address_name_exsits {}".format(video_address_name_exsits))
                    if video_address_name_exsits:
                        updates_flag = 1
                    else:
                        updates_flag = 0
                    for kh in video_link_zip:
                        for i in video_link_zip[kh]:
                            self.data_video_address(video_id, i.get('name'), i.get("url"), i.get("sort_jishu"), updates_flag, kh)

                print("存入影片")

        self.db.commit()
        return items

# p = Asa()
#
# with open("../meiju.log", "r", encoding="utf8") as f:
#     ppps = eval(f.read())
#     p.process_item(ppps)

    def kkk(self):
        sql = "SELECT `id` FROM `data_video_category`  WHERE category_id=14"
        print(sql)
        self.cursor.execute(sql)
        res = self.cursor.fetchall()
        return (res)
    def updata_video(self, id):
        sql = "UPDATE `data_video` SET web_source = 'dilili' WHERE id={}".format(id)
        print(sql)
        self.cursor.execute(sql)
        self.db.commit()
# a = Asa()
# for i in (a.kkk()):
#     a.updata_video(i.get('id'))

