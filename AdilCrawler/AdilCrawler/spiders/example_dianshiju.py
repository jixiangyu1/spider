# -*- coding: utf-8 -*-
import scrapy
from lxml import etree
import re
import logging
import json
import traceback
from scrapy.xlib.pydispatch import dispatcher
from datetime import datetime
from ..items import AdilcrawlerItem
from scrapy import signals
from ..pipelines import Asa

class ExampleSpider(scrapy.Spider):
    name = 'example_1'
    # allowed_domains = ['example.com']
    """
    , 'https://www.suiyikan.tv/dianying/','https://www.suiyikan.tv/zongyi/','https://www.suiyikan.tv/dongman/'
    """
    start_urls = ['https://www.suiyikan.tv/dianshiju/']

    def __init__(self):
        """预留中英文品牌计算侵权排序算法
        :param keyWord:   关键词
        :param username:  用户名
        :param task_id:   任务id《唯一标识》
        :param brand:     品牌
        :param other_msg: 其他需要判断的内容
        """
        super().__init__()
        self.headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36",
            # "Proxy-Authorization": auth_proxy()

        }
        self.category_text_path_dicts = {
    "电影": "电影",
    "电视连续剧": "剧集",
    "动漫": "动漫",
    "综艺节目": "综艺",
    "纪录片": "纪录片",
    "预告片": "预告片",
    "电视剧": "剧集",
    "综艺": "综艺",
}
        self.name_item = {}
        self.dicts_source = {}
        self.pppsd = {}
        dispatcher.connect(self.spider_closed, signals.spider_closed)
        # self.dsaf = AdilcrawlerPipeline()

    def parse(self, response):
        headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36",
            # "Proxy-Authorization": auth_proxy()

        }
        html = etree.HTML(response.text)
        one_href = html.xpath('//span[@class="pull-right"]/a/@href')
        for i in one_href:
            print(i)
            more_url = "https://www.suiyikan.tv" + i
            more_url = re.sub("addtime", "addtime-{}", more_url)
            # if "大陆" in more_url:
            for indexs in range(1, 2):
                print(more_url.format(indexs))
                yield scrapy.Request(more_url.format(indexs), headers=headers, callback=self.parse_one)

    def re_date(self, responses):
        try:
            reqs = re.compile("年份：(.*?)剧情", re.S)
            date = (reqs.findall(responses))
            if date == []:
                reqs = re.compile("首播：(.*?)", re.S)
                date = (reqs.findall(responses))
            return date[0]
        except:
            return 2000

    def re_country(self, responses):
        try:
            country_list = []
            req_country = re.compile("地区：(.*?)年份", re.S)
            country = (req_country.findall(responses))[0]

            if "/" in country:
                country_list = country.split("/")
                return country_list
            country_list.append(country)
            return country_list
        except:
            return ""

    def re_link(self, response):
        re_link = re.compile(r'var cms_player = (.*?);', re.S)
        link = re_link.findall(response)
        return link[0]

    def re_plot(self, responses):
        try:
            req_score = re.compile("剧情：(.*?)详情", re.S)
            plot = (req_score.findall(responses))[0]
            if plot == "暂无":
                return ""
            return plot
        except:
            return ""

    def re_source(self, pp):
        reqs = re.compile("-(.*?)-", re.S)
        source = (reqs.findall(pp))
        return source[0]

    def re_schedule(self, responses):
        try:
            req_score = re.compile("集数: (.*?)\\n", re.S)
            schedule_count = (req_score.findall(responses))[0]
            return schedule_count
        except:
            return ""

    def re_jishu_sort(self, responses):
        # print("传入的技术", responses)
        try:
            # req_sort = re.findall('\d+',ss)
            schedule_count = (re.findall('\d+', responses))[0]
            return schedule_count
        except:
            return "10000"

    def parse_one(self, response):
        more_html = etree.HTML(response.text)
        detail_url_href = more_html.xpath("//ul[@class='list-unstyled vod-item-img ff-img-215']/li/p/a/@href")
        # detail_url_img = more_html.xpath("//ul[@class='list-unstyled vod-item-img ff-img-215']/li/p/a/img/@data-original")
        print(detail_url_href)
        print(len(detail_url_href))
        for j in detail_url_href:
            print("http://suiyikan.tv" + j)
            yield scrapy.Request("http://suiyikan.tv" + j, headers=self.headers, callback=self.xpath_details)

    def xpath_link(self, response):
        kk = response.meta['kk']
        source = response.meta['source']
        item = response.meta['item']
        name = item['name']
        sort_jishu = ""
        if item['video_type'] == "剧集":
            sort_jishu = self.re_jishu_sort(kk)

        elif item['video_type'] == "电影" or item['video_type'] == "综艺" or item['video_type'] == "动漫":
            sort_jishu = 0

        elif item['video_type'] == "综艺" :
            if "-" in kk:
                kk = str(kk).replace("-", "")
            sort_jishu = self.re_jishu_sort(kk)

        print("sort_jishu", sort_jishu)

        link = self.re_link(response.text)
        link = json.loads(link)
        video_link = link.get('url')
        if video_link == "":
            return ""

        # print("传入的kk {} 匹配的url {} source {}".format(kk, video_link, source))


        if video_link.endswith("m3u8") or "request_m3u8" in response.meta:
            if name in self.name_item:
                dicts_source = self.name_item[name]
                if source in dicts_source:
                    dicts_source[source].append({"name": kk, "url": video_link, "sort_jishu": 0})
                else:
                    dicts_source[source] = [{"name": kk, "url": video_link,  "sort_jishu": 0}]
                self.name_item[name] = dicts_source
            else:
                dicts_source = {}
                if source in dicts_source:
                    dicts_source[source].append({"name": kk, "url": video_link,  "sort_jishu": 0})
                else:
                    dicts_source[source] = [{"name": kk, "url": video_link,  "sort_jishu": 0}]
                self.name_item[name] = dicts_source

            # print("name_item", json.dumps(self.name_item))
            item['video_link_zip'] = self.name_item[name]

            if item['video_type'] == "电影":
                item['schedule_status'] = 1

            elif item['video_type'] == "剧集" or item['video_type'] == "动漫":
                if item['schedule'] == "":
                    item['schedule_status'] = 0
                elif "全" in item['schedule']:
                    item['schedule_status'] = 1
                else:
                    item['schedule_status'] = 0

            elif item['video_type'] == "综艺" or item['video_type'] == "纪录片" or item['video_type'] == "预告片":
                item['schedule_status'] = 0

            print("item", item)
            # yield item
            self.pppsd[name] = item

            # self.dsaf.process_;item(item)

            # print( "itnes"item)

        # elif video_link.endswith("html"):
        #     print("不是m3u8格式", video_link)
        #     yield scrapy.Request("https://jx.4dm.cc/by/api.php?url=" + video_link, headers=self.headers, callback=self.xpath_link, meta={"kk": kk, "item": item, "source": source, "request_m3u8":1})

        else:
            # print("走着")
            pass




    def xpath_details(self,response):
        item = AdilcrawlerItem()
        k = etree.HTML(response.text)
        print(k)
        try:
            item['name'] = k.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/h2/a/text()")[0]
            print(item['name'])
            item['link'] = k.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/h2/a/@href")[0]

            if k.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/h2/small/text()") == []:
                item['schedule'] = ""
            else:
                item['schedule'] = str(k.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/h2/small/text()")[0]).strip()
            tag_text = k.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/dl/dd[3]/a/text()")
            tag_href = k.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/dl/dd[3]/a/@href")
            tags = []
            tags_str = []
            for j in zip(tag_text, tag_href):
                tags_href = j[1]
                tags_text = j[0]
                tags.append((tags_href, tags_text))
                tags_str.append(tags_text)
            if tags == []:
                print("tags None")
                item['tags'] = tags
                item['tags_str'] = tags_str
            else:
                item['tags'] = tags
                item['tags_str'] = tags_str

            category_text_path = k.xpath('/html/body/div[1]/div[1]/h2/a[1]/text()')[0]
            print(category_text_path)
            category_text = self.category_text_path_dicts[category_text_path]
            item['video_type'] = category_text
            try:
                item['score'] = k.xpath('/html/body/div[1]/div[2]/div[2]/dl/dd/sup[1]/text()')[0]
            except:
                item['score'] = 0
            video_detail = k.xpath('//dl[@class="dl-horizontal"]')[0]
            video_detail_re = k.xpath('//dl[@class="dl-horizontal"]')[0].xpath('string(.)')
            video_detail_re = "".join(str(video_detail_re).split())
            release_date = self.re_date(video_detail_re)
            item['release_date'] = release_date
            country_list = self.re_country(video_detail_re)
            item['country_list'] = country_list
            plot = self.re_plot(video_detail_re)
            item['plot'] = plot
            # schedule_count = self.re_schedule(video_detail_re)
            # item['schedule_count'] = schedule_count
            video_detail_content = etree.tostring(video_detail, method='html')
            item['video_detail_content'] = str(video_detail_content).replace("\\n", "")
            cover = k.xpath("/html/body/div[1]/div[2]/div[1]/div/div[1]/a/img/@data-original")[0]
            if "http:" not in cover:
               cover = "http://www.suiyikan.tv/" + cover
            item['cover'] = cover
            video_detail_link_list = []
            video_detail_link_quchong_list = []
            # address_name = k.xpath('//div[@class="page-header ff-playurl-line"]//h2/text()')
            address_list = k.xpath('//ul[@class="list-unstyled row text-center ff-playurl-line ff-playurl"]/li/a/@href')
            address_name = k.xpath('//ul[@class="list-unstyled row text-center ff-playurl-line ff-playurl"]/li/a/text()')
            print(address_name)
            print(address_list)
            # print(item)
            # yield item
            # dicts_source = {}
            for kk in zip(address_name, address_list):
                print(kk)

                source = self.re_source(kk[1])
                print("-----------", source)
                yield scrapy.Request("https://www.suiyikan.tv" + kk[1], headers=self.headers, callback=self.xpath_link, meta={"kk":kk[0], "item":item, "source": source})


                # response_video_link = requests.get("https://www.suiyikan.tv" + kk[1], headers=self.headers, timeout=10).text
                # link = self.re_link(response_video_link)
                # link = json.loads(link)
                # video_link = link.get('url')
                # # print(video_link)
                # if video_link.endswith("m3u8"):
                #     if source in dicts_source:
                #         dicts_source[source].append((kk[0], video_link))
                #     else:
                #         dicts_source[source] = [(kk[0], video_link)]

                        # urls  = "https://jx.4dm.cc/by/api.php?url=" + video_link
                    # response_video_link_two = requests.get(urls, headers=self.headers, timeout=15).text
                    # json_response_video_link_two = json.loads(response_video_link_two)
                    # video_link = json_response_video_link_two.get("url")
                    # # print("请求的video_link", video_link)
                    # if video_link == None:
                    #     print("urls", urls, "None", video_link)
                    # g_list = list()
                    # g = gevent.spawn(self.kkkkk, "https://jx.4dm.cc/by/api.php?url=" + video_link)
                    # g_list.append(g)
                    # gevent.joinall(g_list)
                    # for g in g_list:
                    #     print("gvalue", g.value)
                    #     video_link = g.value

                    # print("gvalue", g.value)



            #     video_re_com = re.compile(r'var vid = \'(.*?)\';', re.S)
            #     try:
            #         video_link = video_re_com.search(response_video_link).group(1)
            #         video_link = unquote(video_link)
            #         if "http" not in video_link:
            #             print("剧集为none ", item['name'])
            #             continue
            #         video_detail_link_quchong_list.append(video_link)
            #         video_detail_link_list.append({"name": video_link_name, "url": video_link})
            #     except:
            #         return ""
            # if video_detail_link_quchong_list == []:
            #     return ""
            # item['video_link_zip'] = video_detail_link_list
            # if category_text == "电影":
            #     item['schedule_status'] = 1
            #
            # elif category_text == "剧集" or category_text == "动漫":
            #     if schedule_count == "":
            #         item['schedule_status'] = 0
            #     else:
            #         print(video_detail_link_list)
            #         if video_detail_link_list == []:
            #             item['schedule_status'] = 0
            #         elif schedule_count in video_detail_link_list[-1].get("name"):
            #             item['schedule_status'] = 1
            #         else:
            #             item['schedule_status'] = 0
            #
            # elif category_text == "综艺" or category_text == "纪录片" or category_text == "预告片":
            #     item['schedule_status'] = 0
            # print("self.name_item", self.name_item)
            import time
            print(time.time())
        except Exception   as e:
            print(traceback.print_exc())
            return ""

    def spider_closed(self, spider):
        p = Asa()
        print("sdgsadhsd")

        # print("self.pppsd", self.pppsd)
        # print(self.pppsd)





        p.process_item(self.pppsd)
