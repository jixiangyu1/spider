#! /usr/bin env python
# coding:utf-8
# common config file

from Env import *

# DB
if ENV == 'testing':
    DB = {
        "host": "127.0.0.1",
        "port": 3306,
        "user": "root",
        "passwd": "root",
        "db": "news",
        "charset": "utf8mb4",
        "sms":['18301544250']
    }

#本地环境
if ENV == 'devel':
    DB = {
        "host": "localhost",
        "port": 3306,
        "user": "root",
        "passwd": "123456",
        "db": "news",
        "charset": "utf8mb4",
        "sms": ['18301544250'],
        "es": {u'host': u'172.21.16.38', u'port': b'9200'},
        "waringfive": "本地环境五分钟数据本次抓取只抓取到{}个币种",
        "waringfifteen": "本地环境十五五分钟数据本次抓取只抓取到{}个币种",
        "waringthirty": "本地环境半小时数据本次抓取只抓取到{}个币种",
        "waringhour": "本地环境一小时数据本次抓取只抓取到{}个币种",
        "waringday": "本地环境一天数据本次抓取只抓取到{}个币种",
        "exponnet":"本地环境快链指数数据本次抓取只抓取到{}个币种",
        "feixiaohao": "本地环境非小号数据本次抓取只抓取到{}个币种",
        "startprocess": "程序已经杀掉{}进程,并重新开启{}进程,本地{}缺失数据已经补完,访问google成功",
        "revison":"由于网络原因刚才抓取到的币种数量比较少,本地{}缺失数据已经补完",
        "vpn":"nohup python /root/jixiangyu/vpn/shadowsocksr/shadowsocks/local.py -c /etc/shadowsocks.json 2>&1 &",
        "vpn1":"nohup python /root/jixiangyu/vpn/shadowsocksr/shadowsocks/local.py -c /etc/shadowsocks/shadowsocks1.json 2>&1 &",
        "vpn2":"nohup python /root/jixiangyu/vpn/shadowsocksr/shadowsocks/local.py -c /etc/shadowsocks/shadowsocks2.json 2>&1 &",
        "vpn3":"nohup python /root/jixiangyu/vpn/shadowsocksr/shadowsocks/local.py -c /etc/shadowsocks/shadowsocks3.json 2>&1 &",
        "vpn4":"nohup python /root/jixiangyu/vpn/shadowsocksr/shadowsocks/local.py -c /etc/shadowsocks/shadowsocks4.json 2>&1 &",
        "vpn5":"nohup python /root/jixiangyu/vpn/shadowsocksr/shadowsocks/local.py -c /etc/shadowsocks/shadowsocks5.json 2>&1 &",
        "success": "线上数据已补回,程序已切换vpn{}节点,访问google成功",
        "warings":"所有节点都已经换过但是都没有访问成功"
    }

    RedisDB = {
            "host":"localhost",
            "port":6379,
            "password":"",
            "db":1
        }

    RedisDBHbdm = {
        "host": "localhost",
        "port": 6379,
        "password": "",
        "db": 3
    }

#开发环境
if ENV == 'online':
    DB = {
        "host": "rm-8vb3iaj44716y51sy0o.mysql.zhangbei.rds.aliyuncs.com",
        "port": 3306,
        "user": "finance_app",
        "passwd": "iqXm5H9ebeI",
        "db": "finance_data",
        "charset": "utf8mb4",
        "sms": ['18301544250','15810809217'],
        "es":{u'host': u'172.21.16.22', u'port': b'9200'},
        "waringfive": "由于网络原因,线上环境五分钟数据本次抓取只抓取到{}个币种,程序已杀掉{}进程,并重新开启{}进程，但是访问google没有成功",
        "waringfifteen": "由于网络原因,线上环境十五分钟数据本次抓取只抓取到{}个币种,程序已杀掉{}进程,并重新开启{}进程，但是访问google没有成功",
        "waringthirty": "由于网络原因,线上环境半小时数据本次抓取只抓取到{}个币种,程序已杀掉{}进程,并重新开启{}进程，但是访问google没有成功",
        "waringhour": "由于网络原因,线上环境一小时数据本次抓取只抓取到{}个币种,程序已杀掉{}进程,并重新开启{}进程，但是访问google没有成功",
        "waringday": "由于网络原因,线上环境一天数据本次抓取只抓取到{}个币种,程序已杀掉{}进程,并重新开启{}进程，但是访问google没有成功",
        "exponnet": "线上环境快链指数数据本次抓取只抓取到{}个币种",
        "feixiaohao": "线上环境非小号数据本次抓取只抓取到{}个币种",
        "startprocess": "程序已杀掉{}进程,并重新开启{}进程,线上{}缺失数据已经补完,访问google成功",
        "revison": "由于网络原因刚才抓取到的币种数量比较少,线上{}缺失数据已经补完",
        "vpn":"nohup python /root/jixiangyu/vpn/shadowsocksr/shadowsocks/local.py -c /etc/shadowsocks.json 2>&1 &",
        "vpn1":"nohup python /root/jixiangyu/vpn/shadowsocksr/shadowsocks/local.py -c /etc/shadowsocks/shadowsocks1.json 2>&1 &",
        "vpn2":"nohup python /root/jixiangyu/vpn/shadowsocksr/shadowsocks/local.py -c /etc/shadowsocks/shadowsocks2.json 2>&1 &",
        "vpn3":"nohup python /root/jixiangyu/vpn/shadowsocksr/shadowsocks/local.py -c /etc/shadowsocks/shadowsocks3.json 2>&1 &",
        "vpn4":"nohup python /root/jixiangyu/vpn/shadowsocksr/shadowsocks/local.py -c /etc/shadowsocks/shadowsocks4.json 2>&1 &",
        "vpn5":"nohup python /root/jixiangyu/vpn/shadowsocksr/shadowsocks/local.py -c /etc/shadowsocks/shadowsocks5.json 2>&1 &",
        "success":"线上数据已补回,程序已切换vpn{}节点,访问google成功",
        "warings": "线上所有节点都已经换过但是都没有访问成功"
    }

    RedisDB = {
        "host": "172.21.16.57",
        "port": 6379,
        "password": "kuailian2018",
        "db": 1
    }

#母婴开发环境
if ENV == "online_infant":
    DB = {
        "host":"172.21.16.42",
        "port":3306,
        "user":"root",
        "passwd":"KDC5pKhP7jxub",
        "db":"qimeng",
        "charset":"utf8mb4",
        "sms": ['18301544250'],
    }

#俊鹏
if ENV == 'junpeng':
    DB = {
        "host": "192.168.10.240",
        "port": 3306,
        "user": "root",
        "passwd": "root",
        "db": "klapp",
        "charset": "utf8mb4"
    }

#测试环境
if ENV == "test":
    DB = {
        "host":"123.57.33.212",
        "port":3306,
        "user":"root",
        "passwd":"cy2018xyz",
        "db":"kuailian",
        "charset":"utf8mb4",
        "sms": ['18301544250'],
        "es": {u'host': u'172.21.16.38', u'port': b'9200'},
        "waringfive":"测试环境五分钟数据本次抓取只抓取到{}个币种,程序已杀掉{}进程,并重新开启{}进程，访问google的是vpn1代理,并且访问google成功",
        "waringfifteen":"测试环境十五五分钟数据本次抓取只抓取到{}个币种,程序已杀掉{}进程,并重新开启{}进程，但是访问google没有成功",
        "waringthirty":"测试环境半小时数据本次抓取只抓取到{}个币种,程序已杀掉{}进程,并重新开启{}进程，但是访问google没有成功",
        "waringhour":"测试环境一小时数据本次抓取只抓取到{}个币种,程序已杀掉{}进程,并重新开启{}进程，但是访问google没有成功",
        "waringday": "测试环境一天数据本次抓取只抓取到{}个币种,程序已杀掉{}进程,并重新开启{}进程，但是访问google没有成功",
        "exponnet": "测试环境一小时快链指数数据本次抓取只抓取到{}个币种",
        "feixiaohao":"测试环境非小号数据本次抓取只抓取到{}个币种",
        "startprocess":"程序已杀掉{}进程,并重新开启{}进程,测试{}缺失数据已经补完,访问google成功",
        "revison": "由于网络原因刚才抓取到的币种数量比较少,测试{}缺失数据已经补完",
        "vpn":"nohup python /root/jixiangyu/shadowsocksr/shadowsocks/local.py -c /etc/shadowsocks.json 2>&1 &",
        "vpn1":"nohup python /root/jixiangyu/shadowsocksr/shadowsocks/local.py -c /etc/shadowsocks/shadowsocks1.json 2>&1 &",
        "vpn2":"nohup python /root/jixiangyu/shadowsocksr/shadowsocks/local.py -c /etc/shadowsocks/shadowsocks2.json 2>&1 &",
        "vpn3":"nohup python /root/jixiangyu/shadowsocksr/shadowsocks/local.py -c /etc/shadowsocks/shadowsocks3.json 2>&1 &",
        "vpn4":"nohup python /root/jixiangyu/shadowsocksr/shadowsocks/local.py -c /etc/shadowsocks/shadowsocks4.json 2>&1 &",
        "vpn5":"nohup python /root/jixiangyu/shadowsocksr/shadowsocks/local.py -c /etc/shadowsocks/shadowsocks5.json 2>&1 &",
        "success": "测试数据已补回,程序已切换vpn{}节点,访问google成功",
        "warings": "测试所有节点都已经换过但是都没有访问成功"
    }

    RedisDB = {
        "host":"172.21.16.65",
        "port":6379,
        "password":"Cdg3fD34fgrDrE3Gsio089hrDEer7gedEs4x",
        "db":1
    }

#母婴测试环境
if ENV == "test_infant":
    DB = {
        "host":"172.21.16.38",
        "port":3306,
        "user":"root",
        "passwd":"rzvNCHj9s4wVYA",
        "db":"qimeng",
        "charset":"utf8mb4",
        "sms": ['18301544250'],
    }

#汽车测试环境
if ENV == "car_test":
    DB = {
        "host":"172.21.16.38",
        "port":3306,
        "user":"root",
        "passwd":"rzvNCHj9s4wVYA",
        "db":"car",
        "charset":"utf8mb4",
        "sms": ['18301544250'],
    }

#Public TableMap
TableMap = {
    "SITES_TABLE": "sites",
    "NEWS_TABLE": "news",
    "FLASH": "flash",
    "FLASH_DETAIL": "flash_detail",
    "NEWS_DATAS_TABLE": "news_datas",
    "MARKETS_TABLE": "exchanges",
    "CER_TABLE": "currency_exchange_relationship",
    "CURRENCYS_TABLE": "currencies",
    "CATEGORIES_TABLE": "categories",
    "CURRENCY_DEALS_TABLE": "market_currency_deals_20180409",
    "APP_USERS_TABLE": "app_users",
    "KOL_USERS_TABLE": "kol_users",
    #母婴项目
    "KOL_USER_TABLE": "kol_user",
    "COMMENT_TABLE": "comment",
    "NEWS_TABLEs": "newss",
}

#categories
CATEGORIES = [
    {"id":4,"name":"头条"},
    {"id":5,"name":"财经"},
    {"id":18,"name":"深度"},
    {"id":25,"name":"项目"},
    {"id":11,"name":"学院"},
    {"id":19,"name":"快讯"},
    {"id":20,"name":"Twitter"},
    {"id":21,"name":"微博"},
]

CATEGORIES_SITE = [
    {"id":4,"name":"新闻-头条"},
    {"id":11,"name":"新闻-财经"},
    {"id":6,"name":"新闻-视频"},
    {"id":20,"name":"新闻-学院"},
    {"id":17,"name":"新闻-直播"},
    {"id":18,"name":"新闻-矿业"},
    {"id":19,"name":"新闻-产业"},
    {"id":21,"name":"新闻-政策"},
    {"id":22,"name":"快讯-金色财经"},
    {"id":23,"name":"快讯-火星财经"},
    {"id":26,"name":"快讯-巴比特"},
    {"id":27,"name":"快讯-币世界"},
    {"id":12,"name":"财经-全球财经"},
    {"id":15,"name":"新闻-公告"},
]

parent_site = {
    1: 1,
    2: 2,
    13: 3,
    14: 4
}
#sites
SITES = [
    {"id":1, "name":"火星财经"},
    {"id":2, "name":"新浪财经"},
    {"id":3, "name":"金色财经"},
    {"id":4, "name":"巴比特"},
    {"id":5, "name":"币世界"},
    {"id":6, "name":"全球财经"},
]

category_text_path_dicts = {
    "电影": "电影",
    "电视连续剧": "剧集",
    "动漫": "动漫",
    "综艺节目": "综艺",
    "纪录片": "纪录片",
    "预告片": "预告片",
    "电视剧": "剧集",
    "综艺": "综艺",
}
#author_img_list
author_img = ['http://static.chaingeworld.com/img/news/default_head_1.png', 'http://static.chaingeworld.com/img/news/default_head_2.png', 'http://static.chaingeworld.com/img/news/default_head_3.png', 'http://static.chaingeworld.com/img/news/default_head_4.png', 'http://static.chaingeworld.com/img/news/default_head_5.png', 'http://static.chaingeworld.com/img/news/default_head_6.png', 'http://static.chaingeworld.com/img/news/default_head_7.png', 'http://static.chaingeworld.com/img/news/default_head_8.png', 'http://static.chaingeworld.com/img/news/default_head_9.png', 'http://static.chaingeworld.com/img/news/default_head_10.png', 'http://static.chaingeworld.com/img/news/default_head_11.png', 'http://static.chaingeworld.com/img/news/default_head_12.png', 'http://static.chaingeworld.com/img/news/default_head_13.png', 'http://static.chaingeworld.com/img/news/default_head_14.png', 'http://static.chaingeworld.com/img/news/default_head_15.png', 'http://static.chaingeworld.com/img/news/default_head_16.png', 'http://static.chaingeworld.com/img/news/default_head_17.png', 'http://static.chaingeworld.com/img/news/default_head_18.png', 'http://static.chaingeworld.com/img/news/default_head_19.png', 'http://static.chaingeworld.com/img/news/default_head_20.png', 'http://static.chaingeworld.com/img/news/default_head_21.png', 'http://static.chaingeworld.com/img/news/default_head_22.png', 'http://static.chaingeworld.com/img/news/default_head_23.png', 'http://static.chaingeworld.com/img/news/default_head_24.png', 'http://static.chaingeworld.com/img/news/default_head_25.png', 'http://static.chaingeworld.com/img/news/default_head_26.png', 'http://static.chaingeworld.com/img/news/default_head_27.png', 'http://static.chaingeworld.com/img/news/default_head_28.png', 'http://static.chaingeworld.com/img/news/default_head_29.png', 'http://static.chaingeworld.com/img/news/default_head_30.png', 'http://static.chaingeworld.com/img/news/default_head_31.png', 'http://static.chaingeworld.com/img/news/default_head_32.png', 'http://static.chaingeworld.com/img/news/default_head_33.png', 'http://static.chaingeworld.com/img/news/default_head_34.png', 'http://static.chaingeworld.com/img/news/default_head_35.png', 'http://static.chaingeworld.com/img/news/default_head_36.png', 'http://static.chaingeworld.com/img/news/default_head_37.png', 'http://static.chaingeworld.com/img/news/default_head_38.png', 'http://static.chaingeworld.com/img/news/default_head_39.png', 'http://static.chaingeworld.com/img/news/default_head_40.png', 'http://static.chaingeworld.com/img/news/default_head_41.png', 'http://static.chaingeworld.com/img/news/default_head_42.png', 'http://static.chaingeworld.com/img/news/default_head_43.png', 'http://static.chaingeworld.com/img/news/default_head_44.png', 'http://static.chaingeworld.com/img/news/default_head_45.png', 'http://static.chaingeworld.com/img/news/default_head_46.png', 'http://static.chaingeworld.com/img/news/default_head_47.png', 'http://static.chaingeworld.com/img/news/default_head_48.png', 'http://static.chaingeworld.com/img/news/default_head_49.png', 'http://static.chaingeworld.com/img/news/default_head_50.png']

#muying_img_list
author_baby_list = ['http://baby.chainplanet.cn/image/head/1.png', 'http://baby.chainplanet.cn/image/head/2.png', 'http://baby.chainplanet.cn/image/head/3.png', 'http://baby.chainplanet.cn/image/head/4.png', 'http://baby.chainplanet.cn/image/head/5.png', 'http://baby.chainplanet.cn/image/head/6.png', 'http://baby.chainplanet.cn/image/head/7.png', 'http://baby.chainplanet.cn/image/head/8.png', 'http://baby.chainplanet.cn/image/head/9.png', 'http://baby.chainplanet.cn/image/head/10.png', 'http://baby.chainplanet.cn/image/head/11.png', 'http://baby.chainplanet.cn/image/head/12.png', 'http://baby.chainplanet.cn/image/head/13.png', 'http://baby.chainplanet.cn/image/head/14.png', 'http://baby.chainplanet.cn/image/head/15.png', 'http://baby.chainplanet.cn/image/head/16.png', 'http://baby.chainplanet.cn/image/head/17.png', 'http://baby.chainplanet.cn/image/head/18.png', 'http://baby.chainplanet.cn/image/head/19.png', 'http://baby.chainplanet.cn/image/head/20.png', 'http://baby.chainplanet.cn/image/head/21.png', 'http://baby.chainplanet.cn/image/head/22.png', 'http://baby.chainplanet.cn/image/head/23.png', 'http://baby.chainplanet.cn/image/head/24.png', 'http://baby.chainplanet.cn/image/head/25.png', 'http://baby.chainplanet.cn/image/head/26.png', 'http://baby.chainplanet.cn/image/head/27.png', 'http://baby.chainplanet.cn/image/head/28.png', 'http://baby.chainplanet.cn/image/head/29.png', 'http://baby.chainplanet.cn/image/head/30.png', 'http://baby.chainplanet.cn/image/head/31.png', 'http://baby.chainplanet.cn/image/head/32.png', 'http://baby.chainplanet.cn/image/head/33.png', 'http://baby.chainplanet.cn/image/head/34.png', 'http://baby.chainplanet.cn/image/head/35.png', 'http://baby.chainplanet.cn/image/head/36.png', 'http://baby.chainplanet.cn/image/head/37.png', 'http://baby.chainplanet.cn/image/head/38.png', 'http://baby.chainplanet.cn/image/head/39.png', 'http://baby.chainplanet.cn/image/head/40.png', 'http://baby.chainplanet.cn/image/head/41.png', 'http://baby.chainplanet.cn/image/head/42.png', 'http://baby.chainplanet.cn/image/head/43.png', 'http://baby.chainplanet.cn/image/head/44.png', 'http://baby.chainplanet.cn/image/head/45.png', 'http://baby.chainplanet.cn/image/head/46.png', 'http://baby.chainplanet.cn/image/head/47.png', 'http://baby.chainplanet.cn/image/head/48.png', 'http://baby.chainplanet.cn/image/head/49.png', 'http://baby.chainplanet.cn/image/head/50.png']

#Status
AUDIT_OK = 2 #审核通过
AUDIT_FAIL = 3 #审核不通过

#Log Type
LOG_SYSTEM = 1 # 系统日志
LOG_QUEUE  = 2 # 队列日志

#Log Level
CRITICAL = 50  # 临界值错误: 超过临界值的错误，例如一天24小时，而输入的是25小时这样
ERROR = 40     # 一般错误: 一般性错误
WARNING = 30   # 警告性错误: 需要发出警告的错误
INFO = 20      # 信息: 程序输出信息
DEBUG = 10     # 调试: 调试信息

LOG_RECORD = 1 #是否开启日志记录
LOG_LEVEL = "DEBUG,ERROR,INFO"
LOG_FILE_SIZE = 1024000 #日志尺寸

#except
KD_ERROR = 0 #程序异常

#exchange time
EXCHANGE_REQUEST_RATE = 30 #请求频次每10秒请求一次
EXCHANGE_EXCEPT_RATE = 30 #异常时等待秒数
EXCHANGE_INDEX_RATE = 3600 #异常时等待秒数
EXCHANGE_FEI_RATE = 50 #异常时等待秒数
EXCHANGE_BLOCK_RATE = 120 #异常时等待秒数


NEWS_REQUEST_RATE = 2400 #1小时刷新一次
NEWS_DETAIL_RATE = 1 #请求detail页时停留1秒
NEWS_LIMIT = 20 #每次请求记录数
RATE_REQUEST = 3600   #汇率请求更新时间

#BUY/SELL
KD_BUY = 0 #买入
KD_SELL = 1 #卖出

#requests_num
REQUEST_NUM = 3

MUYING_REQUEST_NUM = 3

#logoutput
# 1 输出LOG日志，适合使用supervistor方式启动的程序，输出的异常信息会写入日志文件
# 2 写入日志文件，将日志文件写入到根目录的logs文件夹
LOG_OUTPUT = 2


