#! /usr/bin env python
# coding:utf-8
# 业务逻辑

import Config
import pymysql
from Utils import getCurFullDay, D
class Logic:

    def __init__(self, db):
        print(db)
        self.db = pymysql.connect(
            host=Config.DB.get("host"),
            port=Config.DB.get("port"),
            user=Config.DB.get("user"),
            passwd=Config.DB.get("passwd"),
            db=db,
            charset=Config.DB.get("charset")
        )
        self.cursor = self.db.cursor(cursor=pymysql.cursors.DictCursor)

    # 获取文章分类信息
    def getCategoryByName(self, name):
        res = [x for x in Config.CATEGORIES_SITE if x['name'] == name]
        if 0 == len(res):
            return {}
        else:
            return res[0]

            # 获取站点信息

    def getSiteByName(self, name):
        res = [x for x in Config.SITES if x['name'] == name]
        print(res)
        if 0 == len(res):
            return {}
        else:
            return res[0]

    # 获取文章信息
    def getNewsByNewsid(self, newsid):
        sql = 'select * from %s where `newsid` = "%s"' % (
            Config.TableMap.get('NEWS_TABLE'), newsid)
        self.cursor.execute(sql)  # 如果找到返回主键id，否则返回0
        res = self.cursor.fetchone()
        if res:
            return res
        else:
            return {}

    # 获取categories下的parent_id
    def getParentid(self, id):
        sql = 'select `parent_id` from %s where `id` = "%s"' % (
            "categories", id)
        self.cursor.execute(sql)  # 如果找到返回主键id，否则返回0
        res = self.cursor.fetchone()
        if res:
            return res['parent_id']
        else:
            return {}

    # 获取头条文章信息
    def getNewsid(self, newsid,source):
        sql = 'select * from %s where `repeat_flag` = "%s" and `source` = "%s"' % (
            Config.TableMap.get('NEWS_TABLE'), newsid,source)
        self.cursor.execute(sql)  # 如果找到返回主键id，否则返回0
        res = self.cursor.fetchone()
        if res:
            return res
        else:
            return {}

    # 获取交易所信息
    def getMarketByName(self, marketname):
        sql = 'select * from '+Config.TableMap.get('MARKETS_TABLE')+' where `name` like "%'+marketname+'%"'
        self.cursor.execute(sql)  # 如果找到返回主键id，否则返回0
        res = self.cursor.fetchone()
        if res:
            return res
        else:
            return {}

    # 获取币种
    def getCurrencyByName(self):
        sql = 'select `id`, `name` from %s' % (
            "futures")
        self.cursor.execute(sql)  # 如果找到返回主键id，否则返回0
        res = self.cursor.fetchall()
        if res:
            return res
        else:
            return {}

    # 获取币种列表
    def getCurrencyList(self):
        sql = 'select * from %s where is_del = %d' % (Config.TableMap.get('CURRENCYS_TABLE'), 0)
        self.cursor.execute(sql)  # 如果找到返回主键id，否则返回0
        res = self.cursor.fetchall()
        if res:
            return res
        else:
            return {}


    # 获取交易所列表
    def getRate(self,from_currency='usd',to_currency='cny'):
        sql = 'select * from %s where `from_currency` = "%s" and to_currency = "%s"' % (
                'rates', 'usd', 'cny')
        self.cursor.execute(sql)  # 如果找到返回主键id，否则返回0
        res = self.cursor.fetchall()
        if res:
            return res
        else:
            return {}

    #获取用户名称id
    def getNickId(self,nick):
        sql = "select * from %s where `nick` = '%s'" % (
            'app_users',nick )
        self.cursor.execute(sql)  # 如果找到返回主键id，否则返回0
        res = self.cursor.fetchone()
        if res:
            return res
        else:
            return {}

    # 获取作者图像的地址
    def getAuthor(self,author):
        sql = "select img from %s WHERE  `nick` = '%s'"%('app_users',author)
        self.cursor.execute(sql)
        res = self.cursor.fetchone()
        if res:
            return res
        else:
            return {}

    def insertAuthorImg(self, author, authorImg=''):
        # insert
        values = "'%s','%s'" % (author, authorImg)
        sql = "insert into %s (" \
              "`nick`," \
              "`img`" \
              ") values (%s)" % (
                  Config.TableMap.get('APP_USERS_TABLE'), values)
        D("Insert: %s" % sql)
        self.cursor.execute(sql)
        user_id = self.cursor.lastrowid
        # #插入文章内容
        sql = "insert into %s (`user_id`, `source`) values (%s, %s)" % (
            Config.TableMap.get("KOL_USERS_TABLE"), user_id, 2)
        self.cursor.execute(sql)
        # 提交事物
        self.db.commit()
        return user_id

    #获取k_id是否存在
    def getKid(self,tables,times,id):
        sql = 'SELECT * FROM %s WHERE `k_id` = "%s" AND `c_id` = "%s"'%(tables,times,id)
        self.cursor.execute(sql)
        res = self.cursor.fetchall()
        if res:
            return res
        else:
            return {}

            # 获取k_id是否存在

    def getData(self, tables, source, id):
        sql = 'SELECT * FROM %s WHERE `source` = %s AND `c_id` = "%s"' % (tables, source, id)
        self.cursor.execute(sql)
        res = self.cursor.fetchall()
        if res:
            return res
        else:
            return {}

    def getCnyprice(self,c_id,tables):
        sql = 'SELECT `cnyprice`,`price` FROM %s WHERE c_id = %s ORDER BY `created_at` DESC LIMIT %s,%s'%(tables,c_id,0,1)
        print(sql)
        self.cursor.execute(sql)
        res = self.cursor.fetchall()
        if res:
            return res
        else:
            return {}

    def get_date(self, table_name, future_id, date):
        sql = ' SELECT * from %s WHERE futures_id = %s AND market_time = "%s"' % (
        table_name, future_id,date)
        # print(sql)
        self.cursor.execute(sql)
        res = self.cursor.fetchall()
        if res:
            return res
        else:
            return {}

    #母婴获取作者id
    def get_futures_id(self, name, chinese_name, flag=1):
        sql = "select * from %s where `name` = '%s'" % (
            'futures', name)
        self.cursor.execute(sql)  # 如果找到返回主键id，否则返回0
        res = self.cursor.fetchone()
        # print("查询出来的结果 {}".format(res))
        dates = getCurFullDay()
        if flag==1:
            if res:
                sql = "UPDATE %s SET `name` = '%s', `introduce` = '%s', `create_at` = '%s', `update_at` = '%s'  WHERE name = '%s'" % ("futures", name, chinese_name, dates, dates, name)
                # print("UPDATE: %s" % sql)
                self.cursor.execute(sql)
            else:
                # insert
                values = "'%s','%s','%s','%s'" % (name, chinese_name, dates, dates)
                sql = "insert into %s (" \
                      "`name`," \
                      "`introduce`," \
                      "`create_at`," \
                      "`update_at`" \
                      ") values (%s)" % (
                          'futures', values)
                # print("Insert: %s" % sql)
                self.cursor.execute(sql)
        elif flag == 2:
            if res:
                sql = "UPDATE %s SET `name` = '%s',  `create_at` = '%s', `update_at` = '%s'  WHERE name = '%s'" % (
                "futures", name, dates, dates, name)
                # print("UPDATE: %s" % sql)
                self.cursor.execute(sql)
            else:
                # insert
                values = "'%s','%s','%s'" % (name, dates, dates)
                sql = "insert into %s (" \
                      "`name`," \
                      "`create_at`," \
                      "`update_at`" \
                      ") values (%s)" % (
                          'futures', values)
                # print("Insert: %s" % sql)
                self.cursor.execute(sql)
        self.db.commit()
        if res:
            return res
        else:
            return {}

    def get_currenies(self):
        sql = 'SELECT `id`, `name`, `categroy` FROM `currencies`'
        # print(sql)
        self.cursor.execute(sql)
        res = self.cursor.fetchall()
        if res:
            return res
        else:
            return {}

    def get_currenies_c_id(self, c_id):
        sql = 'SELECT `c_id` FROM `feixiaohao` WHERE c_id=%s'%(c_id)
        self.cursor.execute(sql)
        res = self.cursor.fetchall()
        if res:
            return res
        else:
            return {}
