#!/usr/bin/python
# coding:utf-8
# 公共方法库

import time, datetime
import hashlib
import Config
import re
from bs4 import BeautifulSoup
import time

# 获取昨天的日期
def getYesterday():
    now_time = datetime.datetime.now()  # 返回的是本地时区的当前时间
    yesterday = now_time + datetime.timedelta(days=-1)  # 昨天的现在
    return yesterday.strftime("%Y-%m-%d")

# 获取昨天的日期
def getYesterdays(a):
    now_time = datetime.datetime.now()  # 返回的是本地时区的当前时间
    yesterday = now_time + datetime.timedelta(days=-a)  # 昨天的现在
    return yesterday.strftime("%Y-%m-%d")

# 获取当前日期
def getCurDay():
    nowtime = datetime.datetime.now()  # 返回的是本地时区的当前时间
    return nowtime.strftime("%Y_%m_%d")

# 获取当前日期
def getCurFullDay():
    nowtime = datetime.datetime.now()  # 返回的是本地时区的当前时间
    return nowtime.strftime("%Y-%m-%d %H:%M:%S")

# # post请求自动加载cookie文件
# def post(url='', formdata={}, headers={}):
#     mycookie = cookielib.MozillaCookieJar()
#     mycookie.load('cookie.text', ignore_discard=True, ignore_expires=True)
#     opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(mycookie))  # cookie和opener绑定
#     urllib2.install_opener(opener)
#     req = urllib2.Request(url, data=formdata, headers=headers)
#     res = opener.open(req)
#     return res.read()


# 获取毫秒数
def getMicSecond():
    t = time.time()
    return (int(round(t * 1000)))


def md5(src):
    m2 = hashlib.md5()
    m2.update(src)
    return m2.hexdigest()


# def log(msg="", log_type=Config.LOG_SYSTEM, log_level=Config.ERROR, log_file="", log_prefix="log"):
#     # 日志对象
#     l = Log()
#     if Config.LOG_RECORD and msg != "":
#         # 写日志
#         l.write(msg, log_file, log_prefix, log_type, log_level)

# 去字符串中的回车换行付
def denter(str=''):
    if '' == str:
        return ''
    return re.sub(r'[\r|\n|\r\n|\t]', "", str.strip())

#输出
def D(val):
    if Config.KD_DEBUG:
        print(val)

#获取Html
def getHtml(html, selector):
    try:
        soup = BeautifulSoup(html, 'lxml')
        articleContent = soup.select(selector)[0]
        return bytes(articleContent)
    except:
        return ""

#获取时间戳时间
def date(date):
    a = int(date)
    # 转换成localtime
    time_local = time.localtime(a)
    # 转换成新的时间格式(2016-05-05 20:28:54)
    dt = time.strftime("%Y-%m-%d %H:%M:%S", time_local)
    return dt

def a():
    from selenium import webdriver

    drive = webdriver.PhantomJS()
    drive.get("https://www.toutiao.com/")
    print(drive.get_cookies())
