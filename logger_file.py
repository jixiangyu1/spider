# -*- coding: utf-8 -*-
"""爬虫启动逻辑日志系统
"""
import os
import time
import logging
import platform
# import winreg


system = platform.platform()


# def get_desktop():
#     key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, 'Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders')
#     return winreg.QueryValueEx(key, "Desktop")[0]


if 'windows' in system.lower():
    LOG_FILE_PATH = 'E:\\history' + '\\PECO'

else:
    LOG_FILE_PATH = '/export/Logs/python_log'


class LoggerInit(object):
    """初始化日志功能，
       配置日志文件
    """
    def __init__(self):
        self.time_str = time.strftime("%Y-%m-%d")

    def _file_is_exist(self):
        """判断日志文件是否存在
        """
        if not self.time_str == time.strftime("%Y-%m-%d"):
            self.time_str = time.strftime("%Y-%m-%d")
        isExists = os.path.exists(LOG_FILE_PATH + '/' + self.time_str)
        return isExists

    def mkdir_file(self):
        """创建日志文件
           如果日志文件存在则不创建
           如果日志文件不存在就创建文件
        """
        if not self._file_is_exist():
            try:
                os.makedirs(LOG_FILE_PATH + '/' + self.time_str)
            except:
                pass

    def init_log(self, file_path):
        # 按日期创建日志文件夹
        self.mkdir_file()
        logger = logging.getLogger(file_path)
        logger.setLevel(logging.DEBUG)
        fh = logging.FileHandler(LOG_FILE_PATH + '/' + self.time_str + "/" + file_path)
        fh.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        logger.addHandler(fh)
        return logger


# 日志功能入口函数
def logger(log_file_name):
    logger = LoggerInit()
    return logger.init_log(log_file_name)


if __name__ == '__main__':
    log = logger('test.log')
    while True:
        log.info("test" + time.strftime("%Y-%m-%d"))
        time.sleep(1)
        print(time.strftime("%Y-%m-%d"))
