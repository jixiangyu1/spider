#encoding:utf8
import requests

#encoding:utf8
import json
import re
import requests
from lxml import etree
from urllib.parse import unquote,quote
from Config import category_text_path_dicts
import hashlib
import time
import traceback
import queue
import threading
from gevent import monkey
from dilili.request_class import ReqCls
monkey.patch_all()

import gevent

orderno = "ZF2020411942R36FBe"
secret = "ca9153d6fce24c98affba07bcf819e3e"
ip_forward = "forward.xdaili.cn"

def auth_proxy():
    timestamp = str(int(time.time()))
    string = ""
    string = "orderno=" + orderno + "," + "secret=" + secret + "," + "timestamp=" + timestamp

    string = string.encode()

    md5_string = hashlib.md5(string).hexdigest()
    sign = md5_string.upper()
    # print(sign)
    auth = "sign=" + sign + "&" + "orderno=" + orderno + "&" + "timestamp=" + timestamp

    return auth


port = "80"
ip_port = ip_forward + ":" + port
privoxy = {"http": "http://" + ip_port, "https": "https://" + ip_port}

class Dll():
    def __init__(self):
        self.headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36",
            # "Proxy-Authorization": auth_proxy()

        }
        self.lists = []
        self.q = queue.Queue(maxsize=5000)
        self.list_end = True
        self.req = ReqCls()
        self.repeat_item_num = 0

    def re_date(self, responses):
        try:
            reqs = re.compile("年份：(.*?)剧情", re.S)
            date = (reqs.findall(responses))
            if date == []:
                reqs = re.compile("首播：(.*?)", re.S)
                date = (reqs.findall(responses))
            return date[0]
        except:
            return 2000

    def re_country(self, responses):
        try:
            country_list = []
            req_country = re.compile("地区：(.*?)年份", re.S)
            country = (req_country.findall(responses))[0]

            if "/" in country:
                country_list = country.split("/")
                return country_list
            country_list.append(country)
            return country_list
        except:
            return ""

    def re_link(self, response):
        # print(response)
        re_link = re.compile(r'var cms_player = (.*?);', re.S)
        link = re_link.findall(response)
        # print("link", link)
        return link[0]

    def re_plot(self, responses):
        try:
            req_score = re.compile("剧情：(.*?)详情", re.S)
            plot = (req_score.findall(responses))[0]
            if plot == "暂无":
                return ""
            return plot
        except:
            return ""

    def re_source(self, pp):
        reqs = re.compile("-(.*?)-", re.S)
        source = (reqs.findall(pp))
        return source[0]

    def re_schedule(self, responses):
        try:
            req_score = re.compile("集数: (.*?)\\n", re.S)
            schedule_count = (req_score.findall(responses))[0]
            return schedule_count
        except:
            return ""

    def re_jishu_sort(self, responses):
        # print("传入的技术", responses)
        try:
            # req_sort = re.findall('\d+',ss)
            schedule_count = (re.findall('\d+', responses))[0]
            return schedule_count
        except:
            return ""

    def kkkkk(self, urls):
        # print("请求的url", urls)
        pass
        # return video_link

    def xpath_details(self,k):
        items = {}
        try:
            items['name'] = k.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/h2/a/text()")[0]
            print(items['name'])
            items['link'] = k.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/h2/a/@href")[0]

            if k.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/h2/small/text()") == []:
                items['schedule'] = ""
            else:
                items['schedule'] = str(k.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/h2/small/text()")[0]).strip()
            tag_text = k.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/dl/dd[3]/a/text()")
            tag_href = k.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/dl/dd[3]/a/@href")
            tags = []
            tags_str = []
            for j in zip(tag_text, tag_href):
                tags_href = j[1]
                tags_text = j[0]
                tags.append((tags_href, tags_text))
                tags_str.append(tags_text)
            if tags == []:
                print("tags None")
                items['tags'] = tags
                items['tags_str'] = tags_str
            else:
                items['tags'] = tags
                items['tags_str'] = tags_str

            category_text_path = k.xpath('/html/body/div[1]/div[1]/h2/a[1]/text()')[0]
            print(category_text_path)
            category_text = category_text_path_dicts[category_text_path]
            items['video_type'] = category_text

            video_detail = k.xpath('//dl[@class="dl-horizontal"]')[0]
            video_detail_re = k.xpath('//dl[@class="dl-horizontal"]')[0].xpath('string(.)')
            video_detail_re = "".join(str(video_detail_re).split())
            release_date = self.re_date(video_detail_re)
            items['release_date'] = release_date
            country_list = self.re_country(video_detail_re)
            items['country_list'] = country_list
            plot = self.re_plot(video_detail_re)
            items['plot'] = plot
            # schedule_count = self.re_schedule(video_detail_re)
            # items['schedule_count'] = schedule_count
            video_detail_content = etree.tostring(video_detail, method='html')
            items['video_detail_content'] = str(video_detail_content).replace("\\n", "")
            cover = k.xpath("/html/body/div[1]/div[2]/div[1]/div/div[1]/a/img/@data-original")[0]
            items['cover'] = cover
            video_detail_link_list = []
            video_detail_link_quchong_list = []
            # address_name = k.xpath('//div[@class="page-header ff-playurl-line"]//h2/text()')
            address_list = k.xpath('//ul[@class="list-unstyled row text-center ff-playurl-line ff-playurl"]/li/a/@href')
            address_name = k.xpath('//ul[@class="list-unstyled row text-center ff-playurl-line ff-playurl"]/li/a/text()')
            print(address_name)
            print(address_list)
            video_detail_link_list = []
            dicts_source = {}
            for kk in zip(address_name, address_list):
                print(kk)
                response_video_link = ""
                pp = 0

                source = self.re_source(kk[1])
                try:
                    response_video_link = self.req.req_html("https://www.suiyikan.tv" + kk[1], parses="response_video_link_suiyikan")
                    link = self.re_link(response_video_link)
                    link = json.loads(link)
                    video_link = link.get('url')
                    # print(video_link)
                    # sort_jishu = self.re_jishu_sort(kk)
            #
                    if video_link.endswith("m3u8"):
                        if source in dicts_source:
                            dicts_source[source].append({"name": kk[0], "url": video_link})
            #
                        else:
                            dicts_source[source] = [{"name": kk[0], "url": video_link}]
                    else:
                        urls  = "https://jx.4dm.cc/by/api.php?url=" + video_link
                        response_video_link_two = self.req.req_html(urls, parses="response_video_link_two_detail_url")
                        if response_video_link_two == "fail":
                            continue
                        json_response_video_link_two = json.loads(response_video_link_two)
                        video_link = json_response_video_link_two.get("url")
                        # # print("请求的video_link", video_link)
                        if video_link == None:
                            print("urls", urls, "None", video_link)

                        if source in dicts_source:
                            dicts_source[source].append({"name": kk[0], "url": video_link})

                        else:
                            dicts_source[source] = [{"name": kk[0], "url": video_link}]


                        break
                except Exception as e:
                    print(e)
                    # print(response_video_link)
                    pp+=1
                    if pp==10:
                        print("name {} 访问失败".format(items['name']))
                        continue

                        # print("访问urlchucuo {}".format("https://www.suiyikan.tv" + kk[1]))
            if dicts_source == {}:
                return ""
            items['video_link_zip'] = [dicts_source]
            if items['video_type'] == "电影":
                items['schedule_status'] = 1

            elif items['video_type'] == "剧集" or items['video_type'] == "动漫":
                if items['schedule'] == "":
                    items['schedule_status'] = 0
                elif "全" in items['schedule']:
                    items['schedule_status'] = 1
                else:
                    items['schedule_status'] = 0



            print(json.dumps(items))
            self.lists.append(json.dumps(items))
            print(self.lists)
            return items
        except Exception   as e:
            print(traceback.print_exc())
            return ""

    def req_link(self, dicts):
        url = dicts.get("url")
        category_text = dicts.get("category_text")
        response = self.req.req_html(url, "response_index")
        html = etree.HTML(response)
        link_all = html.xpath('//span[@class="pull-right"]/a/@href')
        print(link_all)
        for i in link_all:
            more_url =  "https://www.suiyikan.tv" + i
            more_url = re.sub("addtime", "addtime-{}", more_url)

            for indexs in range(1,2):
                print(more_url.format(indexs))
                more_response = self.req.req_html(more_url.format(indexs), parses="more_response")
                # print(more_response)
                more_html = etree.HTML(more_response)
                detail_url_href = more_html.xpath("//ul[@class='list-unstyled vod-item-img ff-img-215']/li/p/a/@href")
                # detail_url_img = more_html.xpath("//ul[@class='list-unstyled vod-item-img ff-img-215']/li/p/a/img/@data-original")
                print(detail_url_href)
                print(len(detail_url_href))
                for j in detail_url_href[:1]:
                    self.q.put("http://suiyikan.tv" + j)

    def requests_detail(self, detail_url):
        try:
            response_detail = self.req.req_html(detail_url, parses="response_detail")
            # if response_detail.status_code == 200:
            html = etree.HTML(response_detail)
            self.xpath_details(html)
                # title = html.xpath('/html/body/div[1]/div[2]/div[1]/div/div[2]/h2/a/text()')
                # print(title)

            while True:
                if self.q.empty() == False:
                    print("线程再次进入取url函数")
                    result = self.q.get()
                    print("队列长度", self.q.qsize())
                    with open("aa.txt", "w+") as f:
                        f.write(json.dumps(self.lists))
                        print("写入成功")
                    self.requests_detail(result)
                    # break

                elif self.q.empty() == True and self.list_end == False:
                    print("线程结束")

                    self.repeat_item_num += 1
                    if self.repeat_item_num == 60:
                        print("所有线程结束")
                        print(int(time.time()) - starttime)
                    exit(0)

                elif self.q.empty() == True and self.list_end == True:
                    print("正在等待")
                    time.sleep(2)
        except requests.exceptions.ConnectionError:
            print("url 链接异常 ", detail_url)
            self.repeat_item_num += 1

        except requests.exceptions.Timeout:
            print("url 等待超时 ", detail_url)
            self.repeat_item_num += 1


    def get_detail_url(self):
        time.sleep(5)
        print("程序启动")
        while True:
            print("目前的q {} {}".format(self.q.empty(), self.list_end))
            n = 0

            while not self.q.empty() and self.list_end == False:
                print("队列长度", self.q.qsize())
                result = self.q.get()
                t = threading.Thread(target=self.requests_detail, args=(result,))
                t.start()

                n += 1
                if n == 30:
                    return

            if self.q.empty() == True and self.list_end == False:
                print("进入结束页面")
                return "1"

            elif self.list_end == True:
                print("进入线程等待页面 主线程没有跑完")
                time.sleep(10)

            elif self.q.empty() == True and self.list_end == True:
                print("正在等待")
                time.sleep(2)

    def start(self):
        t1 = threading.Thread(target=self.get_detail_url)
        t1.start()


        for i in ppp:
            q.req_link(i)
        self.list_end = False

if __name__ == '__main__':

    starttime = time.time()
    q = Dll()
    ppp = [
        # {"url": 'https://www.suiyikan.tv/dianshiju/', "category_text":"剧集"},
        # {"url": 'https://www.suiyikan.tv/dianying/', "category_text":"电影"},
        {"url": 'https://www.suiyikan.tv/dongman/', "category_text":"动漫"},
        # {"url": 'https://www.suiyikan.tv/zongyi/', "category_text":"综艺"},
    ]


    q.start()
    # print(re_date())