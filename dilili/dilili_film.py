#encoding:utf8
import json
import re
import requests
from lxml import etree
from urllib.parse import unquote,quote
from Config import category_text_path_dicts
import hashlib
import time
from dilili.request_class import ReqCls
from dilili.film_logic import Dilili_logic, Dilili_index_logic, Dilili_banner
import traceback
import threading
import queue
orderno = "ZF2020411942R36FBe"
secret = "ca9153d6fce24c98affba07bcf819e3e"
ip_forward = "forward.xdaili.cn"

def auth_proxy():
    timestamp = str(int(time.time()))
    string = ""
    string = "orderno=" + orderno + "," + "secret=" + secret + "," + "timestamp=" + timestamp

    string = string.encode()

    md5_string = hashlib.md5(string).hexdigest()
    sign = md5_string.upper()
    # print(sign)
    auth = "sign=" + sign + "&" + "orderno=" + orderno + "&" + "timestamp=" + timestamp

    return auth


port = "80"
ip_port = ip_forward + ":" + port
privoxy = {"http": "http://" + ip_port, "https": "https://" + ip_port}

class Dll():
    def __init__(self):
        self.req = ReqCls()
        self.lists = []
        self.logic = Dilili_logic()
        self.index_logic = Dilili_index_logic()
        self.banner_logic = Dilili_banner()
        self.q = queue.Queue(maxsize=5000)
        self.list_end = True
        self.repeat_item_num = 0
        self.headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36",
            # "Proxy-Authorization": auth_proxy()

        }
    def re_date(self, responses):
        try:
            reqs = re.compile("上映日期: (.*?)-", re.S)
            date = (reqs.findall(responses))
            if date == []:
                reqs = re.compile("首播: (.*?)-", re.S)
                date = (reqs.findall(responses))
            return date[0][:4]
        except:
            return 2000

    def re_country(self, responses):
        try:
            country_list = []
            req_country = re.compile("国家/地区: (.*?)\\n", re.S)
            country = (req_country.findall(responses))[0]

            if "/" in country:
                country_list = country.split("/")
                return country_list
            country_list.append(country)
            return country_list
        except:
            return ""

    def re_score(self, responses):
        try:
            req_score = re.compile("评分: (.*?)\\n", re.S)
            score = (req_score.findall(responses))[0]
            if score == "暂无":
                return 0
            return score
        except:
            return 0

    def re_schedule(self, responses):
        try:
            req_score = re.compile("集数: (.*?)\\n", re.S)
            schedule_count = (req_score.findall(responses))[0]
            return schedule_count
        except:
            return ""

    def re_category_count(self, response):
        print("sdg", response)
        try:
            req_score = re.compile("共 (.*?) 页", re.S)
            schedule_count = (req_score.findall(response))[0]
            return schedule_count
        except:
            return ""

    def xpath_details(self,k):
        items = {}
        try:
            items['name'] = k.xpath("./a/h2/text()")[0]
            print(items['name'])
            items['link'] = k.xpath("./a/@href")[0]

            if k.xpath("./a/div/div[2]/span/text()") == []:
                items['schedule'] = ""
            else:
                items['schedule'] = k.xpath("./a/div/div[2]/span/text()")[0]

            tags = []
            tags_str = []
            for j in k.xpath("./div/span/a"):
                tags_href = j.xpath('./@href')[0]
                tags_text = j.xpath('./text()')[0]
                tags.append((tags_href, tags_text))
                tags_str.append(tags_text)
            if tags == []:
                print("tags None")
                items['tags'] = tags
                items['tags_str'] = tags_str
            else:
                items['tags'] = tags
                items['tags_str'] = tags_str

            response_video_detail = self.req.req_html(items['link'], parses="response_video_detail")
            if response_video_detail == "fail":
                print(items['link'], "fail")
                return ""
            html_detail = etree.HTML(response_video_detail)
            category_text_path = html_detail.xpath('/html/body/section/div/div/div/header/div/a[2]/text()')[0]
            print(category_text_path)
            category_text = category_text_path_dicts[category_text_path]
            items['video_type'] = category_text
            try:
                plot = html_detail.xpath('//p[@class="jianjie"]/span/text()')[0]
                items['plot'] = plot
            except:
                items['plot'] = ""
            video_detail = html_detail.xpath('//div[@class="video_info"]')[0]
            video_detail_re = html_detail.xpath('//div[@class="video_info"]')[0].xpath('string(.)')
            release_date = self.re_date(video_detail_re)
            items['release_date'] = release_date
            country_list = self.re_country(video_detail_re)
            items['country_list'] = country_list
            score = self.re_score(video_detail_re)
            items['score'] = score
            schedule_count = self.re_schedule(video_detail_re)
            items['schedule_count'] = schedule_count
            video_detail_content = etree.tostring(video_detail, method='html')
            items['video_detail_content'] = str(video_detail_content).replace("\\n", "")
            cover = html_detail.xpath("/html/body/section/div/div/div/article/div[1]/div[1]/img/@src")[0]
            items['cover'] = cover
            video_detail_link_list = []
            video_detail_link_quchong_list = []
            for kk in html_detail.xpath('//div[@class="vlink"]/a'):
                print("http://www.dililitv.com/vplay/" + kk.xpath("./@id")[0])
                video_link_name = kk.xpath("./text()")[0]
                pp = 0
                while True:
                    try:
                        response_video_link = self.req.req_html("http://www.dililitv.com/vplay/" + kk.xpath("./@id")[0],  parses="response_video_link")
                        if response_video_link == "fail":
                            print("http://www.dililitv.com/vplay/" + kk.xpath("./@id")[0], "fail")
                            return ""
                        video_re_com = re.compile(r'var vid = \'(.*?)\';', re.S)
                        video_link = video_re_com.search(response_video_link).group(1)
                        video_link = unquote(video_link)
                        print("video_link", video_link, "http://www.dililitv.com/vplay/" + kk.xpath("./@id")[0], items['name'], category_text)
                        if "http" not in video_link:
                            print("剧集为none ", items['name'])
                            pp+=1
                            if pp == 2:
                                break
                            continue
                        video_detail_link_quchong_list.append(video_link)
                        video_detail_link_list.append({"name": video_link_name, "url": video_link})
                        break
                    except Exception as e:
                        pp+=1
                        if pp==5:
                            break
                        continue


            if video_detail_link_quchong_list == []:
                return ""
            items['video_link_zip'] = video_detail_link_list
            if category_text == "电影":
                items['schedule_status'] = 1

            elif category_text == "剧集" or category_text == "动漫":
                if schedule_count == "":
                    items['schedule_status'] = 0
                else:
                    print(video_detail_link_list)
                    if video_detail_link_list == []:
                        items['schedule_status'] = 0
                    elif schedule_count in video_detail_link_list[-1].get("name"):
                        items['schedule_status'] = 1
                    else:
                        items['schedule_status'] = 0

            elif category_text == "综艺" or category_text == "纪录片" or category_text == "预告片":
                items['schedule_status'] = 0
            print(json.dumps(items))
            self.lists.append(items)
        except Exception   as e:
            print(traceback.print_exc())
            return ""

    def xpath_details_index(self,k):
        items = {}
        try:
            items['name'] = k.xpath("./a/h2/text()")[0]
            print(items['name'])
            items['link'] = k.xpath("./a/@href")[0]

            if k.xpath("./a/div/div[2]/span/text()") == []:
                items['schedule'] = ""
            else:
                items['schedule'] = k.xpath("./a/div/div[2]/span/text()")[0]

            tags = []
            tags_str = []
            for j in k.xpath("./div/span/a"):
                tags_href = j.xpath('./@href')[0]
                tags_text = j.xpath('./text()')[0]
                tags.append((tags_href, tags_text))
                tags_str.append(tags_text)
            if tags == []:
                print("tags None")
                items['tags'] = tags
                items['tags_str'] = tags_str
            else:
                items['tags'] = tags
                items['tags_str'] = tags_str

            response_video_detail = self.req.req_html(items['link'], parses="response_video_detail")
            if response_video_detail == "fail":
                print(items['link'], "fail")
                return ""
            html_detail = etree.HTML(response_video_detail)
            category_text_path = html_detail.xpath('/html/body/section/div/div/div/header/div/a[2]/text()')[0]
            print(category_text_path)
            category_text = category_text_path_dicts[category_text_path]
            items['video_type'] = category_text
            try:
                plot = html_detail.xpath('//p[@class="jianjie"]/span/text()')[0]
                items['plot'] = plot
            except:
                items['plot'] = ""
            video_detail = html_detail.xpath('//div[@class="video_info"]')[0]
            video_detail_re = html_detail.xpath('//div[@class="video_info"]')[0].xpath('string(.)')
            release_date = self.re_date(video_detail_re)
            items['release_date'] = release_date
            country_list = self.re_country(video_detail_re)
            items['country_list'] = country_list
            score = self.re_score(video_detail_re)
            items['score'] = score
            schedule_count = self.re_schedule(video_detail_re)
            items['schedule_count'] = schedule_count
            video_detail_content = etree.tostring(video_detail, method='html')
            items['video_detail_content'] = str(video_detail_content).replace("\\n", "")
            cover = html_detail.xpath("/html/body/section/div/div/div/article/div[1]/div[1]/img/@src")[0]
            items['cover'] = cover
            video_detail_link_list = []
            video_detail_link_quchong_list = []
            for kk in html_detail.xpath('//div[@class="vlink"]/a'):
                print("http://www.dililitv.com/vplay/" + kk.xpath("./@id")[0])
                video_link_name = kk.xpath("./text()")[0]
                pp = 0
                while True:
                    try:
                        response_video_link = self.req.req_html("http://www.dililitv.com/vplay/" + kk.xpath("./@id")[0],  parses="response_video_link")
                        if response_video_link == "fail":
                            print("http://www.dililitv.com/vplay/" + kk.xpath("./@id")[0], "fail")
                            return ""
                        video_re_com = re.compile(r'var vid = \'(.*?)\';', re.S)
                        video_link = video_re_com.search(response_video_link).group(1)
                        video_link = unquote(video_link)
                        print("video_link", video_link, "http://www.dililitv.com/vplay/" + kk.xpath("./@id")[0], items['name'], category_text)
                        if "http" not in video_link:
                            print("剧集为none ", items['name'])
                            pp+=1
                            if pp == 2:
                                break
                            continue
                        video_detail_link_quchong_list.append(video_link)
                        video_detail_link_list.append({"name": video_link_name, "url": video_link})
                        break
                    except Exception as e:
                        pp+=1
                        if pp==5:
                            break
                        continue


            if video_detail_link_quchong_list == []:
                return ""
            items['video_link_zip'] = video_detail_link_list
            if category_text == "电影":
                items['schedule_status'] = 1

            elif category_text == "剧集" or category_text == "动漫":
                if schedule_count == "":
                    items['schedule_status'] = 0
                else:
                    print(video_detail_link_list)
                    if video_detail_link_list == []:
                        items['schedule_status'] = 0
                    elif schedule_count in video_detail_link_list[-1].get("name"):
                        items['schedule_status'] = 1
                    else:
                        items['schedule_status'] = 0

            elif category_text == "综艺" or category_text == "纪录片" or category_text == "预告片":
                items['schedule_status'] = 0
            print(json.dumps(items))
            return items

        except Exception   as e:
            print(traceback.print_exc())
            return ""

    def xpath_banner(self, link):
        print("擦回国来哦哦 ", link)
        items = {}
        try:

            items['link'] = link


            response_video_detail = requests.get(items['link'], headers=self.headers, timeout=15).text
            html_detail = etree.HTML(response_video_detail)
            items['name'] = html_detail.xpath("//div[@class='breadcrumbs']/a/text()")[-1]
            print(items['name'])

            category_text_path = html_detail.xpath('/html/body/section/div/div/div/header/div/a[2]/text()')[0]
            print(category_text_path)
            category_text = category_text_path_dicts[category_text_path]
            items['video_type'] = category_text
            try:
                plot = html_detail.xpath('//p[@class="jianjie"]/span/text()')[0]
                items['plot'] = plot
            except:
                items['plot'] = ""
            video_detail = html_detail.xpath('//div[@class="video_info"]')[0]
            video_detail_re = html_detail.xpath('//div[@class="video_info"]')[0].xpath('string(.)')
            release_date = self.re_date(video_detail_re)
            items['release_date'] = release_date
            country_list = self.re_country(video_detail_re)
            items['country_list'] = country_list
            score = self.re_score(video_detail_re)
            items['score'] = score
            schedule_count = self.re_schedule(video_detail_re)
            items['schedule_count'] = schedule_count
            video_detail_content = etree.tostring(video_detail, method='html')
            items['video_detail_content'] = str(video_detail_content).replace("\\n", "")
            cover = html_detail.xpath("/html/body/section/div/div/div/article/div[1]/div[1]/img/@src")[0]
            items['cover'] = cover
            video_detail_link_list = []
            video_detail_link_quchong_list = []
            for kk in html_detail.xpath('//div[@class="vlink"]/a'):
                print("http://www.dililitv.com/vplay/" + kk.xpath("./@id")[0])
                video_link_name = kk.xpath("./text()")[0]

                response_video_link = requests.get("http://www.dililitv.com/vplay/" + kk.xpath("./@id")[0],
                                                   headers=self.headers, timeout=15).text
                video_re_com = re.compile(r'var vid = \'(.*?)\';', re.S)
                try:
                    video_link = video_re_com.search(response_video_link).group(1)
                    video_link = unquote(video_link)
                    if "http" not in video_link:
                        print("剧集为none ", items['name'])
                        continue
                    video_detail_link_quchong_list.append(video_link)
                    video_detail_link_list.append({"name": video_link_name, "url": video_link})
                except:
                    return ""
            if video_detail_link_quchong_list == []:
                return ""
            items['video_link_zip'] = video_detail_link_list
            if category_text == "电影":
                items['schedule_status'] = 1

            elif category_text == "剧集" or category_text == "动漫":
                if schedule_count == "":
                    items['schedule_status'] = 0
                else:
                    print(video_detail_link_list)
                    if schedule_count in video_detail_link_list[-1].get("name"):
                        items['schedule_status'] = 1
                    else:
                        items['schedule_status'] = 0

            elif category_text == "综艺" or category_text == "纪录片" or category_text == "预告片":
                items['schedule_status'] = 0

            items['schedule'] = ""

            tags = []
            tags_str = []
            tag_all = html_detail.xpath("//div[@class='article-tags']/a")
            if tag_all:
                for j in tag_all:
                    tags_href = j.xpath('./@href')[0]
                    tags_text = j.xpath('./text()')[0]
                    tags.append((tags_href, tags_text))
                    tags_str.append(tags_text)
            if tags == []:
                print("tags None")
                items['tags'] = tags
                items['tags_str'] = tags_str
            else:
                items['tags'] = tags
                items['tags_str'] = tags_str

            print(json.dumps(items))
            return items
        except Exception   as e:
            print(traceback.print_exc())
            return ""

    def req_link(self):

        response = self.req.req_html("http://www.dililitv.com/", parses="shouye")
        # print(response)
        html = etree.HTML(response)
        link_all = html.xpath('//ul[@class="nav"]/li')
        # print(link_all)
        for i in link_all[1:]:

            category_text = (i.xpath("./a/text()"))[0]
            # print(category_text)
            if category_text == "首页" or category_text == "最新" or category_text == "纪录片" or category_text == "电影" or category_text == "综艺" or category_text == "动漫":
                continue

            category_link = i.xpath("./a/@href")[0]
            print(category_link)
            response_category = self.req.req_html(category_link, parses="response_category")
            category_html = etree.HTML(response_category)
            page_count = category_html.xpath("//div[@class='pagination pagination-multi']/ul/li")[-1]
            print(page_count)
            count_page = (self.re_category_count(page_count.xpath("./span/text()")[0]))
            for jjj in range(1, 10):
                print("{}/page/{}".format(category_link, jjj))
                pp = 0
                while True:
                    try:
                        response_category = self.req.req_html("{}/page/{}".format(category_link, jjj), parses="category_index")
                        if response_category == "fail":
                            print("{}/page/{}".format(category_link, jjj), "fail")
                            break
                        # print("response_category", response_category)
                        html = etree.HTML(response_category)
                        # print(category_html)
                    # data_video_type = category_html.xpath("//div[@class='list-content']/h3/strong/a/text()")[0]
                    # print(data_video_type)
                        video_all = html.xpath("//article[@class='u-movie']")
                        print(video_all)
                        for k in video_all:
                            print(k)
                            self.q.put(k)
                        break
                    except Exception as e:
                        time.sleep(1)
                        pp += 1
                        if pp == 5:
                            break
                    #     continue
               # items = self.xpath_details(k, category_text)
               # if items != "":
               #     self.lists.append(items)
                    # break
        # print(self.lists)
        # self.logic.importdata(self.lists)

    def req_index(self, response):
        response = requests.get("http://www.dililitv.com/", headers=self.headers, timeout=15).text
        html = etree.HTML(response)
        all = html.xpath('//div[@class="m-movies clearfix"]')
        self.index_list = []
        for i in all:
            '''
            | ./h1/strong/a/text()
            | ./article[@class="u-movie"]
            '''
            list_nav = i.xpath('./div/div/ul/li/text()')
            # print(list_nav[0])
            article_div = i.xpath('./div/div[@class="posts_new_list"]  | ./div/div[@class="update_list_con"]')
            # print(article_div)
            if list_nav != []:
                for k,v in zip(list_nav, article_div):

                    print(k,v)
                    video_all = v.xpath("./article")

                    for v_detail in video_all:
                        print(v_detail)
                        result = self.xpath_details_index(v_detail, )
                        print(result)
                        if result == "":
                            continue
                        result['group_name'] = k
                        print(result)
                        self.index_list.append(result)


        print(self.index_list)
        # self.index_logic.importdata_list(self.index_list)

    def req_banner(self, response):
        self.banner_list = []
        response = requests.get("http://www.dililitv.com/", headers=self.headers, timeout=15).text
        html = etree.HTML(response)
        all_banner = html.xpath("//div[@class='carousel-inner']/div")
        for i in all_banner:
            img = i.xpath('./a/img/@src')
            href  =i.xpath('./a/@href')
            # print(img, href)
            for imgs, hrefs in zip(img, href):
                # print(hrefs)
                if "http" in hrefs:
                    href = hrefs

                else:
                    href = "http://www.dililitv.com" + hrefs
                print(href)
                results = self.xpath_banner(href)
                if results != "":
                    if "http" not in imgs:
                        imgs = "http:" + imgs
                    results['img'] = imgs
                    self.banner_list.append(results)
        print(self.banner_list)
        self.banner_logic.Dilili_banner(self.banner_list)

    def requests_detail(self, article_xpath):
        try:
            self.xpath_details(article_xpath)
                # title = html.xpath('/html/body/div[1]/div[2]/div[1]/div/div[2]/h2/a/text()')
                # print(title)

            while True:
                if self.q.empty() == False:
                    print("线程再次进入取url函数")
                    print("队列长度", self.q.qsize())
                    result = self.q.get()

                    self.requests_detail(result)
                    # break

                elif self.q.empty() == True and self.list_end == False:
                    print("线程结束")

                    self.repeat_item_num += 1
                    if self.repeat_item_num == 30:
                        print("所有线程结束")
                        with open('./b.txt', 'w+') as f:
                            f.write(json.dumps(self.lists))
                        print(len(self.lists))
                        self.logic.importdata(self.lists)
                        print(int(time.time()) - starttime)
                    exit(0)

                elif self.q.empty() == True and self.list_end == True:
                    print("正在等待")
                    time.sleep(2)
        except requests.exceptions.ConnectionError:
            print("url 链接异常 ",)
            self.repeat_item_num += 1

        except requests.exceptions.Timeout:
            print("url 等待超时 ",)
            self.repeat_item_num += 1


    def get_detail_url(self):
        time.sleep(5)
        print("程序启动")
        while True:
            print("目前的q {} {}".format(self.q.empty(), self.list_end))
            n = 0

            while not self.q.empty() and self.list_end == False:
                print("队列长度", self.q.qsize())
                result = self.q.get()
                t = threading.Thread(target=self.requests_detail, args=(result,))
                t.start()

                n += 1
                if n == 30:
                    return

            if self.q.empty() == True and self.list_end == False:
                print("进入结束页面")
                return "1"

            elif self.list_end == True:
                print("进入线程等待页面 主线程没有跑完")
                time.sleep(10)

            elif self.q.empty() == True and self.list_end == True:
                print("正在等待")
                time.sleep(2)

    def start(self):
        t1 = threading.Thread(target=self.get_detail_url)
        t1.start()


        q.req_link()
        self.list_end = False

if __name__ == '__main__':
    # sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')
    # html = "\x81\x97\x8b6\x94\n\xf0\x14\xe4e\xe5Q\xb60\xab\x07\xa13\xbe\x0e\xa7<\xbc\x07\xa4"

    starttime = int(time.time())
    q = Dll()
    q.start()
    # q.req_index('1')
    # q.req_banner('1')
    # d = ['2020']
    # print(d[0][:4])
    '''
    https://api1.dating.com/users/693210731/photos/eb349a9c15f1fa72.x400.scaleup.x1200.gallery
    https://api3.dating.com/users/693210731/photos/52ec7da3967852eb.x400.scaleup.x1200.gallery

    '''