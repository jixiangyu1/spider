#encoding:utf8

import pymysql
from Utils import getCurFullDay
import json

class Dilili_logic():
    def __init__(self):
        self.db = pymysql.connect(
            host="123.57.33.212",
            port=3306,
            user="root",
            passwd="cy2018xyz",
            db="video_data",
            charset="utf8mb4"
        )
        self.cursor = self.db.cursor(cursor=pymysql.cursors.DictCursor)

    def select_update_name(self, websource):
        sql = "SELECT `name` FROM `data_video` WHERE `schedule_status` = 0 AND `web_source` = '%s'"%(websource)
        print(sql)
        self.cursor.execute(sql)
        res = self.cursor.fetchall()
        if res:
            return res
        else:
            return {}

    def select_name(self, table_name, name_value, flag=0):
        # print("chaunjinlai de ", name_value)
        # print(flag)
        if flag == 1:
            sql = "SELECT `id` FROM `%s` WHERE `video_id` = %s" % (table_name, name_value)
        else:
            sql = "SELECT `id` FROM `%s` WHERE `name` = '%s'" % (table_name, name_value)
        print(sql)
        self.cursor.execute(sql)
        res = self.cursor.fetchone()
        if res:
             return res
        else:
            return {}

    def select_tag_regin_name(self, table_name, name_value, category_id):
        # print("chaunjinlai de ", name_value)
        # print(flag)

        sql = "SELECT `id` FROM `%s` WHERE `name` = '%s' AND `category_id` = %s" % (table_name, name_value, category_id)
        print(sql)
        self.cursor.execute(sql)
        res = self.cursor.fetchone()
        if res:
             return res
        else:
            return {}

    def importdata(self, row):

        for i in row:
            data_category_name = i.get("video_type")
            data_video_name = str(i.get("name")).strip()
            schedule = i.get("schedule")
            cover = i.get('cover')
            tags_str = i.get('tags_str')
            score = i.get('score')
            release_date = i.get('release_date')
            plot = i.get('plot')
            country_list = i.get('country_list')
            schedule_status = i.get('schedule_status')
            print("schedule_status", schedule_status)

            #查询data_category表name是否存在 存在返回id 否则插入进入返回id
            data_category_name_exsit = self.select_name("data_category",  data_category_name)
            if data_category_name_exsit:
                data_category_id = data_category_name_exsit["id"]
                print(data_category_id)
            else:
                data_category_id = self.insert_data_category(data_category_name)

            #查询影片名称是否重复
            data_video_name_exsit = self.select_id("data_video", data_video_name)
            print("data_video_name_exsit", data_video_name_exsit)
            if data_video_name_exsit == "success":
                print("影片存在")
                continue
                # break
            else:
                print(data_category_name)
                #html化影片内容
                video_detail_content = pymysql.escape_string(str(i.get('video_detail_content', '')))

                #插入影片 data_video
                if data_video_name_exsit == {}:
                    if tags_str == []:
                        tags_str = ""
                    else:
                        tags_str = ",".join(tags_str)
                    sql = "INSERT INTO `data_video`( `name`, `cover`, `schedule_status`, `schedule`,`tags`,  `status`, `score`,  `release_date`, `create_time`,`description`,`plot`, `web_source`) VALUES ('%s', '%s', %s, '%s', '%s', %s, %s, %s,'%s', '%s', '%s', '%s');" % (data_video_name, cover, schedule_status, schedule, tags_str, 1, float(score), int(release_date), getCurFullDay(),  video_detail_content, plot, "dilili")

                    print(sql)
                    self.cursor.execute(sql)
                    video_id = self.cursor.lastrowid

                    # 插入address
                    video_link_zip = i.get('video_link_zip')

                    video_address_name_exsits = self.select_name("data_video_address", video_id, flag=1)
                    print("video_address_name_exsits {}".format(video_address_name_exsits))
                    if video_address_name_exsits:
                        updates_flag = 1
                    else:
                        updates_flag = 0
                    for indexs, k in enumerate(video_link_zip):
                        # print(json.dumps(video_link_zip))
                        self.data_video_address(video_id, k.get('name'), k.get("url"), indexs, updates_flag)

                    self.data_video_category(video_id, data_category_id)

                    # # 查询data_region表name是否存在 存在返回id 否则插入进入返回id
                    if country_list == []:
                        country_list = ["无"]
                    for country_name in country_list:
                        data_region_name_exsit = self.select_tag_regin_name("data_region", str(country_name).strip(), data_category_id)
                        if data_region_name_exsit:
                            data_region_id = data_region_name_exsit["id"]

                        else:
                            data_region_id = self.insert_data_region(str(country_name).strip(), data_category_id)

                        print(data_region_id)

                        self.insert_data_video_region(video_id, data_region_id)

                    tags = i.get('tags')
                    if tags == []:
                        tags = [["无","无"]]

                    for tags_lists in tags:
                        print(tags_lists)
                        data_tags_name_exsit = self.select_tag_regin_name("data_tags", tags_lists[1], data_category_id)
                        if data_tags_name_exsit:
                            data_tags_id = data_tags_name_exsit["id"]

                        else:
                            data_tags_id = self.insert_tags(tags_lists[1], data_category_id)

                        self.insert_video_tags(video_id, data_tags_id)


                else:
                    print("只需要更新 {}".format(data_video_name))
                    video_id = data_video_name_exsit

                    self.updates_schedule(schedule, schedule_status, video_id, data_video_name)
                    # 插入address
                    video_link_zip = i.get('video_link_zip')

                    video_address_name_exsits = self.select_name("data_video_address", video_id, flag=1)
                    print("video_address_name_exsits {}".format(video_address_name_exsits))
                    if video_address_name_exsits:
                        updates_flag = 1
                    else:
                        updates_flag = 0
                    for index, k in enumerate(video_link_zip):
                        # print(json.dumps(video_link_zip))
                        self.data_video_address(video_id, k.get('name'), k.get("url"), index, updates_flag)

                print("存入影片")

                self.db.commit()

            # break

    #插入标签name表
    def insert_tags(self, tag, category_id):
        sql = "INSERT INTO `data_tags`( `name`, `category_id`,  `create_time`) VALUES ( '%s', %s, '%s');"%(tag, category_id, getCurFullDay())
        print(sql)
        self.cursor.execute(sql)
        tag_id = self.cursor.lastrowid
        return tag_id

    #更新schedule状态
    def updates_schedule(self, schedule, schedule_status, video_id, name):
        sql = "UPDATE data_video SET `schedule` = '{}', `schedule_status` = {} WHERE `id` = {} AND `name` = '{}'".format(schedule, schedule_status, video_id, name)
        print(sql)
        self.cursor.execute(sql)


    #插入 data_category 分类名称
    def insert_data_category(self, name):
        sql = "INSERT INTO `data_category`(`name`, `description`, `sort`, `level`, `pid`, `create_time`, `status`) VALUES ('%s', NULL, NULL, NULL, NULL, '%s', 1);" % (
            name, getCurFullDay())
        self.cursor.execute(sql)
        data_category_id = self.cursor.lastrowid
        return data_category_id

    #插入data_region 国家地区表
    def insert_data_region(self, name, category_id):
        sql = "INSERT INTO `data_region`(`name`, `category_id`, `create_time`) VALUES ('%s', %s, '%s');" % (name, category_id, getCurFullDay())
        self.cursor.execute(sql)
        data_region_id = self.cursor.lastrowid
        return data_region_id

    #插入data_video_region 国家地区 影片id
    def insert_data_video_region(self, video_id, region_id):
        sql = "INSERT INTO `data_video_region`(`video_id`,`region_id`, `create_time`) VALUES (%s, '%s', '%s');"%(video_id,region_id, getCurFullDay())
        print(sql)
        self.cursor.execute(sql)

    #插入data_video_tags videoid 与 tag_id
    def insert_video_tags(self, video_id, tags_id):
        sql = "INSERT INTO `data_video_tags`(`video_id`,`tags_id`, `create_time`) VALUES (%s, %s, '%s');"%(video_id, tags_id, getCurFullDay())
        print(sql)
        self.cursor.execute(sql)


    #插入 data_video_category videoid 与 tag_id
    def data_video_category(self, video_id, category_id):
        sql = "INSERT INTO `data_video_category`(`video_id`, `category_id`, `create_time`) VALUES (%s, %s, '%s');" % (video_id, category_id, getCurFullDay())
        print(sql)
        self.cursor.execute(sql)

    #插入adress表
    def data_video_address(self, video_id, name, url, indexs, updates_flag):
        if updates_flag == 1:
            sql = "SELECT `id` FROM `data_video_address` WHERE `video_id` = %s AND `name` = '%s' AND `source` = 0" % (video_id, name)
            print(sql)
            self.cursor.execute(sql)
            res = self.cursor.fetchone()
            print("addreass",res )
            if res:
                sql = "UPDATE data_video_address SET `name` = '{}', `url` = '{}', `sort` = {} WHERE video_id = {} AND `name` = '{}' AND `source` = 0".format(name, url, indexs, video_id, name)
                print(sql)
                self.cursor.execute(sql)
            else:
                sql = "INSERT INTO `data_video_address`(`video_id`, `name`, `url`, `sort`, `create_time`,`play_count`, `source`, `status`) VALUES (%s, '%s', '%s', %s, '%s', 0, '%s',1);" % (video_id, name, url, indexs, getCurFullDay(), 0)
                print(sql)
                self.cursor.execute(sql)
        else:
            sql = "INSERT INTO `data_video_address`(`video_id`, `name`, `url`, `sort`, `create_time`,`play_count`, `source`, `status`) VALUES (%s, '%s', '%s', %s, '%s', 0, '%s',1);" % (video_id, name, url, indexs, getCurFullDay(), 0)
            print(sql)
            self.cursor.execute(sql)
        # self.db.commit()

    def select_id(self, table_name, name):
        sql = "SELECT `id`, `schedule_status`, `web_source` FROM `%s` WHERE `name` = '%s'" % (table_name, name)
        self.cursor.execute(sql)
        res = self.cursor.fetchone()
        if res:
            if res.get('schedule_status') == 1 and res.get('web_source') == "dilili":
                return "success"
            return res.get('id')

        else:
            return {}




class Dilili_index_logic(Dilili_logic):
    def __init__(self):
        Dilili_logic.__init__(self)

    def select_groups(self, table_name, name_value, group_id):
        # print("chaunjinlai de ", name_value)
        # print(flag)

        sql = "SELECT `id` FROM `%s` WHERE `video_id` = %s AND `group_id` = %s" % (table_name, name_value, group_id)
        print(sql)
        self.cursor.execute(sql)
        res = self.cursor.fetchone()
        if res:
            return res
        else:
            return {}


    # 插入 data_category 分类名称
    def insert_data_group(self, name):
        sql = "INSERT INTO `data_group`(`name`,  `create_time`) VALUES ('%s', '%s');" % (name, getCurFullDay())
        print(sql)
        self.cursor.execute(sql)

        data_category_id = self.cursor.lastrowid
        return data_category_id

    def insert_data_video_group(self, video_id, group_id):
        sql = "INSERT INTO `data_video_group`(`video_id`,  `group_id`, `create_time`) VALUES (%s, %s, '%s');" % (video_id, group_id, getCurFullDay())
        print(sql)
        self.cursor.execute(sql)
        data_category_id = self.cursor.lastrowid
        return data_category_id

    def importdata_list(self, row):
        for i in row:
            print(i)
            data_video_name = str(i.get('name')).strip()
            data_group_name_exsit = Dilili_logic.select_name(self, 'data_group', i.get('group_name'))
            print(data_group_name_exsit)
            if data_group_name_exsit:
                data_group_id = data_group_name_exsit['id']
            else:
                print("sag")
                data_group_id = self.insert_data_group(i.get('group_name'))
            print(data_group_id)
            data_video_id_exsit = Dilili_logic.select_name(self, 'data_video',data_video_name)
            print(data_video_id_exsit)
            if data_video_id_exsit:
                data_video_id = data_video_id_exsit['id']
            else:
                Dilili_logic.importdata(self, [i])
                select_data_video_id = Dilili_logic.select_name(self, 'data_video', data_video_name)
                data_video_id = select_data_video_id['id']
            select_data_group_video_id = self.select_groups('data_video_group', data_video_id, data_group_id)
            if select_data_group_video_id:
                print("data_video_id cunzau", data_video_id)
                pass

            else:
                group_video_id = self.insert_data_video_group(data_video_id, data_group_id)
                if group_video_id:
                    print("插入车工")
            self.db.commit()

class Dilili_banner(Dilili_logic):
    def __init__(self):
        Dilili_logic.__init__(self)

    def insert_data_banner(self, video_id, title, img):
        sql = "INSERT INTO `data_banner`(`video_id`,  `title`, `img`, `create_time`) VALUES (%s, '%s', '%s', '%s');" % (video_id, title, img, getCurFullDay())
        print(sql)
        self.cursor.execute(sql)
        data_bannner_id = self.cursor.lastrowid
        return data_bannner_id


    def Dilili_banner(self, row):
        if row != []:
            sql = "truncate table `data_banner`;"
            self.cursor.execute(sql)
            for i in row:
                data_video_name = str(i.get('name')).strip()
                data_img = i.get('img')
                data_video_name_exsit = Dilili_logic.select_name(self, 'data_video', data_video_name)
                if data_video_name_exsit:
                    data_video_id = data_video_name_exsit['id']
                else:
                    Dilili_logic.importdata(self, [i])
                    select_data_video_id = Dilili_logic.select_name(self, 'data_video', data_video_name)
                    data_video_id = select_data_video_id['id']
                data_bannner_id = self.insert_data_banner(data_video_id, data_video_name, data_img)
                if data_bannner_id:
                    print("插入车工")
            self.db.commit()

# with open("./b.txt", "r", encoding="utf8") as f:
#     dictss = (f.read())
#     jsondata = eval(dictss)
#     p = Dilili_index_logic()
#     p.importdata_list(jsondata)