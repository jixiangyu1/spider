#encoding:utf8
import json
import re
import requests
from lxml import etree
from urllib.parse import unquote,quote
from Config import category_text_path_dicts
import hashlib
import time
from dilili.request_class import ReqCls
from dilili.film_logic import Dilili_logic, Dilili_index_logic, Dilili_banner
import traceback
import threading
import queue
orderno = "ZF2020411942R36FBe"
secret = "ca9153d6fce24c98affba07bcf819e3e"
ip_forward = "forward.xdaili.cn"

def auth_proxy():
    timestamp = str(int(time.time()))
    string = ""
    string = "orderno=" + orderno + "," + "secret=" + secret + "," + "timestamp=" + timestamp

    string = string.encode()

    md5_string = hashlib.md5(string).hexdigest()
    sign = md5_string.upper()
    # print(sign)
    auth = "sign=" + sign + "&" + "orderno=" + orderno + "&" + "timestamp=" + timestamp

    return auth


port = "80"
ip_port = ip_forward + ":" + port
privoxy = {"http": "http://" + ip_port, "https": "https://" + ip_port}

class Dll():
    def __init__(self):
        self.req = ReqCls()
        self.lists = []
        self.logic = Dilili_logic()
        self.index_logic = Dilili_index_logic()
        self.banner_logic = Dilili_banner()
        self.q = queue.Queue(maxsize=5000)
        self.list_end = True
        self.repeat_item_num = 0
        self.headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36",
            # "Proxy-Authorization": auth_proxy()

        }
    def re_date(self, responses):
        try:
            reqs = re.compile("上映日期: (.*?)-", re.S)
            date = (reqs.findall(responses))
            if date == []:
                reqs = re.compile("首播: (.*?)-", re.S)
                date = (reqs.findall(responses))
            return date[0][:4]
        except:
            return 2000

    def re_country(self, responses):
        try:
            country_list = []
            req_country = re.compile("国家/地区: (.*?)\\n", re.S)
            country = (req_country.findall(responses))[0]

            if "/" in country:
                country_list = country.split("/")
                return country_list
            country_list.append(country)
            return country_list
        except:
            return ""

    def re_score(self, responses):
        try:
            req_score = re.compile("评分: (.*?)\\n", re.S)
            score = (req_score.findall(responses))[0]
            if score == "暂无":
                return 0
            return score
        except:
            return 0

    def re_schedule(self, responses):
        try:
            req_score = re.compile("集数: (.*?)\\n", re.S)
            schedule_count = (req_score.findall(responses))[0]
            return schedule_count
        except:
            return ""

    def re_category_count(self, response):
        print("sdg", response)
        try:
            req_score = re.compile("共 (.*?) 页", re.S)
            schedule_count = (req_score.findall(response))[0]
            return schedule_count
        except:
            return ""

    def xpath_details(self,k):
        items = {}
        try:
            items['name'] = k.xpath("./a/h2/text()")[0]
            print(items['name'])
            items['link'] = k.xpath("./a/@href")[0]

            if k.xpath("./a/div/div[2]/span/text()") == []:
                items['schedule'] = ""
            else:
                items['schedule'] = k.xpath("./a/div/div[2]/span/text()")[0]

            tags = []
            tags_str = []
            for j in k.xpath("./div/span/a"):
                tags_href = j.xpath('./@href')[0]
                tags_text = j.xpath('./text()')[0]
                tags.append((tags_href, tags_text))
                tags_str.append(tags_text)
            if tags == []:
                print("tags None")
                items['tags'] = tags
                items['tags_str'] = tags_str
            else:
                items['tags'] = tags
                items['tags_str'] = tags_str

            response_video_detail = self.req.req_html(items['link'], parses="response_video_detail")
            if response_video_detail == "fail":
                print(items['link'], "fail")
                return ""
            html_detail = etree.HTML(response_video_detail)
            category_text_path = html_detail.xpath('/html/body/section/div/div/div/header/div/a[2]/text()')[0]
            print(category_text_path)
            category_text = category_text_path_dicts[category_text_path]
            items['video_type'] = category_text
            try:
                plot = html_detail.xpath('//p[@class="jianjie"]/span/text()')[0]
                items['plot'] = plot
            except:
                items['plot'] = ""
            video_detail = html_detail.xpath('//div[@class="video_info"]')[0]
            video_detail_re = html_detail.xpath('//div[@class="video_info"]')[0].xpath('string(.)')
            release_date = self.re_date(video_detail_re)
            items['release_date'] = release_date
            country_list = self.re_country(video_detail_re)
            items['country_list'] = country_list
            score = self.re_score(video_detail_re)
            items['score'] = score
            schedule_count = self.re_schedule(video_detail_re)
            items['schedule_count'] = schedule_count
            video_detail_content = etree.tostring(video_detail, method='html')
            items['video_detail_content'] = str(video_detail_content).replace("\\n", "")
            cover = html_detail.xpath("/html/body/section/div/div/div/article/div[1]/div[1]/img/@src")[0]
            items['cover'] = cover
            video_detail_link_list = []
            video_detail_link_quchong_list = []
            for kk in html_detail.xpath('//div[@class="vlink"]/a'):
                print("http://www.dililitv.com/vplay/" + kk.xpath("./@id")[0])
                video_link_name = kk.xpath("./text()")[0]
                pp = 0
                while True:
                    try:
                        response_video_link = self.req.req_html("http://www.dililitv.com/vplay/" + kk.xpath("./@id")[0],  parses="response_video_link")
                        if response_video_link == "fail":
                            print("http://www.dililitv.com/vplay/" + kk.xpath("./@id")[0], "fail")
                            return ""
                        video_re_com = re.compile(r'var vid = \'(.*?)\';', re.S)
                        video_link = video_re_com.search(response_video_link).group(1)
                        video_link = unquote(video_link)
                        print("video_link", video_link, "http://www.dililitv.com/vplay/" + kk.xpath("./@id")[0], items['name'], category_text)
                        if "http" not in video_link:
                            print("剧集为none ", items['name'])
                            pp+=1
                            if pp == 2:
                                break
                            continue
                        video_detail_link_quchong_list.append(video_link)
                        video_detail_link_list.append({"name": video_link_name, "url": video_link})
                        break
                    except Exception as e:
                        pp+=1
                        if pp==5:
                            break
                        continue


            if video_detail_link_quchong_list == []:
                return ""
            items['video_link_zip'] = video_detail_link_list
            if category_text == "电影":
                items['schedule_status'] = 1

            elif category_text == "剧集" or category_text == "动漫":
                if schedule_count == "":
                    items['schedule_status'] = 0
                else:
                    print(video_detail_link_list)
                    if video_detail_link_list == []:
                        items['schedule_status'] = 0
                    elif schedule_count in video_detail_link_list[-1].get("name"):
                        items['schedule_status'] = 1
                    else:
                        items['schedule_status'] = 0

            elif category_text == "综艺" or category_text == "纪录片" or category_text == "预告片":
                items['schedule_status'] = 0
            print(json.dumps(items))
            self.lists.append(items)
        except Exception   as e:
            print(traceback.print_exc())
            return ""

    def two_respon(self, keyword):

        try:
            response = self.req.req_html("http://www.dililitv.com/?s=++{}".format(quote(keyword)), parses="category_index")
            # print(response)
            html = etree.HTML(response)
            video_all = html.xpath("//div[@class='m-movies clearfix']/article")
            # print("video_all", video_all)
            self.xpath_details(video_all[0])
        except:
            print("次哦催哦", traceback.print_exc())
            return ""


    def req_link(self):
        updates_name = (self.logic.select_update_name("dilili"))
        # for i in updates_name:
        #     print(i)
        for i in updates_name[150:]:
            keyword = i.get('name')
            if "海贼王" not in keyword:

                self.q.put(keyword)

        # print(quote(keyword))

            # self.q.put(video_all)
        # for k in video_all[:1]:
        #     print(k)
        #


    def requests_detail(self, article_xpath):
        try:
            self.two_respon(article_xpath)
                # title = html.xpath('/html/body/div[1]/div[2]/div[1]/div/div[2]/h2/a/text()')
                # print(title)

            while True:
                if self.q.empty() == False:
                    print("线程再次进入取url函数")
                    print("队列长度", self.q.qsize())
                    result = self.q.get()

                    self.requests_detail(result)
                    # break

                elif self.q.empty() == True and self.list_end == False:
                    print("线程结束 self.repeat_item_num {}".format(self.repeat_item_num))

                    self.repeat_item_num += 1
                    if self.repeat_item_num == 30:
                        print("所有线程结束")
                        with open('./b.txt', 'w+') as f:
                            f.write(json.dumps(self.lists))
                        print(len(self.lists))
                        self.logic.importdata(self.lists)
                        print(int(time.time()) - starttime)
                    exit(0)

                elif self.q.empty() == True and self.list_end == True:
                    print("正在等待")
                    time.sleep(2)
        except requests.exceptions.ConnectionError:
            print("url 链接异常 ",)
            self.repeat_item_num += 1

        except requests.exceptions.Timeout:
            print("url 等待超时 ",)
            self.repeat_item_num += 1


    def get_detail_url(self):
        time.sleep(5)
        print("程序启动")
        while True:
            print("目前的q {} {}".format(self.q.empty(), self.list_end))
            n = 0

            while not self.q.empty() and self.list_end == False:
                print("队列长度", self.q.qsize())
                result = self.q.get()
                t = threading.Thread(target=self.requests_detail, args=(result,))
                t.start()

                n += 1
                if n == 30:
                    return

            if self.q.empty() == True and self.list_end == False:
                print("进入结束页面")
                return "1"

            elif self.list_end == True:
                print("进入线程等待页面 主线程没有跑完")
                time.sleep(10)

            elif self.q.empty() == True and self.list_end == True:
                print("正在等待")
                time.sleep(2)

    def start(self):
        t1 = threading.Thread(target=self.get_detail_url)
        t1.start()


        q.req_link()
        self.list_end = False

if __name__ == '__main__':
    starttime = int(time.time())
    q = Dll()
    q.start()

