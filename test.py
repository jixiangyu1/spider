#encoding:utf8
import json

import requests
from lxml import etree

headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36",
            # "Proxy-Authorization": auth_proxy()

        }
response = requests.get("http://www.dililitv.com/gresource/7887",headers=headers).text
html_detail = etree.HTML(response)
video_detail = html_detail.xpath('//div[@class="video_info"]')[0].xpath('string(.)')
print(video_detail)
import re

reqs = re.compile("上映日期: (.*?)-", re.S)
req_country = re.compile("国家/地区: (.*?)\\n", re.S)
date = (reqs.findall(video_detail))
if date == []:
    reqs = re.compile("首播: (.*?)-", re.S)
    date = (reqs.findall(video_detail))
country = (req_country.findall(video_detail))[0]
print(date)
print(country)
req_score = re.compile("评分: (.*?)\\n", re.S)
score = (req_score.findall(video_detail))[0]
print(score)
# if "/" in country:
#     print(country.split("/"))
print(json.dumps({'video_type': '电影', 'name': '十二夜', 'link': 'http://www.dililitv.com/gresource/7920', 'schedule': '', 'tags': [('http://www.dililitv.com/tag/juqing', '剧情'), ('http://www.dililitv.com/tag/aiqing', '爱情')], 'tags_str': '剧情爱情', 'release_date': ['2000'], 'country_list': ['中国香港'], 'score': '8.2', 'video_detail_content': 'b\'<div class="video_info"><strong>&#23548;&#28436;:</strong>  &#26519;&#29233;&#21326;<br><strong>&#32534;&#21095;:</strong> &#26519;&#29233;&#21326;<br><strong>&#20027;&#28436;:</strong> &#24352;&#26575;&#33437; / &#38472;&#22869;&#36805; / &#21346;&#24039;&#38899; / &#24352;&#29130;&#24742; / &#21306;&#28113;&#36126; / &#21331;&#38901;&#33437; / &#35874;&#38662;&#38155; / &#20911;&#24503;&#20262; / &#37073;&#20013;&#22522;<br><strong>&#31867;&#22411;:</strong> &#21095;&#24773; / &#29233;&#24773;<br><strong>&#22269;&#23478;/&#22320;&#21306;:</strong> &#20013;&#22269;&#39321;&#28207;<br><strong>&#35821;&#35328;:</strong> &#31908;&#35821;<br><strong>&#19978;&#26144;&#26085;&#26399;:</strong> 2000-04-20(&#20013;&#22269;&#39321;&#28207;)<br><strong>&#29255;&#38271;:</strong> 92&#20998;&#38047;<br><strong>&#21448;&#21517;:</strong> 12&#22812; / Twelve Nights<br><strong>IMDb&#32534;&#30721;:</strong> tt0282159<br><strong>&#35780;&#20998;:</strong> 8.2</div>\'', 'cover': 'https://pic.dlili.tv/upload/poster/945ef7c80928aaeb.jpg', 'video_link_zip': [[{'name': 'ZD高清', 'url': 'https://yi.jingdianzuida.com/20190928/Gs0XlDOy/index.m3u8'}], [{'name': 'UU高清', 'url': 'https://zy.512wx.com/20171204/Z0MjYD2y/index.m3u8'}], [{'name': 'YY高清', 'url': 'https://s1.135-cdn.com/20191124/mutFRN4DV8cQZYOa/index.m3u8'}]]}
) )
a = ["剧情", "剧情"]
print(",".join(a))