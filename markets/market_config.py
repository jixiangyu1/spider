#encoding:utf8

config_dict = {
    5: "futures_five_min",
    15: "futures_fifteen_min",
    30: "futures_thirty_min",
    60: "futures_hour",
    1000: "futures_day",
}