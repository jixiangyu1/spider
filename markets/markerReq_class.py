#encoding:utf8
import hashlib
import time

import requests


# class req_market():
#     def __init__(self):
#         self.orderno = "ZF2020411942R36FBe"
#         self.secret = "ca9153d6fce24c98affba07bcf819e3e"
#         self.proxy  = {
#                 "http": "http://{}".format("forward.xdaili.cn:80"),
#                 "https": "https://{}".format("forward.xdaili.cn:80"),
#             }
#         self.headers = {
#             "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36",
#             "Proxy-Authorization": self.get_auth()
#         }
#
#
#     def get_auth(self):
#         timestamp = str(int(time.time()))
#         string = "orderno=" + self.orderno + "," + "secret=" + self.secret + "," + "timestamp=" + timestamp
#         string = string.encode()
#         md5_string = hashlib.md5(string).hexdigest()  # 计算sign
#         sign = md5_string.upper()  # 转换成大写
#         auth = "sign=" + sign + "&" + "orderno=" + self.orderno + "&" + "timestamp=" + timestamp
#         return auth
#
#     def req_url(self, url):
#         except_num = 0
#         while True:
#             try:
#                 response = requests.get(url, headers=self.headers, proxies=self.proxy, verify=False, timeout=15)
#                 if response.status_code == 200:
#                     return response
#             except:
#                 print("请求url 出现错误 {}".format(url))
#                 except_num += 1
#                 if except_num == 5:
#                     return "fail"
#                 continue

#encoding:utf8
import json

import requests
import time
import queue
from lxml import etree
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

class getProxy():
    def __init__(self):
        # self.redis_con = RedisConnect()
        self.api = "http://49.233.75.211:5000/proxy?count=30"
        self.queue = queue.Queue()
        # self.logger = logger("getPorxy.log")
        self.flag = True

    def Get_NewProxy(self, times=0):

        """从代理池中获取代理
        """
        if times >= 6:
            return

        try:
            proxy = requests.get(self.api, timeout=25).text
            proxy_list = eval(proxy)
            proxies_list = list()
            if type(proxy_list) is dict:
                time.sleep(10)
                times += 1
                self.Get_NewProxy()
            if proxy_list[0]["ip"] is None or proxy_list[0]["ip"] == "None":
                time.sleep(30)
                times += 1
                self.Get_NewProxy()
            else:
                for proxy_msg in proxy_list:
                    proxies_list.append(proxy_msg)
                    self.queue.put(proxy_msg)
                self.flag = True
            if self.queue.empty():
                times += 1
                self.Get_NewProxy()
        except Exception as e:
            # self.logger.info("请求接口失败 {}".format(e))
            time.sleep(5)
            self.Get_NewProxy()

    def retProxy(self):
        if self.queue.empty():
            if self.flag == True:
                self.flag = False
                self.Get_NewProxy()
            return "fail"

        else:
            return self.queue.get()

getProxy = getProxy()
class req_market():
    def __init__(self):
        # self.logger = logger("{}.log".format(log_name))
        self.s = requests.session()
        self.headers = {
                    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36",
                }

    def req_url(self, url):
        print("====")
        without_title = 0

        except_flag = 0
        while True:
            try:
                proxies = getProxy.retProxy()
                print(proxies)

                if proxies != "fail":
                    if str(url).split(':')[0] == "http":
                        privoxy = {"http": "http://{}".format(proxies.get("ip")), "https": "https://{}".format(proxies.get("ip"))}
                    else:
                        privoxy = {"http": "http://{}".format(proxies.get("ip")), "https": "https://{}".format(proxies.get("ip"))}


                    response = requests.get(url, headers=self.headers, proxies=privoxy, verify=False, timeout=15)
                    if response.status_code == 200:
                        return response
                    else:
                        without_title += 1
                        if without_title == 10:
                            return "fail"
                        continue

            except Exception as e:
                except_flag += 1
                if except_flag == 20:
                    print("出现异常错误五次 url {} 异常 {}".format(url, e))
                    return "fail"
                continue

            time.sleep(5)


