#! /usr/bin env python
# coding:utf-8
# 业务逻辑

import pymysql
from Logic import Logic
from Utils import getCurFullDay, D

class BlockLogic(Logic):
    def __init__(self):

        Logic.__init__(self, "finance_data")

    def importData(self,data):

        sql = ""
        if 0 == len(data):
            return

        for i in data:
            # try:
                futures_name = i.get("futures_name", "")
                chinese_name = i.get("c_name", "")
                marketInfo = self.get_futures_id(futures_name, chinese_name)
                if 0 == len(marketInfo):
                    continue
                # D(marketInfo.get("id"))
                futures_id = marketInfo.get("id")
                futures_name = i.get("futures_name", "")
                price = i.get("price")
                price_cny = i.get("price_cny")
                rise_price = i.get("rise_price")
                rise_rate = i.get("rise_rate")
                open_price = i.get("open_price")
                hight_price = i.get("hight_price")
                low_price = i.get("low_price")
                last_settlement_price = i.get("last_settlement_price")
                hold = i.get("hold")
                buy_price = i.get("buy_price")
                buy_amount = i.get("buy_amount")
                sell_price = i.get("sell_price")
                sell_amount = i.get("sell_amount")
                market_time = i.get("market_time")
                create_at = getCurFullDay()

                valuesTuple = (
                    futures_id,
                    futures_name,
                    price,
                    price_cny,
                    rise_price,
                    rise_rate,
                    open_price,
                    hight_price,
                    low_price,
                    last_settlement_price,
                    hold,
                    buy_price,
                    buy_amount,
                    sell_price,
                    sell_amount,
                    market_time,
                    create_at,
                )
                D(valuesTuple)

                is_exsit = self.get_date("futures_deals", futures_id, market_time)
                if is_exsit:
                    print("数据存在")
                    continue

                values = "%s,'%s',%s,%s,%s,'%s',%s,%s,%s,%s,%s,%s,%s,%s,%s,'%s','%s'" % valuesTuple
                sql = "insert into %s (`futures_id`, `futures_name`, `price`, `price_cny`, `rise_price`, `rise_rate`, `open_price`, `hight_price`,`low_price`,`last_settlement_price`, `hold`, `buy_price`, `buy_amount`, `sell_price`, `sell_amount`, `market_time`, `create_at`) values (%s)" % (
                        "futures_deals", values)
                D("Insert: %s" % sql)
                self.cursor.execute(sql)
            # except:
            #     D("futures_id {} 发生错误".format(i.get("futures_name")))
    #     # 提交事物
        self.db.commit()
