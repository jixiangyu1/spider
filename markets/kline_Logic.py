#! /usr/bin env python
# coding:utf-8
# 业务逻辑

import pymysql
from Logic import Logic
from Utils import getCurFullDay, D
from logger_file import logger
from markets.market_config import config_dict
class kline_Logic(Logic):
    def __init__(self):
        Logic.__init__(self, "finance_data")
        self.dat_dicts = {}

    def importData(self,data, flag, logger_name):

        sql = ""
        logs = logger(logger_name)
        if 0 == len(data):
            return

        for i in data:
            for name, lists in i.items():
                try:
                    marketInfo = self.get_futures_id(name, "nnn", 2)
                    print(name, marketInfo.get("id"), lists[-1])
                    if marketInfo.get("id") == None or lists == None:
                        continue
                    # for k in lists:
                    k = lists[-1]
                    # D(name, k)
                    if flag in [5, 15, 30, 60]:
                        futures_id = marketInfo.get("id")
                        futures_name = marketInfo.get("name")
                        open_price = k.get("o")
                        close_price = k.get("c")
                        high_price = k.get("h")
                        low_price = k.get("l")
                        market_time = k.get("d")
                        create_at = getCurFullDay()
                        rise_price = round(float(close_price) - float(open_price), 2)
                        rise_rate = round((float(close_price) - float(open_price)) / float(open_price) * 100, 4)
                        print(futures_id, futures_name, open_price, close_price, high_price, low_price, rise_price, rise_rate, k.get('d'))
                    else:
                        D('zouzhe')

                        futures_id = marketInfo.get("id")
                        futures_name = marketInfo.get("name")
                        open_price = k.get("open")
                        close_price = k.get("close")
                        high_price = k.get("high")
                        low_price = k.get("low")
                        market_time = k.get("date")
                        create_at = getCurFullDay()
                        D("markter {} 拿到的 {}".format(market_time, self.dat_dicts))
                        if self.dat_dicts == {}:
                            rise_price = round(float(close_price) - float(open_price), 2)
                            rise_rate = round((float(close_price) - float(open_price)) / float(open_price) * 100, 2)
                        else:
                            try:
                                rise_price = round(float(open_price) - float(self.dat_dicts.get("close")), 2)
                                rise_rate = round((float(close_price) - float(open_price)) / float(open_price) * 100, 4)
                            except:
                                continue
                        print(futures_id, futures_name, open_price, close_price, high_price, low_price, rise_price, rise_rate, k.get('date'))
                        self.dat_dicts['close'] = close_price
                    is_exsit = self.get_date(config_dict.get(flag), futures_id, market_time)
                    if is_exsit:
                        print("数据存在")
                        continue
                    valuesTuple = (
                        futures_id,
                        futures_name,
                        rise_price,
                        rise_rate,
                        open_price,
                        close_price,
                        high_price,
                        low_price,
                        market_time,
                        create_at
                    )
                    D(valuesTuple)

                    values = "%s,'%s',%s,%s,%s,%s,%s,%s,'%s','%s'" % valuesTuple
                    sql = "insert into %s (`futures_id`, `futures_name`, `rise_price`, `rise_rate`, `open`, `close`, `high`, `low`,`market_time`,`create_at`) values (%s)" % (
                           config_dict.get(flag) , values)
                    D("Insert: %s" % sql)
                    self.cursor.execute(sql)
                except Exception as e:
                    D("futures_id {} 发生错误".format(i.get("futures_name")))
                    logs.info("futures_id {} 发生错误 {}".format(i.get("futures_name"), e))

    # #     # 提交事物
        self.db.commit()
