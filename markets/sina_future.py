#encoding:utf8
import json
import re
from news.request_class import req
import requests
from Utils import D
from markets.market_Logic import BlockLogic
from logger_file import logger
class sina_future():
    def __init__(self):
        self.req = req()
        self.url= "http://finance.sina.com.cn/money/future/hf.html?from=wap"
        self.hf = {}
        self.headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36"
        }
        self.name_value = []
        self.chinese_name =[]
        self.futures_list = []
        self.logic = BlockLogic()
        self.logger = logger('sina_futures.log')
    def req_cny_rate(self):
        response_cny = requests.get("http://hq.sinajs.cn/?list=USDCNY", headers=self.headers).text
        result = (self.re_rules(response_cny))
        if result == "fail":
            return "fail"
        return (result[0].split(",")[-3])

    def re_rules(self, response):
        req_detail = re.compile(r'var .*?=(.*?);')
        result = req_detail.findall(response)
        if result == []:
            return "fail"
        return result

    def start_request(self):
        cny_result = self.req_cny_rate()
        if cny_result == "fail":
            return "fail"
        D(cny_result)

        response = requests.get(self.url, headers=self.headers)
        code = (response.encoding)
        html = response.text
        html = html.encode(code)
        response = html.decode("gb18030")
        re_bz_com = re.compile(r'var oHF_1 = (.*?);', re.S)
        result = re_bz_com.findall(response)
        import demjson
        for i, j in demjson.decode(result[0]).items():
            self.hf[j[0]] = j[1][0]
            self.name_value.append(j[1][0])
            self.chinese_name.append(j[0])
        self.hf["cny_rate"] = cny_result
        parame_list = (list(demjson.decode(result[0]).keys()))
        pp = ""
        for i in parame_list:
            pp += "hf_" + i + ","
        D(pp)
        url_index = "http://hq.sinajs.cn/?list={}".format(pp)
        response_index = requests.get(url_index, headers=self.headers)
        rules_result = self.re_rules(response_index.text)
        if rules_result == "fail":
            D("没有匹配到行情")
            return

        for i, j, z, b in zip(parame_list, rules_result, self.name_value, self.chinese_name):

            zui_price = str(j.split(",")[0]).replace('"', "").strip()
            if zui_price == "":
                continue
            item = {}

            item['price'] = float(zui_price)
            item['price_cny'] = float(zui_price) *  float(self.hf.get("cny_rate")) * float(z)
            item['rise_price'] = round(float(zui_price) - float(j.split(",")[7]), 2)
            item['rise_rate'] = "{:.2f}%".format(round(float((float(zui_price) - float(j.split(",")[7])) / float(j.split(",")[7]) * 100), 2))
            item['open_price'] = float(j.split(",")[8])
            item['hight_price'] = float(j.split(",")[4])
            item['low_price'] = float(j.split(",")[5])
            item['last_settlement_price'] = float(j.split(",")[7])
            item['hold'] = float(j.split(",")[9])
            item['buy_price'] = float(j.split(",")[2])
            item['buy_amount'] = float(j.split(",")[10])
            item['sell_price'] = float(j.split(",")[10])
            item['sell_amount'] = float(j.split(",")[11])
            item['market_time'] = (j.split(",")[6])
            item['futures_name'] = i
            item['c_name'] = b
            D(item)
            self.futures_list.append(item)
        self.logger.info(str(self.futures_list))
        self.logic.importData(self.futures_list)

pp = sina_future()
pp.start_request()
'''
上周上线问题：
1.沟通上线内容不明确
2.没有在正确的时间将teambition里面的任务放在正确的地方
3.没有团队意识 将代码放在一起进行联调
4.没有把握好上线时间和测试时间，导致上线与测试推迟

反思与行动：
1.评估需求开发时间要准
2.要及时更新teambition的状态
3.与其他开发人员要达成一致进行程序联调
4.要严格按照我开发与测试流程进行

'''
'''
5 https://stock2.finance.sina.com.cn/futures/api/jsonp.php/var%20_CT2020_3_19=/GlobalFuturesService.getGlobalFuturesDailyKLine?symbol=CT&_=2020_3_19&source=web
15 https://gu.sina.cn/ft/api/jsonp.php/var%20_CT_15_1584552387163=/GlobalService.getMink?symbol=CT&type=15
30 https://gu.sina.cn/ft/api/jsonp.php/var%20_CT_30_1584552409367=/GlobalService.getMink?symbol=CT&type=30
https://gu.sina.cn/ft/api/jsonp.php/var%20_CT_60_1584552506113=/GlobalService.getMink?symbol=CT&type=60
https://gu.sina.cn/ft/api/jsonp.php/var%20_CT_5_1584552565759=/GlobalService.getMink?symbol=CT&type=5
https://stock2.finance.sina.com.cn/futures/api/jsonp.php/var%20_CT2020_3_19=/GlobalFuturesService.getGlobalFuturesDailyKLine?symbol=CT&_=2020_3_19&source=web
https://stock2.finance.sina.com.cn/futures/api/jsonp.php/var%20_PBD2020_3_19=/GlobalFuturesService.getGlobalFuturesDailyKLine?symbol=PBD&_=2020_3_19&source=web

'''