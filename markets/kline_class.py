#encoding:utf8
import requests
import json
import re
import time
import demjson
from markets.kline_Logic import kline_Logic
import argparse
from Utils import D
from markets.markerReq_class import req_market
from Utils import getCurDay
from logger_file import logger

class Kline():
    def __init__(self):
        parser = argparse.ArgumentParser(description='manual to this script')
        parser.add_argument('--type', type=int, default=None)
        args = parser.parse_args()
        self.value = args.type
        # self.value = type
        self.data = []
        self.headers = {
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36",
        }
        self.logic = kline_Logic()
        self.req_market = req_market()
        self.logger = logger("future_{}.log".format(self.value))
        
    def future_list(self):
        self.logger.info("*************************{}*********************".format(getCurDay()))
        response = requests.get("http://finance.sina.com.cn/money/future/hf.html?from=wap", headers=self.headers)
        code = (response.encoding)
        html = response.text
        html = html.encode(code)
        response = html.decode("gb18030")
        re_bz_com = re.compile(r'var oHF_1 = (.*?);', re.S)
        result = re_bz_com.findall(response)
        parame_list = (list(demjson.decode(result[0]).keys()))
        return parame_list

    def parse(self):

        # self.logic.importData(self.data)

        parame_list = self.future_list()
        D(parame_list)
        for name in parame_list:
            D("==================={}====================".format(name))
            try:
                if self.value in [5, 15, 30, 60]:
                    self.url = "https://gu.sina.cn/ft/api/jsonp.php/var%20_{}_{}_1584608155365=/GlobalService.getMink?symbol={}&type={}".format(name,self.value, name, self.value)
                    self.re_rules = 'var _{}_{}_1584608155365=\((.*?)\);'.format(name, self.value)

                else:
                    self.url = "https://stock2.finance.sina.com.cn/futures/api/jsonp.php/var%20_{}=/GlobalFuturesService.getGlobalFuturesDailyKLine?symbol={}&_={}&source=web".format(name+getCurDay(), name, getCurDay())
                    self.re_rules = 'var _{}=\((.*?)\);'.format(name+getCurDay())
                D(self.url)
                response = requests.get(self.url).text
                response = str(response).replace("/*<script>location.href='//sina.com';</script>*/", "")
                re_data = re.compile(self.re_rules,re.S)
                jsondata = re_data.findall(response)
                jsondata = json.loads(jsondata[0])
                self.data.append({name:jsondata})
                time.sleep(2)
            except:
                D("***************{}********************出错".format(name))
                continue
        # self.logger.info("拿到的数据{}".format(str(self.data)))
        self.logic.importData(self.data, self.value, "future_{}.log".format(self.value))

if __name__ == '__main__':
    logic = kline_Logic()
    # for i in [5]:
    op = Kline()
    op.parse()